<?php

$config = array(
    '/promotion/cashback' => [
        'promotion_name' => [
            'field' => 'promotion_name',
            'label' => 'Name',
            'rules' => 'required',
            'input' => ['type'=>'text']
        ],
        'max_claim_amount_per_user' => [
            'field' => 'max_claim_amount_per_user',
            'label' => 'Maximum claim amount per user',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'max_claim_amount_per_order' => [
            'field' => 'max_claim_amount_per_order',
            'label' => 'Maximum claim amount per transaction',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'minimum_transaction_amount' => [
            'field' => 'minimum_transaction_amount',
            'label' => 'Minimum Transaction Amount',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'promotion_value' => [
            'field' => 'promotion_value',
            'label' => 'Promotion Value ( % )',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'budget' => [
            'field' => 'budget',
            'label' => 'Budget',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'max_claim_per_user' => [
            'field' => 'max_claim_per_user',
            'label' => 'Maximum claim per user',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'max_claim_per_day' => [
            'field' => 'max_claim_per_day',
            'label' => 'Maximum claim per day',
            'rules' => 'required|is_natural',
            'input' => ['type'=>'number']
        ],
        'date_from' => [
            'field' => 'date_from',
            'label' => 'Start date',
            'rules' => 'required',
            'input' => ['type'=>'date']
        ],
        'date_to' => [
            'field' => 'date_to',
            'label' => 'End date',
            'rules' => 'required',
            'input' => ['type'=>'date']
        ],
        'claim_from' => [
            'field' => 'claim_from',
            'label' => 'Start',
            'rules' => 'required',
            'input' => ['type'=>'time']
        ],
        'claim_to' => [
            'field' => 'claim_to',
            'label' => 'End',
            'rules' => 'required',
            'input' => ['type'=>'time']
        ]
    ],
    '/promotion/banner' => [
        'title' => [
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required',
            'input' => [ 'type' => 'text' ]
        ],
        'image' => [
            'field' => 'image',
            'label' => 'Banner',
            'rules' => 'required',
            'input' => [
                'type' => 'file',
                'file_type' => 'jpg|jpeg|png'
            ]
        ],
        'cover' => [
            'field' => 'cover',
            'label' => 'Cover',
            'rules' => 'required',
            'input' => [
                'type' => 'file',
                'file_type' => 'jpg|jpeg|png'
            ]
        ],
        'highlight' => [
            'field' => 'highlight',
            'label' => 'Highlight',
            'rules' => '',
            'input' => [ 'type' => 'textarea' ]
        ],
        'web' => [
            'field' => 'web',
            'label' => 'Website',
            'rules' => '',
            'input' => [ 'type' => 'url' ]
        ],
        'email' => [
            'field' => 'email',
            'label' => 'Email',
            'rules' => '',
            'input' => [ 'type' => 'email' ]
        ],
        'phone' => [
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => '',
            'input' => [ 'type' => 'tel' ]
        ],
        'status' => [
            'field' => 'status',
            'label' => 'Status',
            'rules' => '',
            'input' => [ 'type' => 'select' ]
        ],
        'terms' => [
            'field' => 'terms',
            'label' => 'Terms',
            'rules' => '',
            'input' => [ 'type' => 'tinymce' ]
        ]
    ]
);