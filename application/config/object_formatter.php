<?php

$config['object_formatter'] = array(
    'ppobproducts' => array(
        'price' => 'integer',
        'category_id' => '@parent[ppob/DigitalCategory]',
        'created_at' => 'utc'
    ),
    'ppobdigitaltransactions' => array(
        'price' => 'integer',
        'admin_fee' => 'integer',
        'total' => 'integer',
        'user_id' => '@parent[nusausers/NusaUser]'
    ),
    'merchantproduct' => array(
        'price' => 'integer',
        'price_agent' => 'integer',
        'merchant_id' => '@parent[merchant/Merchant]',
        'category_id' => '@parent[merchant/Category]',
        'created_at' => 'utc',
        'updated_at' => 'utc',
        'deleted_at' => 'utc'
    ),
    'ticket' => array(
        'subject' => 'text',
        'issuer' => '@parent[main/User]',
        'category' => '@parent[ticket/TicketCategory]',
        'priority' => '@parent[ticket/TicketPriority]',
        'status' => '@parent[ticket/TicketStatus]',
        'assigned_to' => '@parent[main/User]',
        'rejected_by' => '@parent[main/User]',
        'resolved_by' => '@parent[main/User]',
        'solving_time' => 'utc',
        'time_to_solve' => 'utc',
        'updated' => 'utc',
        'created' => 'utc'
    ),
    'ticket/TicketCategory' => array(
        'parent' => '@parent[ticket/TicketCategory]'
    ),
    'ticket_guideline' => array(
        'subject' => 'text'
    ),
    'ticket_comment' => array(
        'user' => '@parent[main/User]',
        'updated' => 'utc',
        'created' => 'utc'
    ),
    'promocashback' => array(
        
    ),
    'promocashbacktrans' => array(
        'redeemed_by' => '@parent[nusausers/NusaUser]',
        'created_at' => 'utc'
    )
);