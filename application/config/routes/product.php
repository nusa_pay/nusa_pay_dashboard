<?php

$route['product/merchant']              = 'product/merchant/index';
$route['product/merchant/filter']       = 'product/merchant/filter';
$route['product/merchant/item/(:num)']  = 'product/merchant/item/$1';

$route['product/ppob']              = 'product/ppob/index';
$route['product/ppob/filter']       = 'product/ppob/filter';
$route['product/ppob/item/(:num)']  = 'product/ppob/item/$1';
$route['product/ppob/edit/(:num)']  = 'product/ppob/edit/$1';
$route['product/ppob/set']          = 'product/ppob/set';