<?php

$route['promotion/banner']               = 'promotion/banner/index';
$route['promotion/banner/(:num)']        = 'promotion/banner/edit/$1';
$route['promotion/banner/(:num)/remove'] = 'promotion/banner/remove/$1';

$route['promotion/cashback']               = 'promotion/cashback/index';
$route['promotion/cashback/filter']        = 'promotion/cashback/filter';
$route['promotion/cashback/edit/(:num)']   = 'promotion/cashback/edit/$1';
$route['promotion/cashback/item/(:num)']   = 'promotion/cashback/item/$1';
$route['promotion/cashback/terminate/(:num)'] = 'promotion/cashback/terminate/$1';