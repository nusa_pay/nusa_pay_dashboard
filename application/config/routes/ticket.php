<?php

$route['ticket']                        = 'ticket/ticket/index';
$route['ticket/create']                 = 'ticket/ticket/create';
$route['ticket/item/(:num)']            = 'ticket/ticket/single/$1';
$route['ticket/item/(:num)/activate']   = 'ticket/ticket/activate/$1';
$route['ticket/item/(:num)/comment']    = 'ticket/ticket/comment/$1';
$route['ticket/item/(:num)/reject']     = 'ticket/ticket/reject/$1';
$route['ticket/item/(:num)/resolve']    = 'ticket/ticket/resolve/$1';