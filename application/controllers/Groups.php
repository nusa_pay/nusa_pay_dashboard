<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends SB_Controller 
{

  protected $layout   = "layouts/main";
  public $module     = 'groups';
  public $per_page  = '10';

  function __construct() {
    parent::__construct();
    
    $this->load->model('groupsmodel');
    $this->model = $this->groupsmodel;
    
    $this->info = $this->makeInfo( $this->module);
    $this->access = $this->validAccess($this->info['id']);  
    
    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);
    
  }
  
  function index() 
  {
    if($this->access['is_view'] ==0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
    }  
      
   $this->data['cardTitle'] = "Groups";
    $this->data['cardDesc'] = "List Groups App";
    $this->data['listGroups'] = json_encode($this->model->list());
    $this->data['content'] = $this->load->view('groups/index',$this->data, true );
    
    $this->load->view('layouts/main', $this->data );
    
    
  }
  
  function show( $id = null) 
  {
    if($this->access['is_detail'] ==0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
      }    

    $row = $this->model->getRow($id);
    if($row)
    {
      $this->data['row'] =  $row;
    } else {
      $this->data['row'] = $this->model->getColumnTable('tb_groups'); 
    }
    
    $this->data['id'] = $id;
    $this->data['content'] =  $this->load->view('groups/view', $this->data ,true);    
    $this->load->view('layouts/main',$this->data);
  }
  
  function add( $id = null ) 
  {
    // if($id =='')
    //   if($this->access['is_add'] ==0) redirect('dashboard',301);

    // if($id !='')
    //   if($this->access['is_edit'] ==0) redirect('dashboard',301);  

    $row = $this->model->getRow( $id );
    if($row)
    {
      $this->data['row'] =  $row;
    } else {
      $this->data['row'] = $this->model->getColumnTable('tb_groups'); 
    }
  
    $this->data['cardTitle'] = "Groups";
    $this->data['cardDesc'] = "Add Group App";
    $this->data['listGroups'] = $this->getGroupUser();
    $this->data['id'] = $id;
    $this->data['content'] = $this->load->view('groups/form',$this->data, true );    
      $this->load->view('layouts/main', $this->data );
  
  }
  
  function save() {

      
      $data = $this->input->post();
      $id = $this->model->insertRow($data , $this->input->get_post( 'group_id' , true ));

    // Redirect after save  
      SiteHelpers::alert('success',"save success");
      redirect('groups',301);
  }

   function nonActive($id)
  {
    if($id != null)
    {
        $row = $this->model->getRow($id);
        // $data['updated_at'] = date('Y-m-d H:i:s');
        if($row['active'] == 1) {
          $data['active'] = 0;
        } else {  $data['active'] = 1;}
      
        $this->model->insertRow($data , $id);

      SiteHelpers::alert('success',"Save success");
      redirect('groups',301);

    } else {
       SiteHelpers::alert('error','The following errors occurred');
       redirect('groups',301);
    }
  }

  function destroy()
  {
    if($this->access['is_remove'] ==0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
      }
      
    $this->model->destroy($this->input->post( 'id' , true ));
    $this->inputLogs("ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
    
    SiteHelpers::alert('success',"ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
    Redirect('groups',301);
  }


}

?>