<?php

if(!defined('BASEPATH'))
    die;

class Media extends SB_Controller
{
    function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);
    }
    
    /**
     * File upload
     */
    public function upload(){
        $this->load->library('MediaFile', '', 'media');
        $name = $this->input->post('name');
        $data = $this->media->processUpload('file', $name);

        if(!is_string($data)){
            $data = array(
                'original_name' => $data['original_name'],
                'media_file'    => $data['local_media_file']
            );
        }

        $json = json_encode($data);
        $cset = config_item('charset');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', $cset)
            ->set_output($json)
            ->_display();
        exit;
    }
}