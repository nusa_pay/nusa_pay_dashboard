<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends SB_Controller 
{

  protected $layout   = "layouts/main";
  public $module     = 'users';
  public $per_page  = '10';

  function __construct() {
    parent::__construct();
    
    $this->load->model('usersmodel');
    $this->model = $this->usersmodel;
    
    $this->info = $this->makeInfo( $this->module);
    $this->access = $this->validAccess($this->info['id']);  
    
    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);
    
  }
  
  function index() 
  {
    if ($this->access['is_view'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }

    // Render into template
    $this->data['cardTitle'] = "Users";
    $this->data['cardDesc'] = "List User App";
    $this->data['listusers'] = json_encode($this->model->list());
    $this->data['content'] = $this->load->view('users/index',$this->data, true );
    
    $this->load->view('layouts/main', $this->data );
    
    
  }
  
  function show( $id = null) 
  {
    if($this->access['is_detail'] ==0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
      }    

    $row = $this->model->getRow($id);
    if($row)
    {
      $this->data['row'] =  $row;
    } else {
      $this->data['row'] = $this->model->getColumnTable('tb_users'); 
    }
    
    $this->data['id'] = $id;
    $this->data['content'] =  $this->load->view('users/view', $this->data ,true);    
    $this->load->view('layouts/main',$this->data);
  }
  
  function add( $id = null ) 
  {
    // if($id =='')
    //   if($this->access['is_add'] ==0) redirect('dashboard',301);

    // if($id !='')
    //   if($this->access['is_edit'] ==0) redirect('dashboard',301);  

    $row = $this->model->getRow($id);
    if($row)
    {
      $this->data['row'] =  $row;
    } else {
      $this->data['row'] = $this->model->getColumnTable('tb_users');
    }


    $this->data['cardTitle'] = "Users";
    $this->data['cardDesc'] = "Add User App";
    $this->data['listGroups'] = $this->getGroupUser();
    $this->data['id'] = $id;
    $this->data['content'] = $this->load->view('users/form',$this->data, true );    
      $this->load->view('layouts/main', $this->data );
  
  }
  
  function save() {
    
   
      $data = $this->input->post();
      $data['active'] = 1;
      $data['created_at'] = date('Y-m-d h:i:s');
      if (isset($data['password'])) {
            $data['password'] = md5(trim($this->input->post('password')));
            
      }

      $id = $this->model->insertRow($data , $this->input->get_post( 'id' , true ));

    // Redirect after save  
      SiteHelpers::alert('success',"save success");
      redirect('users',301);
      
    // } else {
    //   $this->session->set_flashdata(
    //     array(
    //       'errors'  => validation_errors('<li>', '</li>')
    //       )
    //   );    
    //   SiteHelpers::alert('error','The following errors occurred');
    //   redirect('users/add/'.$this->input->get_post( 'id' , true ),301);
    // }
  }

  function nonActive($id)
  {
    if($id != null)
    {
        $row = $this->model->getRow($id);
        $data['updated_at'] = date('Y-m-d H:i:s');
        if($row['active'] == 1) {
          $data['active'] = 0;
        } else {  $data['active'] = 1;}
      
        $this->model->insertRow($data , $id);

      SiteHelpers::alert('success',"Save success");
      redirect('users',301);

    } else {
       SiteHelpers::alert('error','The following errors occurred');
       redirect('users',301);
    }
  }

  function destroy()
  {
    if($this->access['is_remove'] ==0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
      }
      
    $this->model->destroy($this->input->post( 'id' , true ));
    $this->inputLogs("ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
    SiteHelpers::alert('success',"ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
    Redirect('users',301);
  }


}

?>