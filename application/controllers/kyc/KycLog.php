<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KycLog extends SB_Controller {

	 public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('kyc/LogModel',"_logModel");
    }

	public function index()
	{
		
	}

	public function saveRejection($id)
	{
		
		// if (empty($this->session->userdata('uid'))) {
  //           SiteHelpers::alert('error', 'Your are not allowed to access the page');
  //           redirect('dashboard', 301);
  //       }

		$post = $this->input->post();
		$data['operator'] = $this->session->userdata('uid');
		$data['user'] = $id;
		$data['rejection_subject'] = $post['subject'];
		$data['rejection_reason'] = $post['content'];
		$data['rejection'] = 1;

		$insert = $this->_logModel->create($data);
		
		if ($insert) {
			//update user information
			 $this->load->model('NusaUsersModel',"num");
			  $updateReject = $this->num->updateVerifyKyc($id,$obj=array(),"REJECT");
			SiteHelpers::alert('success', 'Rejection success');
  			redirect('nusa/users/kyc-log', 301);
		} else {
			SiteHelpers::alert('error', 'Ups Something Wrong !');
  			redirect('nusa/users/kycRequestDetail/'.$id, 301);
		}
	}

	

}

/* End of file KycLog.php */
/* Location: ./application/controllers/kyc/KycLog.php */