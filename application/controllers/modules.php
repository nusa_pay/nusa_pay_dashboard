<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends SB_Controller {

  public $module     = 'modules';
  public $per_page  = '10';

  function __construct() {
    parent::__construct();
    
    $this->load->model('modulemodel');
    $this->model = $this->modulemodel;
    
    $this->info = $this->makeInfo( $this->module);
    $this->access = $this->validAccess($this->info['id']);  
    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);
    
  }

	public function index()
	{

	if($this->access['is_view'] == 0)
    { 
      SiteHelpers::alert('error','Your are not allowed to access the page');
      redirect('dashboard',301);
    }  

    // Render into template
    $this->data['cardTitle'] = "Modules";
    $this->data['cardDesc'] = "List Modules App";
    $this->data['listModule'] = json_encode($this->model->list());
    $this->data['content'] = $this->load->view('modules/index',$this->data, true );
    
    $this->load->view('layouts/main', $this->data );
	}

	public function permission($modulesId = null)
	{
		 // Render into template
	$this->data['id'] = $modulesId;
	//$row = $this->model->getRow($modulesId);
    

    $row = $this->db->get_where('tb_module',array('module_id'=>$modulesId))->row();
    $countRow = $this->db->get_where('tb_module',array('module_id'=>$modulesId))->num_rows();
   
    if($countRow <= 0) redirect('modules',301);
 
      $this->data['row'] = $row;
      $this->data['module'] = $this->module;
      $this->data['module_name'] = $row->module_name;
      $this->data['cardTitle']= "Modules ". $row->module_name;
    $this->data['cardDesc'] = "Set Permissions";
    $this->data['listGroups'] = $this->db->get('tb_groups')->result_array();
      $config = SiteHelpers::CF_decode_json($row->module_config);

       $tasks = array(
              //'is_global'        => 'Global ',
              'is_view'        => 'View ',
              'is_detail'        => 'Detail',
              'is_add'        => 'Add ',
              'is_edit'        => 'Edit ',
              'is_remove'        => 'Remove ',
              'is_export'        => 'Export ',
          );
      

      if(isset($config['tasks'])) {
        foreach($config['tasks'] as $row)
        {
          $tasks[$row['item']] = $row['title'];
        }
      }
      $this->data['tasks'] = $tasks;
      $this->data['groups'] = $this->db->get('tb_groups')->result();

      $access = array();
      foreach($this->data['groups'] as $r)
      {
      //    $GA = $this->model->gAccessss($this->uri->rsegment(3),$row['group_id']);
        $group = ($r->group_id !=null ? "and group_id ='".$r->group_id."'" : "" );
        $GA = $this->db->query("SELECT * FROM tb_groups_access where module_id ='".$row->module_id."' $group")->row();
        $countGA = $this->db->query("SELECT * FROM tb_groups_access where module_id ='".$row->module_id."' $group")->num_rows();
        if($countGA >=1){
          $GA = $GA;
        }

        $access_data = (isset($GA->access_data) ? json_decode($GA->access_data,true) : array());

        $rows = array();
        //$access_data = json_decode($AD,true);
        $rows['group_id'] = $r->group_id;
        $rows['group_name'] = $r->name;
        foreach($tasks as $item=>$val)
        {
          $rows[$item] = (isset($access_data[$item]) && $access_data[$item] ==1  ? 1 : 0);
        }
        $access[$r->name] = $rows;



      }
      //echo '<pre>';print_r($access);echo '</pre>';exit;
      $this->data['access'] = $access;
      $this->data['groups_access'] =$this->db->query("select * from tb_groups_access where module_id ='".$row->module_id."'")->result();
    $this->data['content'] = $this->load->view('modules/permissions',$this->data, true );
      
    $this->load->view('layouts/main', $this->data );

	}

	public function savePermissions($modulesId = null)
	{
	  $id = $modulesId;
      $row = $this->db->get_where('tb_module',array('module_id'=>$modulesId))->row();

      if(count($row) <= 0) redirect('modules',301);

          $this->data['row'] = $row;
          $config = SiteHelpers::CF_decode_json($row->module_config);
          $tasks = array(
              //'is_global'        => 'Global ',
              'is_view'        => 'View ',
              'is_detail'        => 'Detail',
              'is_add'        => 'Add ',
              'is_edit'        => 'Edit ',
              'is_remove'        => 'Remove ',
              'is_export'        => 'Export ',
          );
        

          if(isset($config['tasks'])) {
              foreach($config['tasks'] as $row)
              {
                  $tasks[$row['item']] = $row['title'];
              }
          }

     
          $permission = array();

      $this->db->where('module_id',$id);
      $this->db->delete('tb_groups_access');

          $groupID = $_POST['group_id'];
          for($i=0;$i<count($groupID); $i++)
          {
              // remove current group_access
              $group_id = $groupID[$i];
              $arr = array();
              $id = $groupID[$i];
              foreach($tasks as $t=>$v)
              {
                  $arr[$t] = (isset($_POST[$t][$id]) ? "1" : "0" );

              }

              $permissions = json_encode($arr);

              $data = array
              (
                  "access_data"    => $permissions,
                  "module_id"       => $modulesId,
                  "group_id"        => $groupID[$i],
              );
              $this->db->insert('tb_groups_access',$data);
          }

          SiteHelpers::alert('success','Permission Has Changed Successful.');
          redirect('modules',301);

	}

	function add( $id = null ) 
  	{
    // if($id =='')
    //   if($this->access['is_add'] ==0) redirect('dashboard',301);

    // if($id !='')
    //   if($this->access['is_edit'] ==0) redirect('dashboard',301);  

    $row = $this->model->getRow($id);
    if($row)
    {
      $this->data['row'] =  $row;
    } else {
      $this->data['row'] = $this->model->getColumnTable('tb_module');
    }


    $this->data['cardTitle'] = "Modules";
    $this->data['cardDesc'] = "Add Modules App";
    $this->data['id'] = $id;
    $this->data['content'] = $this->load->view('modules/form',$this->data, true );    
    $this->load->view('layouts/main', $this->data );
  
  	}


  	function save()
  	{
  	  
  	  $data = $this->input->post();
      $data['module_type'] = "addOn";
      $data['module_created'] = date('Y-m-d h:i:s');
    

      $id = $this->model->insertRow($data , $this->input->get_post( 'module_id' , true ));

    // Redirect after save  
      SiteHelpers::alert('success',"save Module success");
      redirect('modules',301);
  	}


  	function nonActive($id)
  {
    if($id != null)
    {
        $row = $this->model->getRow($id);
       
        if($row['active'] == 1) {
          $data['active'] = 0;
        } else {  $data['active'] = 1;}
      
        $this->model->insertRow($data , $id);

      SiteHelpers::alert('success',"Save success");
      redirect('modules',301);

    } else {
       SiteHelpers::alert('error','The following errors occurred');
       redirect('modules',301);
    }
  }

}

/* End of file modules.php */
/* Location: ./application/controllers/modules.php */