<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MerchantsRequest extends SB_Controller {
	public $module     = 'MerchantRequest';
	protected $layout   = "layouts/main";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('NusaMerchantRequestModel',"_mmrModel");
		$this->load->model('NusaUsersModel',"_nuModel");

		//model db_merchants_request
		$this->load->model('merchant_request/AccountsModel','_accountsModel');
		$this->load->model('merchant_request/AddressesModel','_addressModel');
		$this->load->model('merchant_request/AttachmentsModel','_attachmentModel');
		$this->load->model('merchant_request/ContractsModel','_contractsModel');
		$this->load->model('merchant_request/DirectorsModel','_directorsModel');
		$this->load->model('merchant_request/InformationModel','_informationModel');
		$this->load->model('merchant_request/MerchantsReqModel','_merchantsReqModel');
		$this->load->model('merchant_request/OwnerModel','_ownerModel');

		$this->info = $this->makeInfo( $this->module);
	    $this->access = $this->validAccess($this->info['id']);

		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
	}

	public function index()
	{
		if ($this->access['is_view'] == 0) {
	       SiteHelpers::alert('error','Your are not allowed to access the page');
	       redirect('dashboard',301);
    	}

		$this->data['cardTitle'] = "Merchants Request";
		$this->data['cardDesc'] = "All List Merchants";
		$this->data['request_list'] = json_encode($this->_mmrModel->getMerchantRequest());
		$this->data['access']		= $this->access;
		$this->data['content'] = $this->load->view('nusa/Merchants_Request/index', $this->data, true);
		$this->load->view('layouts/main', $this->data);
	}

	public function add($id = null)
	{
		if ($this->access['is_add'] == 0) {
	       SiteHelpers::alert('error','Your are not allowed to access the page');
	       redirect('dashboard',301);
    	}

    	$this->load->library('NusaApi', 'nusaapi');
        $this->load->library('SiteForm', '', 'form');

        $this->form->setForm('/promotion/banner');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

        $this->data = [
            'atoken' => $token,
            'upload_url' => $config['endpoint']['media'].'/api/upload'
        ];

    	$this->data['access']		= $this->access;
		$this->data['type_of_request'] = $this->_mmrModel->type_of_request();
		$this->data['company_residence'] = $this->_mmrModel->company_residence();
		$this->data['type_of_company'] = $this->_mmrModel->type_of_company();
		$this->data['cooperation_purpose'] = $this->_mmrModel->cooperation_purpose();
		$this->data['operating_transaction_per_month'] = $this->_mmrModel->operating_transaction_per_month();
		$this->data['bussiness_status'] = $this->_mmrModel->bussiness_status();
		$this->data['bussiness_type'] = $this->_mmrModel->bussiness_type();
		$this->data['bussiness_location'] = $this->_mmrModel->bussiness_location();
		$this->data['bussiness_omzet'] = $this->_mmrModel->bussiness_omzet();
		$this->data['avg_trx'] = $this->_mmrModel->avg_trx();
		$this->data['provinces'] = $this->_nuModel->getProvince();

		if ($id != null) {
			$this->data['row'] = (array) $this->_merchantsReqModel->get($id);
			$this->data['row_account'] = (array) $this->_accountsModel->getBy("merchant_id",$id);
			$this->data['row_address'] = (array) $this->_addressModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
			$this->data['row_attachment'] = (array) $this->_attachmentModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
			$this->data['row_contract'] = (array) $this->_contractsModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
			$this->data['row_director'] = (array) $this->_directorsModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
			$this->data['row_information'] = (array) $this->_informationModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
			$this->data['row_owner'] = (array) $this->_ownerModel->getBy("merchant_id",$id,1,1,['merchant_id'=>'ASC']);
		} else {
			$this->data['row'] = $this->_merchantsReqModel->getColumnTable();
			$this->data['row_account'] = $this->_accountsModel->getColumnTable();
			$this->data['row_address'] = $this->_addressModel->getColumnTable();
			$this->data['row_attachment'] = $this->_attachmentModel->getColumnTable();
			$this->data['row_contract'] = $this->_contractsModel->getColumnTable();
			$this->data['row_director'] = $this->_directorsModel->getColumnTable();
			$this->data['row_information'] = $this->_informationModel->getColumnTable();
			$this->data['row_owner'] = $this->_ownerModel->getColumnTable();
		}

        $this->data['content'] = $this->load->view('nusa/Merchants_Request/form', $this->data, true);
		$this->load->view('layouts/main', $this->data);
	}

	public function updateApprove($value='')
	{
		$post = $this->input->post();
		$data['status'] = $post['status'];
		$data['upd_date'] = date('Ymd h:i:s');
		$data['upd_by'] = $this->session->userdata('fid');

		$updateApprove = $this->_mmrModel->updateApprove($post['id'],$data);
		echo json_encode($updateApprove);
	}

	public function save($id = null)
	{
		$post = $this->input->post();
		//print_r($post); exit;	
		$merchantsId = $this->set_merchants_table($post,$id);

		 $this->set_merchants_information_table($merchantsId,$post,$id);
		 $this->set_merchants_address_table($merchantsId,$post,$id);
		 $this->set_merchants_dc($merchantsId,$post,$id);
		 $this->set_merchants_owner($merchantsId,$post,$id);
		 $this->set_merchants_contract($merchantsId,$post,$id);
		 $this->set_merchants_accounts_table($merchantsId,$post,$id);
		 $this->set_merchants_attachment($merchantsId,$post,$id);

		
		SiteHelpers::alert('success',"save success");
		redirect('nusa/merchantsRequest',301);
	}

	function set_merchants_table($post,$updateid = null)
	{
		$this->load->library('NusaApi', 'nusaapi');
        $this->load->config('nusaapi');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();
		
		$data['registration_number']	= $this->registraion_number();
		$data['business_name']			= $post['business_name'];
		$data['store_name']				= $post['store_name'];
		$data['email']					= $post['email'];
		$data['taxpayer_number']		= $post['taxpayer_number'];
		$data['phone']					= $post['phone'];
		$data['facsimile']				= $post['facsimile'];
		$data['crt_date']				= date('Ymdhis');
		$data['upd_date']				= date('Ymdhis');
		$data['logo']					= $post['file_logo'];
		$data['image']					= $post['file_image'];
		$data['usr_crt']				= $this->session->userdata('fid');


		if ($updateid == null) {
			//create
			$insert = $this->_merchantsReqModel->create($data);

			return $insert;
		} else {
			$update = $this->_merchantsReqModel->set($updateid,$data);
			return $updateid;
		}



		
		
	}

	function set_merchants_information_table($merchantId,$post,$updateid = null)
	{
		$data['merchant_id']						= $merchantId;
		$data['request_type']						= $post['type_of_request'];
		$data['established']						= $post['tempat_usaha_established'];
		$data['store_ownership_status']				= $post['tempat_usaha_status'];
		$data['business_entity']					= $post['bentuk_badan_usaha'];
		$data['company_resident']					= $post['company_residence'];
		$data['store_location']						= $post['tempat_usaha_lokasi'];
		$data['omzet']								= $post['tempat_usaha_omzet'];
		$data['avg_transaction']					= $post['tempat_usaha_avgTrx'];
		$data['usr_crt']							= $this->session->userdata('fid');
		$data['cooperation_purpose']				= $post['cooperation_purpose'];
		$data['operating_transaction_per_month']	= $post['operating_transaction_per_month'];


		if ($updateid == null) {
			//create
			$return = $this->_informationModel->create($data);
		} else {
			unset($data['merchant_id']);
			$return = $this->_informationModel->setBy("merchant_id",$updateid,$data);

		}


			
		return $return;
	}

	function set_merchants_address_table($merchantId,$post,$updateid = null)
	{
		$data['merchant_id']					= $merchantId;
		$data['address']						= $post['address'];
		$data['province']						= $post['provinces'];
		$data['zip_code']						= $post['zip_code'];

		if ($updateid == null) {
			//create
			$return = $this->_addressModel->create($data);
		} else {
			unset($data['merchant_id']);
			$return = $this->_addressModel->setBy("merchant_id",$updateid,$data);

		}


		return $return;
	}

	function set_merchants_accounts_table($merchantId,$post,$updateid = null)
	{
		$data['merchant_id']					= $merchantId;
		$data['name']							= $post['authorized_name'];
		$data['identity_number']				= $post['authorized_ktp'];
		$data['phone']					= $post['authorized_mobile_number'];
		$data['title']					= $post['authorized_position'];

		
		if ($updateid == null) {
			//create
			$return = $this->_accountsModel->create($data);
		} else {
			unset($data['merchant_id']);
			$return = $this->_accountsModel->setBy("merchant_id",$updateid,$data);

		}


		return $return;

	}

	function set_merchants_dc($merchantId,$post,$updateid = null)
	{
		$data['merchant_id']					= $merchantId;
		$data['name']							= $post['directors_name'];
		$data['date_of_birth']					= $post['directors_date_of_birth'];
		$data['gender']							= $post['directors_gender'];
		$data['identity_number']				= $post['directors_identity'];
		$data['taxpayer_number']				= $post['directors_tax_number'];

		if ($updateid == null) {
			//create
			$return = $this->_directorsModel->create($data);
		} else {
			unset($data['merchant_id']);
			$return = $this->_directorsModel->setBy("merchant_id",$updateid,$data);
		}


		return $return;


	}

	function set_merchants_owner($merchantId,$post,$updateid = null)
	{
		$data['merchant_id']					= $merchantId;
		$data['name']							= $post['owner_name'];
		$data['job_position']					= $post['owner_position'];
		$data['identity_number']				= $post['owner_ktp'];
		$data['taxpayer_number']				= $post['owner_tax_number'];
		$data['phone']					= $post['owner_phone_number'];
		$data['mobile_phone']							= $post['owner_mobile_number'];
		$data['email']							= $post['owner_email'];
		$data['address']							= $post['owner_address'];
		$data['province']							= $post['owner_province'];
		$data['zip_code']							= $post['owner_zip_code'];
		$data['siblings_contact']							= $post['owner_sibling_contact'];
		$data['siblings_phone']							= $post['owner_sibling_phone'];
		$data['city']							= $post['owner_city'];
		$data['village']							= $post['owner_village'];
		$data['subdistrict']							= $post['owner_subdistrict'];

		if ($updateid == null) {
			//create
			$return = $this->_ownerModel->create($data);
		} else {
			unset($data['merchant_id']);
			$return = $this->_ownerModel->setBy("merchant_id",$updateid,$data);
		}

		return $return;

	}

	function set_merchants_contract($merchantId,$post,$updateid = null)
	{

		$data['merchant_id']					= $merchantId;
		// $data['contract_number']				= $post['owner_name'];
		// $data['service']						= $post['directors_name'];
		// $data['contract_at']					= $post['directors_date_of_birth'];
		// $data['expired_at']						= $post['directors_gender'];
		// $data['status']							= $post['owner_ktp'];
		// $data['aproved_by']						= $post['directors_tax_number'];

		if ($updateid == null) {
			//create
			$return = $this->_contractsModel->create($data);
		} else {
			// unset($data['merchant_id']);
			// $return = $this->_contractsModel->setBy("merchant_id",$updateid,$data);
			$return = $updateid;
		}


		return $return;

	}

	function set_merchants_attachment($merchantId,$post,$updateid = null)
	{
		$this->load->library('NusaApi', 'nusaapi');
        $this->load->config('nusaapi');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

		$data['merchant_id']					= $merchantId;
		// $data['name']							= $post['owner_name'];
		// $data['job_position']					= $post['directors_name'];
		// $data['date_of_birth']					= $post['directors_date_of_birth'];
		// $data['gender']							= $post['directors_gender'];
		// $data['identity_number']				= $post['owner_ktp'];
		// $data['taxpayer_number']				= $post['directors_tax_number'];

		if ($updateid == null) {
			//create
			$return = $this->_addressModel->create($data);
		} else {
			// unset($data['merchant_id']);
			// $return = $this->_addressModel->setBy("merchant_id",$updateid,$data);
			$return = $updateid;
		}


		return $return;
		
	}

	function approve($merchantId)
	{
		$data['status'] = $this->input->post('status');

		$update = $this->_merchantsReqModel->setBy("id",$merchantId,$data);

		if ($update) {
			echo json_encode(array('status'=> 200));
		} else echo json_encode(array('status'=> 500));
	}

	function rejectApprove($merchantId)
	{

		$data['status'] = $this->input->post('status');

		$update = $this->_merchantsReqModel->setBy("id",$merchantId,$data);

		if ($update) {
			echo json_encode(array('status'=> 200));
		}
		
	}

	function registraion_number()
	{
		$reg = "FNP-MKT/".date('Ymdhis');

		return $reg;
	}

}

/* End of file MerchantsRequest.php */
/* Location: ./application/controllers/nusa/MerchantsRequest.php */