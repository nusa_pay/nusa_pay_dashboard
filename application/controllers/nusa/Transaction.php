<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends SB_Controller {
    protected $layout   = "layouts/main";
    public $module     = 'transaction';

	  public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('NusaUsersModel',"num");
        $this->load->model('NusaUserTransactionModel',"nutm");
        $this->load->model('NusaUsersWalletModel','nuw');
        $this->load->model('NusaPromotionUsersModel','NPUM');
        $this->load->model('NusaUsersTopUpModel','NTUM');
        $this->load->model('NusaUsersWithdrawModel','NWDM');
        $this->load->model('NusaUsersTransferModel','NTRFM');
        $this->load->model('NusaCartModel','NCM');
        $this->load->model('ppob/DigitalTransactionModel', '_PpobTrx');
        $data = array();
        $identityType = array('KTP' => "KTP",'SIM' => "SIM",'Kartu Pelajar' => "Kartu Pelajar",'Paspor' => "Paspor" );
        $this->statusCart = array('canceled' => "0",'new' => "1",'pending' => "2",'done' => "3" );

        $this->info = $this->makeInfo( $this->module);
        $this->access = $this->validAccess($this->info['id']);  

    }

    public function index()
    {
        if ($this->access['is_view'] == 0) {
               SiteHelpers::alert('error','Your are not allowed to access the page');
               redirect('dashboard',301);
            }

        $res = $this->NTUM->getTopUPlist();
        $this->data['topUpList'] = json_encode($res);
        $this->data['countTopUp'] = $this->getCountTopUp();
        $this->data['totalTopUp'] = $this->getTotalTopUp();
        // withdraw
        $res_wd = $this->NWDM->getWithdrawlist();
        $this->data['withdrawList'] = json_encode($res_wd);
        $this->data['countWithdraw'] = $this->getCountWithdraw();
        $this->data['totalWithdraw'] = $this->getTotalWithdraw();
        // transfer
        $res_trf = $this->NTRFM->getTransferlist();
        $this->data['transferList'] = json_encode($res_trf);
        $this->data['countTransfer'] = $this->getCountTransfer();
        $this->data['totalTransfer'] = $this->getTotalTransfer();
        //merchant
        $res_merchant = $this->NCM->getCartList();
        $most_selling_item = $this->NCM->getMostSellingItem();
        $this->data['merchantList'] = json_encode($res_merchant);
        $this->data['mostSellingItem'] = json_encode($most_selling_item);

        //transaction PPOB
        $ppob = $this->_PpobTrx->getList();
        $this->data['ppobList'] = json_encode($ppob);

        $this->data['content'] = $this->load->view('nusa/transaction/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    function getCountTopUp(){
        $countTopUp['deleted'] = $this->NTUM->getCountByStatus($this->statusWallet['deleted']);
        $countTopUp['cancel'] = $this->NTUM->getCountByStatus($this->statusWallet['cancel']);
        $countTopUp['waiting'] = $this->NTUM->getCountByStatus($this->statusWallet['waiting']);
        $countTopUp['success'] = $this->NTUM->getCountByStatus($this->statusWallet['success']);

        return $countTopUp;
    }

    function getTotalTopUp(){
        $totalTopUp['deleted'] = $this->NTUM->getTotalByStatus($this->statusWallet['deleted']);
        $totalTopUp['cancel'] = $this->NTUM->getTotalByStatus($this->statusWallet['cancel']);
        $totalTopUp['waiting'] = $this->NTUM->getTotalByStatus($this->statusWallet['waiting']);
        $totalTopUp['success'] = $this->NTUM->getTotalByStatus($this->statusWallet['success']);

        return $totalTopUp;
    }

     function getCountWithdraw(){
        $countWithdraw['deleted'] = $this->NWDM->getCountByStatus($this->statusWallet['deleted']);
        $countWithdraw['cancel'] = $this->NWDM->getCountByStatus($this->statusWallet['cancel']);
        $countWithdraw['waiting'] = $this->NWDM->getCountByStatus($this->statusWallet['waiting']);
        $countWithdraw['success'] = $this->NWDM->getCountByStatus($this->statusWallet['success']);

        return $countWithdraw;
    }

    function getTotalWithdraw(){
        $totalWithdraw['deleted'] = $this->NWDM->getTotalByStatus($this->statusWallet['deleted']);
        $totalWithdraw['cancel'] = $this->NWDM->getTotalByStatus($this->statusWallet['cancel']);
        $totalWithdraw['waiting'] = $this->NWDM->getTotalByStatus($this->statusWallet['waiting']);
        $totalWithdraw['success'] = $this->NWDM->getTotalByStatus($this->statusWallet['success']);

        return $totalWithdraw;
    }

     function getCountTransfer(){
        $countTransfer['deleted'] = $this->NTRFM->getCountByStatus($this->statusWallet['deleted']);
        $countTransfer['cancel'] = $this->NTRFM->getCountByStatus($this->statusWallet['cancel']);
        $countTransfer['waiting'] = $this->NTRFM->getCountByStatus($this->statusWallet['waiting']);
        $countTransfer['success'] = $this->NTRFM->getCountByStatus($this->statusWallet['success']);

        return $countTransfer;
    }

    function getTotalTransfer(){
        $totalTransfer['deleted'] = $this->NTRFM->getTotalByStatus($this->statusWallet['deleted']);
        $totalTransfer['cancel'] = $this->NTRFM->getTotalByStatus($this->statusWallet['cancel']);
        $totalTransfer['waiting'] = $this->NTRFM->getTotalByStatus($this->statusWallet['waiting']);
        $totalTransfer['success'] = $this->NTRFM->getTotalByStatus($this->statusWallet['success']);

        return $totalTransfer;
    }

    function json_getCartDetail()
    {
        $cart_id = $this->input->post('cart_id');
        $cart_detail = $this->NCM->getCartDetail($cart_id);

        echo json_encode($cart_detail);

    }

}

/* End of file Transaction.php */
/* Location: ./application/controllers/nusa/Transaction.php */