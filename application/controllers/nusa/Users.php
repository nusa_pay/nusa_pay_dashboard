<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends SB_Controller {

  protected $layout   = "layouts/main";
  public $module     = 'nusa_users';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('NusaUsersModel',"num");
        $this->load->model('NusaUserTransactionModel',"nutm");
        $this->load->model('NusaUsersWalletModel','nuw');
        $this->load->model('NusaPromotionUsersModel','NPUM');
        $this->load->model('wallet/WalletLimitModel','_walletLimitModel');

        $data = array();
        $identityType = array('KTP' => "KTP",'SIM' => "SIM",'Kartu Pelajar' => "Kartu Pelajar",'Paspor' => "Paspor" );
        $this->statusCart = array('canceled' => "0",'new' => "1",'pending' => "2",'done' => "3" );

        $this->info = $this->makeInfo( $this->module);
        $this->access = $this->validAccess($this->info['id']);  
    }
    
    public function index()
    {
        
    if ($this->access['is_view'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }
       
        $this->data['cardTitle'] = "All User";
        $this->data['cardDesc'] = "-";

         $this->data['countAllUser']  = count($this->num->getAllUsers());
         $this->data['countActiveUser']  = $this->num->countUser(1);
         $this->data['countNotActiveUser']  = $this->num->countUser(0);
         $this->data['countSuspendUser']  = $this->num->countUser(2);
         $this->data['countKyc']  = $this->num->countStatusKyc(2);
         $this->data['countNonKYC']  = $this->num->countNonKyc();
        

        $this->data['content'] = $this->load->view('nusa/user/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
        
    }

    function merchants()
    {
       
        $this->data['cardTitle'] = "Merchants User";
        $this->data['cardDesc'] = "-";

      
        $this->data['merchants'] = json_encode($this->num->getMerchantUser());
        
        $this->data['content'] = $this->load->view('nusa/user/merchants', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function kycRequest()
    {
        
    if ($this->access['is_view'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }

        $this->data['cardTitle'] = "All Request User KYC";
        $this->data['cardDesc'] = "-";

        $this->data['content'] = $this->load->view('nusa/user/kyc_request', $this->data, true);
        $this->load->view('layouts/main', $this->data);

    }

    public function kycLog()
    {
        
        if ($this->access['is_view'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }

        $this->load->model('kyc/LogModel',"_logModel");

        $this->data['cardTitle'] = "KYC LOG";
        $this->data['cardDesc'] = "-";
        $this->data['logData'] = json_encode($this->_logModel->logList());

        $this->data['content'] = $this->load->view('nusa/user/kyc_request_log', $this->data, true);
        $this->load->view('layouts/main', $this->data);

    }

     public function kycRequestDetail($id = null)
    {
       
        if ($this->access['is_view'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }

    
        $this->data['cardTitle'] = "User KYC Detail";
        $this->data['cardDesc'] = "-";
        $this->data['id'] = $id;
        $this->data['kycReqData'] = $this->num->getUserKycDetail($id);
        $this->data['content'] = $this->load->view('nusa/user/kyc_request_detail', $this->data, true);
        $this->load->view('layouts/main', $this->data);

    }

    public function profilling($userId = null)
    {
         
        if ($this->access['is_view'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }



        $this->data['content'] = $this->load->view('nusa/user/profilling', $this->data, true);
        $this->load->view('layouts/main', $this->data);
        
    }

     public function save_profilling($id='')
    {
         
        if ($this->access['is_edit'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }

        $post = $this->input->post();

        if ($post['id'] == "") {
            $data['job_name'] = $post['job_name'];
            $data['risk_type'] = $post['risk_type'];
            $data['DTM_CRT'] = date('Y-m-d H:i:s');
            $data['USR_CRT'] = $this->session->userdata('fid');

            $res = $this->db->insert('job_profiling', $data);
			redirect('nusa/users/profilling',301);

        } else {
        	//get risk_id by name

        	$this->db->where('risk_name', $post['risk_name']);
        	$risk_type = $this->db->get('job_profiling_type')->row_array()['id'];

        	$object['risk_type'] = $risk_type;
        	$object['job_name'] = $post['job_name'];
        	$this->db->where('id', $post['id']);
        	$update = $this->db->update('job_profiling', $object);
        	
        	echo json_encode($update);
        }
        
        
        
    }

    public function update_profiling($id)
    {
       
        if ($this->access['is_edit'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to edit this page');
           redirect('dashboard',301);
        }

        $post = $this->input->post();

        $update = $this->num->update_profiling($id,$post);

        echo $update;

    }


     public function saveKycRequest()
    {
        
    if ($this->access['is_edit'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }

        $id = $this->input->post('id');
        

        $data['indentity_type'] = $this->input->post('indentity_type');
        $data['indentity_number'] = $this->input->post('indentity_number');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['approved_by'] = $this->session->userdata('uid');
        
        $update = $this->num->updateVerifyKyc($id,$data);

        if ($update) {
            # code...
            SiteHelpers::alert('success', 'Approve success');
            redirect('nusa/users/kyc-request/',301);
        } else {
            //handling error
            SiteHelpers::alert('error', 'Approve error!');
            redirect('nusa/users/kyc-request/',301);
        }

        

    }

     public function detail($id = null)
    {
        
        if ($this->access['is_detail'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }

        $this->load->library('NusaApi', 'nusaapi');
        $this->load->library('SiteForm', '', 'form');

        $this->form->setForm('/promotion/banner');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

        $this->data = [
            'atoken' => $token,
            'upload_url' => $config['endpoint']['media'].'/api/upload'
        ];

        //$this->data['userTransaction'] = $this->nutm->getTrxByUser($id);
        $this->data['user_detail'] = $this->num->getDetailUser($id);
        $this->data['user_address'] = $this->num->getAdressUser($id);
        // transaction purcashes
        $this->data['user_total_trx'] = $this->nutm->getTotalTrxByUser($id);
        $this->data['user_total_trx_done'] = $this->nutm->getTotalTrxByUser($id,$this->statusCart['done']);
        $this->data['user_total_trx_cancel'] = $this->nutm->getTotalTrxByUser($id,$this->statusCart['canceled']);
        $this->data['user_total_trx_new'] = $this->nutm->getTotalTrxByUser($id,$this->statusCart['new']);
        $this->data['user_total_trx_pending'] = $this->nutm->getTotalTrxByUser($id,$this->statusCart['pending']);
        // wallet        
        $this->data['wallet_balance'] = $this->nuw->getUserBalance($id);
        $this->data['wallet_limit_trx'] = $this->nuw->getMaxUserWalletTrx($id);
        //bonus
        $this->data['total_promotion_user'] = $this->NPUM->getTotalPromotionUser($id);
        //wallet ncash
        $this->data['wallet_ncash'] = $this->nuw->getUserBalance($id);
        //user referral
        $this->data['user_referral'] = json_encode($this->num->getReferrals($id));
        $this->data['user_referral_by'] = json_encode($this->num->getReferralsBy($id));
        $this->data['userId'] = $id;
        $this->data['jobs'] = $this->db->get('job_profiling')->result_array();
        $this->data['tickets'] = $this->db->get_where('ticket',['status'=>2])->result_array();

        $walletLimit = $this->_walletLimitModel->getBy('wallet',$this->data['wallet_balance']['id']);
        if (!$walletLimit) {
         $this->data['walletLimit'] = $this->_walletLimitModel->getColumnTable();   
        } else $this->data['walletLimit'] = (array)$walletLimit;
        
        $this->data['content'] = $this->load->view('nusa/user/user_detail', $this->data, true);
        $this->load->view('layouts/main', $this->data);

    }

      public function reporting($id = null)
    {
        
        if ($this->access['is_view'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }

        //$userMasterLocation = $this->num->getMasterLocationUser();
		$this->data['userMasterLocation'] = $this->num->getMasterLocationUser();
      
        $this->data['content'] = $this->load->view('nusa/user/reporting/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);

    }

    public function suspendAccount($id)
    {
      
        if ($this->access['is_edit'] == 0) {
           SiteHelpers::alert('error','Your are not allowed to access the page');
           redirect('dashboard',301);
        }
        
        $post = $this->input->post();

        //update status suspend
        $updateStatus = $this->num->suspendUser($id);

        if ($updateStatus) {
            //create suspend log
            $data['operator'] = $this->session->userdata('uid');
            $data['user'] = $id;
            $data['ticket_ref_id'] = $post['ref_ticket_id'];
            $data['subject'] = $post['subject'];
            $data['reason'] = $post['content'];

            $this->db->insert('tiket_ref_log', $data);

            SiteHelpers::alert('success', 'Freeze or Un freeze  success');
            redirect('nusa/users/detail/'.$id, 301);

        } else {
            SiteHelpers::alert('error', 'Ups Something Wrong !');
            redirect('nusa/users/detail/'.$id, 301);
        }

       

    }

    public function updateLockingWallet($id)
    {
        $formUpdateLocking = $this->input->post("formUpdateLocking");
        $formUpdateLockingReason = $this->input->post("formUpdateLockingReason");
       
        //update Wallet Locking
        $data['lock_in'] = $formUpdateLocking[0]['value'];
        $data['lock_out'] = $formUpdateLocking[1]['value'];
        $updateLockingWallet = $this->nuw->updateLockingWallet($id,$data);

        if ($updateLockingWallet) {
            //$this->insertTiketRefLog($id,$p)
            $log['ref_ticket_id'] = $formUpdateLockingReason[0]['value'];
            $log['subject'] = $formUpdateLockingReason[1]['value'];
            $log['content'] = $formUpdateLockingReason[2]['value'];

            $this->insertTiketRefLog($id,$log);
        }

        echo json_encode(array('status'=>$updateLockingWallet));
    }

    public function updateLimitWallet($id)
    {
        $data['balance'] = $this->input->post('balance');
        $data['transaction'] = $this->input->post('transaction');
        $data['documents'] = $this->input->post('documents');
        $formUpdateLimitReason =$this->input->post("formUpdateLimitReason");

        //get row wallet 
        $wallet = $this->nuw->getUserBalance($id);

        if (count($wallet) == 0) {
               SiteHelpers::alert('error','This User Has No Wallet');
               redirect('nusa/users/detail/'.$id, 301);
        }

        //check wallet has limit
        $walletLimit =  $this->_walletLimitModel->getBy('wallet',$wallet["id"],true);

  
        if (!$walletLimit) {
        //create a limit
        $data['wallet'] = $wallet['id'];
        $this->_walletLimitModel->create($data);
            
        } else {
        $this->_walletLimitModel->set($wallet['id'],$data);
        }

        //create log
        $log['ref_ticket_id'] = $formUpdateLimitReason[0]['value'];
        $log['subject'] = $formUpdateLimitReason[1]['value'];
        $log['content'] = $formUpdateLimitReason[2]['value'];
        
        $this->insertTiketRefLog($id,$log);

        echo json_encode(['status'=> 200]);
    }

    function insertTiketRefLog($id,$post)
    {
         //create suspend log
            $data['operator'] = $this->session->userdata('uid');
            $data['user'] = $id;
            $data['ticket_ref_id'] = $post['ref_ticket_id'];
            $data['subject'] = $post['subject'];
            $data['reason'] = $post['content'];

            $this->db->insert('tiket_ref_log', $data);
    }

    public function json_getAllUsers()
    {
        $items = json_encode($this->num->getAllUsers());
        print_r($items);
    }

    public function json_getTrxUser($id)
    {
        $items = json_encode($this->nutm->getTrxByUser($id));
        print_r($items);
    }

    public function json_getKycUser()
    {
        $items = json_encode($this->num->getListKycRequest());
        print_r($items);
    }


	public function json_get_wallet_top_up($id)
	{
	   $items = json_encode($this->nuw->getTopUpUserList($id));
	   print_r($items);
    }

    public function json_get_wallet_trx($id)
	{
	   $items = json_encode($this->nuw->getTransactionWalletByUser($id));
	   print_r($items);
	}

    public function json_get_promotion_user($id)
	{
	        $items = json_encode($this->NPUM->getListPromotionUsers($id));
	        print_r($items);
	}

    //reporting	 
	//transaction amount report   
	public function json_get_user_transaction_report()
	{

	$post = $this->input->post();
	    	
	if (empty($post['dateFrom'])) {
	    		$post['dateFrom'] = '0000-00-00';
	    		
	}

	if (empty($post['dateTo'])) {
	    		$post['dateTo'] = '0000-00-00';
	}

	if (empty($post['amount'])) {
	    		$post['amount'] =0;
	}

	$items['data'] = $this->nutm->getReportTrxUser($post);
	        
	        echo json_encode($items);
	}

    public function json_get_list_master_profilling()
    {
        $items = json_encode($this->num->getListProfilling());
        print_r($items);
    }


   

}

/* End of file Users.php */
