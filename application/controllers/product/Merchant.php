<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends SB_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);

        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->load->model('main/UserModel', '_User');
    }

    public function filter(){
        $this->load->model('merchant/ProductModel', '_Product');
        
        $sortf = $this->input->get('sortField');
        $sortd = $this->input->get('sortOrder');

        $filters = [
            'Name'      => 'product_name',
            'Price'     => 'product_price',
            'Merchant'  => 'merchant_name',
            'Industry'  => 'merchant_industry',
            'Sold'      => 'cart_sold'
        ];

        $cond = [];
        $sort = [];

        if($sortf && $sortd && isset($filters[$sortf]))
            $sort[ $filters[$sortf] ] = $sortd;

        foreach($filters as $qry => $loc){
            $val = $this->input->get($qry);
            if($val)
                $cond[$loc] = $val;
        }
        $page = (int)$this->input->get('pageIndex');
        if(!$page)
            $page = 1;
        $rpp  = (int)$this->input->get('pageSize');
        if(!$rpp)
            $rpp = 20;

        $products = $this->_Product->getByStats($cond, $rpp, $page, $sort);

        if($products){
            foreach($products as &$product)
                $product->Price = number_format($product->Price, 2, ',', '.');
            unset($product);
        }

        $result = [
            'data' => $products,
            'itemsCount' => $this->_Product->countByStats($cond)
        ];

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function index(){
        $this->load->model('merchant/TaggingTaggedModel', '_TTagged');

        $this->data = [];

        $industries = $this->_TTagged->getIndustries();
        if($industries){
            array_unshift($industries, (object)[
                'name' => 'All'
            ]);
        }
        $this->data['industries'] = $industries;

        $this->data['content'] = $this->load->view('product/merchant/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function item($id){
        $this->load->model('merchant/ProductModel', '_Product');
        $this->load->model('cart/CartItemModel', '_CItem');
        $this->load->model('merchant/TaggingTaggedModel', '_TTagged');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->data = [];

        $product = $this->_Product->get($id);
        if(!$product)
            return $this->show_404();

        $industries = $this->_TTagged->getByCond([
            'taggable_id' => $product->merchant_id,
            'taggable_type' => 'App\\Entities\\Merchant'
        ], true);

        if($industries)
            $this->data['industries'] = implode(', ', array_column($industries, 'tag_name'));

        $product = $this->formatter->merchantproduct($product, false, [
            'merchant_id',
            'category_id'
        ]);
        
        $this->data['product'] = $product;

        $this->data['purchased'] = $this->_CItem->productCountSuccess($product->id);

        $this->data['content'] = $this->load->view('product/merchant/single', $this->data, true);
        $this->load->view('layouts/main', $this->data);   
    }
}