<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppob extends SB_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);

        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->load->model('main/UserModel', '_User');
    }

    public function edit($id=null){
        $this->load->model('ppob/DigitalProductModel', '_Product');
        $this->load->model('ppob/DigitalCategoryModel', '_PCategory');

        $this->data = [];

        $product = $this->_Product->get($id);
        if(!$product)
            return $this->show_404();

        $this->data['product'] = $product;

        $categories = $this->_PCategory->getByCond([], true, false, ['name'=>'ASC']);
        $gcategories = [];
        foreach($categories as $cat){
            if(!isset($gcategories[$cat->parent_id]))
                $gcategories[$cat->parent_id] = [];
            $gcategories[$cat->parent_id][] = $cat;
        }
        $this->data['categories'] = $gcategories;

        // validate form
        if($_POST){
            $body = [];
            foreach($_POST as $key => $val)
                $body[$key] = preg_replace('![^0-9]!', '', $val);
            $this->_Product->setBy('id', $product->id, $body);
            return redirect('/product/ppob/item/' . $product->id, 301);

        }

        $this->data['content'] = $this->load->view('product/ppob/edit', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function filter(){
        $this->load->model('ppob/DigitalProductModel', '_Product');
        
        $sortf = $this->input->get('sortField');
        $sortd = $this->input->get('sortOrder');

        $filters = [
            'Type'      => 'product_admin_fee',
            'Code'      => 'product_code',
            'Name'      => 'product_name',
            'Price'     => 'product_price',
            'Category'  => 'product_category',
            'Sold'      => 'transaction_sold',
        ];

        $cond = [];
        $sort = [];

        if($sortf && $sortd && isset($filters[$sortf]))
            $sort[ $filters[$sortf] ] = $sortd;

        foreach($filters as $qry => $loc){
            $val = $this->input->get($qry);
            if($val)
                $cond[$loc] = $val;
        }
        $page = $this->input->get('pageIndex');
        if(!$page)
            $page = 1;
        $rpp  = $this->input->get('pageSize');
        if(!$rpp)
            $rpp = 20;

        $products = $this->_Product->getByStats($cond, $rpp, $page, $sort);
        if($products){
            foreach($products as &$product){
                $product->Category = (object)[
                    'Id' => $product->CId,
                    'Name' => $product->Category
                ];
                $product->Price = number_format($product->Price, 2, ',', '.');
                $product->Type = (object)[
                    'Id' => $product->Type,
                    'Name' => $product->Type == 1 ? 'Service' : 'Goods'
                ];
            }
            unset($product);
        }

        $result = [
            'data' => $products,
            'itemsCount' => $this->_Product->countByStats($cond)
        ];

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function index(){
        $this->load->model('ppob/DigitalCategoryModel', '_PCategory');
        $this->data = [];

        $types = [
            (object)['name' => 'Goods'],
            (object)['name' => 'Service']
        ];

        $this->data['types'] = $types;

        $categories = $this->_PCategory->getByCond(['status'=>1], true, false, ['name'=>'ASC']);
        foreach($categories as &$cat){
            if($cat->description)
                $cat->name.= ' ( ' . $cat->description . ' )';
            $cat->Id = $cat->id;
            $cat->Name = $cat->name;
        }
        unset($cat);
        array_unshift($categories, (object)[
            'Id'   => 0,
            'Name' => 'All'
        ]);
        $this->data['categories'] = $categories;

        $this->data['content'] = $this->load->view('product/ppob/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function item($id=null){
        $this->load->model('ppob/DigitalProductModel', '_Product');
        $this->load->model('ppob/DigitalTransactionModel', '_DTransaction');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->data = [];

        $product = $this->_Product->get($id);
        if(!$product)
            return $this->show_404();

        $product = $this->formatter->ppobproducts($product, false, [
            'category_id'
        ]);
        
        $this->data['product'] = $product;
        $this->data['purchased'] = $this->_DTransaction->countByCond(['status'=>'success', 'code'=>$product->code]);

        $this->data['purchases'] = [];

        $purchases = $this->_DTransaction->getByCond(['status'=>'success','code'=>$product->code], 13, 1, ['id'=>'DESC']);
        if($purchases){
            $this->data['purchases'] = $this->formatter->ppobdigitaltransactions($purchases, false, [
                'user_id'
            ]);
        }
        
        $this->data['content'] = $this->load->view('product/ppob/single', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function set(){
        $this->load->model('ppob/DigitalProductModel', '_Product');
        $result = ['error'=>false];

        $data = [];
        $code = null;

        $updatable_fields = [
            'price',
            'price_agent',
            'reseller_price',
            'profit_fee',
            'admin_fee',
            'base_price'
        ];

        foreach($_POST as $key => $val){
            if($key == 'code')
                $code = $val;
            elseif(in_array($key, $updatable_fields))
                $data[$key] = preg_replace('![^0-9]!', '', $val);
        }

        if(!$data){
            $result['error'] = true;
            $result['message'] = 'No data to update';
        }elseif(!$code){
            $result['error'] = true;
            $result['message'] = 'No code property found';
        }else{
            $this->_Product->setBy('code', $code, $data);
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }
}