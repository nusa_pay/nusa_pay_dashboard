<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends SB_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);

        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    public function edit($id){
        $this->load->model('banner/BannerModel', '_Banner');
        
        $this->load->library('NusaApi', 'nusaapi');
        $this->load->library('SiteForm', '', 'form');

        $this->form->setForm('/promotion/banner');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

        $this->data = [
            'title' => 'Create New Banner',
            'atoken' => $token,
            'upload_url' => $config['endpoint']['media'].'/api/upload'
        ];
        
        if($id){
            $object = $this->_Banner->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = 'Edit Banner';
        }else{
            $object = (object)array();
        }

        $this->data['banner'] = $object;
        $this->form->setObject($object);



        if(!($new_object=$this->form->validate($object))){
            $this->data['content'] = $this->load->view('promotion/banner/edit', $this->data, true);
            return $this->load->view('layouts/main', $this->data);
        }

        if($new_object === true)
            return redirect('promotion/banner', 301);

        if(!$id){
            $this->_Banner->create($new_object);
        }else{
            $this->_Banner->set($id, $new_object);
        }

        return redirect('promotion/banner', 301);
    }

    public function create(){
        $this->load->model('banner/BannerModel', '_Banner');
        $this->load->library('NusaApi', 'nusaapi');
        $this->load->config('nusaapi');

        $config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

        $file = $_FILES['file'] ?? null;
        if(!$file)
            return redirect('promotion/banner', 301);

        $type = $file['type'];
        if(!preg_match('!^image\/!', $type))
            return redirect('promotion/banner', 301);

        $post = array(
            'file' => new CURLFile($file['tmp_name'], $type, $file['name'])
        );

        $ch = curl_init($config['endpoint']['media'].'/api/upload');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $token
        ]);
        $result = curl_exec($ch);
        curl_close ($ch);

        if(!$result)
            deb('Error: ', $result);

        $result = json_decode($result);

        if(!$result->success)
            deb('Error: ' . $result->message);

        $file = $result->data->url;

        $this->_Banner->create(['image'=>$file]);

        return redirect('promotion/banner', 301);
    }

    public function index(){
        $this->load->model('banner/BannerModel', '_Banner');
        $this->data = [];

        $this->data['banners'] = $this->_Banner->getByCond([], true);

        $this->data['content'] = $this->load->view('promotion/banner/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function remove($id){
        $this->load->model('banner/BannerModel', '_Banner');

        $this->_Banner->remove($id);

        redirect('promotion/banner', 301);
    }
}