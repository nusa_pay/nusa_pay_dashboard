<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashback extends SB_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);

        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    public function filter(){
        $this->load->model('promotion/PromotionsModel', '_Promotions');
        
        $sortf = $this->input->get('sortField');
        $sortd = $this->input->get('sortOrder');

        $filters = [
            'Name'      => 'promotion_name',
            'Status'    => 'status',
            'DateFrom'  => 'date_from',
            'DateTo'    => 'date_to'
        ];

        $cond = [
            'status' => [0,1,6],
            'promotion_type'   => 3
        ];
        $sort = [];

        if($sortf && $sortd && isset($filters[$sortf]))
            $sort[ $filters[$sortf] ] = $sortd;

        foreach($filters as $qry => $loc){
            $val = $this->input->get($qry);
            if($val){
                if($qry == 'Status'){
                    if(in_array($val, [-1,1,6])){
                        if($val == -1)
                            $val = 0;
                        $cond[$loc] = $val;
                    }
                }else{
                    $cond[$loc] = $val;
                }
            }

        }
        $page = $this->input->get('pageIndex');
        if(!$page)
            $page = 1;
        $rpp  = $this->input->get('pageSize');
        if(!$rpp)
            $rpp = 20;

        $statuses = [
            0 => 'New',
            1 => 'Active',
            6 => 'Terminated'
        ];

        $cashbacks = $this->_Promotions->getByCond($cond, $rpp, $page, $sort);
        $result = [];
        if($cashbacks){
            $result = [];
            foreach($cashbacks as $cashback){
                $cashback->Status = (object)[
                    'Id'   => !$cashback->status ? -1 : $cashback->status,
                    'Name' => $statuses[$cashback->status]
                ];

                $result[] = [
                    'Id'       => $cashback->id,
                    'Status'   => $cashback->Status,
                    'Name'     => $cashback->promotion_name,
                    'Budget'   => number_format($cashback->budget),
                    'DateFrom' => $cashback->date_from,
                    'DateTo'   => $cashback->date_to
                ];
            }
        }

        $result = [
            'data'       => $result,
            'itemsCount' => $this->_Promotions->countByCond($cond)
        ];

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function item($id=null){
        $this->load->model('promotion/PromotionsModel', '_Promotions');
        $this->load->model('promotion/CashbacksModel', '_Cashbacks');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->data = [];

        $cashback = $this->_Promotions->getByCond([
            'promotion_type' => 3,
            'id' => $id
        ]);
        if(!$cashback)
            return $this->show_404();

        $cashback = $this->formatter->promocashback($cashback);
        
        $this->data['cashback'] = $cashback;
        $this->data['used'] = $this->_Cashbacks->countByCond(['promotion_id'=>$cashback->id]);

        $this->data['transactions'] = [];

        $transactions = $this->_Cashbacks->getByCond(['promotion_id'=>$cashback->id], 13, 1, ['id'=>'DESC']);
        if($transactions){
            $this->data['transactions'] = $this->formatter->promocashbacktrans($transactions, false, 'redeemed_by');
        }
        
        $this->data['content'] = $this->load->view('promotion/cashback/single', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function edit($id){
        $this->load->model('promotion/PromotionsModel', '_Promotions');
        $this->load->model('promotion/ProductFilterModel', '_PFilter');
        $this->load->model('ppob/DigitalProductModel', '_Product');
        
        $this->load->library('SiteForm', '', 'form');

        $this->form->setForm('/promotion/cashback');

        $this->data = [
            'title' => 'Create New Cashback',
            'products' => []
        ];
        
        if($id){
            $object = $this->_Promotions->get($id);
            if(!$object)
                return $this->show_404();

            $object->product = [];

            $to_int = [
                'max_claim_amount_per_user',
                'max_claim_amount_per_order',
                'minimum_transaction_amount',
                'promotion_value',
                'budget',
                'max_claim_per_user',
                'max_claim_per_day'
            ];
            foreach($to_int as $key)
                $object->$key = (int)$object->$key;

            $my_products = $this->_PFilter->getByCond(['promotion_id'=>$object->id], true);
            if($my_products)
                $object->product = array_column($my_products, 'product_id');

            $this->data['title'] = 'Edit Cashback';
        }else{
            $object = (object)array(
                'product' => []
            );
        }

        $products = $this->_Product->getByCond([], true, false, 'name');
        if($products)
            $this->data['products'] = $products;

        $this->data['cashback'] = $object;
        $this->form->setObject($object);

        if(!($new_object=$this->form->validate($object))){
            $this->data['content'] = $this->load->view('promotion/cashback/edit', $this->data, true);
            return $this->load->view('layouts/main', $this->data);
        }

        // if($new_object === true)
            // return redirect('promotion/cashback', 301);

        $products = $this->input->post('product');

        if(!$id && $new_object !== true){
            $new_object['promotion_type'] = 3;

            $id = $this->_Promotions->create($new_object);
            if($products){
                $pdss = [];
                foreach($products as $prd)
                    $pdss[] = ['promotion_id'=>$id,'product_id'=>$prd];
                if($pdss)
                    $this->_PFilter->create_batch($pdss);
            }
        }else{
            if($new_object !== true)
                $this->_Promotions->set($id, $new_object);

            if($products){
                $to_create = [];
                $to_remove = [];
                $exits_prds = $this->_PFilter->getByCond(['promotion_id'=>$id], true);
                if($exits_prds){
                    $exits_prds = array_column($exits_prds, 'product_id');
                    foreach($exits_prds as $pid){
                        if(!in_array($pid, $products))
                            $to_remove[] = $pid;
                    }
                    if($to_remove)
                        $this->_PFilter->removeByCond(['promotion_id'=>$id, 'product_id' => $to_remove]);
                }else{
                    $exits_prds = [];
                }

                foreach($products as $pid){
                    if(!in_array($pid, $exits_prds))
                        $to_create[] = ['promotion_id'=>$id, 'product_id'=>$pid];
                }
                if($to_create)
                    $this->_PFilter->create_batch($to_create);
            }
        }

        return redirect('promotion/cashback', 301);
    }

    public function index(){
        $this->load->model('promotion/PromotionsModel', '_Promotions');
        $this->data = [
            'statuses' => [
                '0' => 'New',
                '1' => 'Active',
                '6' => 'Terminated'
            ]
        ];

        $this->data['content'] = $this->load->view('promotion/cashback/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function terminate($id){
        $this->load->model('promotion/PromotionsModel', '_Promotions');

        $this->_Promotions->set($id, [
            'status' => 6
        ]);

        redirect('promotion/cashback', 301);
    }
}