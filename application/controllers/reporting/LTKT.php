<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LTKT extends SB_Controller {

  protected $layout   = "layouts/main";
  public $module     = 'Ltkt';
  public $per_page  = '10';

  function __construct() {
    parent::__construct();
    
    $this->load->model('reporting/LtktModel');
    $this->model = $this->LtktModel;
    
     $this->info = $this->makeInfo( $this->module);
     $this->access = $this->validAccess($this->info['id']);  
    
    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);
    
  }

	public function index()
	{
		if ($this->access['is_view'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }
    
		$this->data['cardTitle'] = "Reporting LTKT";
	    $this->data['cardDesc'] = "<a href='http://www.ppatk.go.id/pelaporan/read/50/pedoman-pelaporan.html' target='_blank'>ReadMe</a>";
	   // $this->data['listusers'] = json_encode($this->model->list());
	    $this->data['content'] = $this->load->view('reporting/Ltkt_view',$this->data, true );
	    
	    $this->load->view($this->layout, $this->data );
	}

	public function searchData()
	{
		$result = $this->model->searchData($this->input->post());

		echo json_encode($result);

	}

}

/* End of file LTKT.php */
/* Location: ./application/controllers/reporting/LTKT.php */