<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends SB_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            return redirect('user/login', 301);

        $this->load->library('ObjectFormatter', '', 'formatter');

        $this->load->library('Parsedown', '', 'markdown');
        $this->markdown->setSafeMode(true);
        $this->markdown->setMarkupEscaped(true);

        $this->load->model('ticket/TicketModel', '_Ticket');
        $this->load->model('ticket/TicketCategoryModel', '_TCategory');
        $this->load->model('ticket/TicketCommentModel', '_TComment');
        $this->load->model('ticket/TicketGuidelineModel', '_TGuideline');
        $this->load->model('ticket/TicketScopeModel', '_TScope');
        $this->load->model('ticket/TicketPriorityModel', '_TPriority');
        $this->load->model('main/UserModel', '_User');
    }

    public function activate($id){
        $ticket = $this->_Ticket->get($id);
        if(!$ticket)
            return $this->show_404();

        $this->_Ticket->set($id, [
            'status' => 2,
            'rejected_by' => null,
            'solving_time' => null,
            'resolved_by' => null
        ]);

        return redirect('ticket/item/'.$id, 303);
    }

    public function comment($id){
        $ticket = $this->_Ticket->get($id);
        if(!$ticket)
            return $this->show_404();

        if($ticket->status != 2)
            return redirect('ticket/item/'.$id, 303);

        $data = [
            'user'      => $this->session->userdata('uid'),
            'ticket'    => $ticket->id,
            'content'   => $this->input->post('content'),
            'attachment'=> $this->input->post('attachment')
        ];

        $this->_TComment->create($data);

        return redirect('ticket/item/'.$id, 303);
    }

    public function create(){
        $this->data = [
            'categories' => [],
            'priorities' => [],
            'scopes' => [],
            'users' => []
        ];

        $categories = $this->_TCategory->getByCond([], true, false, ['owner' => 'ASC', 'name'=>'ASC']);
        if($categories){
            $gcats = [];
            foreach($categories as $cat){
                if(!$cat->parent)
                    $cat->parent = 0;
                if(!isset($gcats[$cat->parent]))
                    $gcats[$cat->parent] = [];
                $gcats[$cat->parent][] = $cat;
            }
            $this->data['categories'] = $gcats;
        }

        $priorities = $this->_TPriority->getByCond([], true, false, ['id'=>'ASC']);
        if($priorities)
            $this->data['priorities'] = $priorities;

        $scopes = $this->_TScope->getByCond([], true, false, ['id'=>'ASC']);
        if($scopes)
            $this->data['scopes'] = array_column($scopes, 'label', 'id');

        $users = $this->_User->getByCond(['active'=>1], true, false, ['first_name'=>'ASC','last_name'=>'ASC']);
        if($users)
            $this->data['users'] = $users;

        if($this->input->method(TRUE) == 'POST'){
            $data = $this->input->post();
            $data['time_to_solve'] = 0;
            
            $category = $this->_TCategory->get($data['category']);
            $data['time_to_solve'] = time() + $category->time_to_solve;

            $data['issuer'] = $this->session->userdata('uid');

            $data['time_to_solve'] = gmdate('Y-m-d H:i:s', $data['time_to_solve']);

            $id = $this->_Ticket->create($data);
            return redirect('ticket/item/'.$id, 303);
        }

        $this->data['content'] = $this->load->view('ticket/create', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function index(){
        $status   = $this->input->get('status');
        $q        = $this->input->get('q');
        $priority = $this->input->get('priority');

        $this->data = array(
            'resume' => (object)[
                'total'     => 0,
                'active'    => 0,
                'resolved'  => 0,
                'rejected'  => 0
            ],
            'status'   => $status,
            'q'        => (string)$q,
            'priority' => $priority,
            'tickets'  => [],
            'pagination' => []
        );

        $totals = $this->_Ticket->countGroupedBy('status', (object)['>',0], 'status');
        
        if($totals){
            $total = 0;
            foreach($totals as $rsts => $rttl){
                $total+= $rttl;
                switch($rsts){
                    case 1:
                        $this->data['resume']->rejected = $rttl;
                        break;
                    case 2:
                        $this->data['resume']->active = $rttl;
                        break;
                    case 3:
                        $this->data['resume']->resolved = $rttl;
                        break;
                }
            }
            $this->data['resume']->total = $total;
        }

        $cond = $pcond = [];
        $cond['status'] = (object)['>',0];
        if($q){
            $pcond['q'] = $q;
            if(is_numeric($q))
                $cond['id'] = $q;
            else
                $cond['subject'] = (object)['LIKE', $q];
        }
        if($status)
            $pcond['status'] = $cond['status'] = $status;
        if($priority)
            $pcond['priority'] = $cond['priority'] = $priority;

        $rpp = 20;
        $page = max( $this->input->get('page', 1), 1 );

        $tickets = $this->_Ticket->getByCond($cond, $rpp, $page);
        if($tickets){
            $tickets = $this->formatter->ticket($tickets, false, [
                'category' => ['parent'],
                'priority',
                'status'
            ]);
            $this->data['tickets'] = $tickets;
        }

        $total = $this->_Ticket->countByCond($cond);
        if($total)
            $this->data['pagination'] = calculate_pagination($total, $page, $rpp, $pcond);
        
        $this->data['content'] = $this->load->view('ticket/index', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }

    public function resolve($id){
        $ticket = $this->_Ticket->get($id);
        if(!$ticket)
            return $this->show_404();

        $this->_Ticket->set($id, [
            'status' => 3,
            'resolved_by' => $this->session->userdata('uid'),
            'solving_time' => date('Y-m-d H:i:s')
        ]);

        return redirect('ticket/item/'.$id, 303);
    }

    public function reject($id){
        $ticket = $this->_Ticket->get($id);
        if(!$ticket)
            return $this->show_404();

        $this->_Ticket->set($id, [
            'status' => 1,
            'rejected_by' => $this->session->userdata('uid')
        ]);

        return redirect('ticket/item/'.$id, 303);
    }

    public function single($id){
        $this->data = array(
            'ticket' => null,
            'guidelines' => [],
            'comments' => []
        );

        $ticket = $this->_Ticket->get($id);
        if(!$ticket)
            return $this->show_404();

        // get the guideline
        if($ticket->category){
            $guidelines = $this->_TGuideline->getBy('category', $ticket->category, true);
            $guidelines = $this->formatter->ticket_guideline($guidelines);
            if($guidelines){
                foreach($guidelines as &$gl)
                    $gl->content = $this->markdown->text($gl->content);
            }
            unset($gl);
            $this->data['guidelines'] = $guidelines;
        }

        // get commentts
        $comments = $this->_TComment->getBy('ticket', $ticket->id, true, false, ['id'=>'DESC']);
        if($comments){
            $comments = $this->formatter->ticket_comment($comments, false, [
                'user'
            ]);
            foreach($comments as &$cm)
                $cm->content = $this->markdown->text($cm->content);
            unset($cm);
            $this->data['comments'] = $comments;
        }

        $ticket = $this->formatter->ticket($ticket, false, [
            'issuer',
            'category' => ['parent'],
            'priority',
            'status',
            'rejected_by',
            'resolved_by',
            'assigned_to'
        ]);
        $ticket->body = $this->markdown->text($ticket->body);
        if($ticket->scope){
            $scopes = explode(',', $ticket->scope);
            $ticket->scope = $this->_TScope->get($scopes, true, false, ['id' => 'ASC']);
        }
        $this->data['ticket'] = $ticket;

        $this->data['content'] = $this->load->view('ticket/single', $this->data, true);
        $this->load->view('layouts/main', $this->data);
    }
}