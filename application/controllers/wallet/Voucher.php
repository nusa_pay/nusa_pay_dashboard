<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends SB_Controller {

  protected $layout   = "layouts/main";
  public $module     = 'voucher';
  public $per_page  = '10';

  public function __construct()
  {
  	parent::__construct();
  	$this->load->model('wallet/VoucherModel');
	    $this->model = $this->VoucherModel;
	    
	     $this->info = $this->makeInfo( $this->module);
	     $this->access = $this->validAccess($this->info['id']);  
	    
	    
	    if(!$this->session->userdata('logged_in')) redirect('user/login',301);
  }

	public function index()
	{
		if ($this->access['is_view'] == 0) {
       SiteHelpers::alert('error','Your are not allowed to access the page');
       redirect('dashboard',301);
    }
    
		$this->data['cardTitle'] = "Voucher Create and Redeem";
	    $this->data['cardDesc'] = "-";
	   // $this->data['listusers'] = json_encode($this->model->list());
	    $this->data['content'] = $this->load->view('wallet/voucherView',$this->data, true );
	    
	    $this->load->view($this->layout, $this->data );
	}

	public function searchData()
	{
		$result = $this->db->get("nusa_voucher_view")->result_array();

		echo json_encode($result);

	}



}

/* End of file Voucher.php */
/* Location: ./application/controllers/wallet/Voucher.php */