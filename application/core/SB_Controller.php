<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

class SB_Controller extends CI_Controller
{

	var $data = array();
 
	function __construct() {
		parent::__construct();

		if($this->session->userdata('lang') =='')
		{
			$this->session->set_userdata('lang','en');
			$this->load->language('core','en');
		} else {
			$this->load->language('core',$this->session->userdata('lang'));
		}
		
		$this->data['content'] = 'Welcome NUSAPAY REPORTING DASHBOARD';
		$this->data['page'] = $this->input->get('page',true);
		$this->data['basic_url'] = $this->uri->segment(1) . "/" . $this->uri->segment(2);

		$this->statusWallet['deleted'] = '0';
        $this->statusWallet['cancel'] =  1;
        $this->statusWallet['waiting'] = 2;
        $this->statusWallet['success'] = 3;

		//$this->data['custom_js'] = "";
		// init for upload library
		$upload_config = array();
		$this->load->library('upload', $upload_config );
 
		$imagelib_config = array();
		$this->load->library('image_lib', $imagelib_config );
 
	}

	
	function displayError($data)
	{
		$this->load->view('layouts/errors',$data);
	}

	function makeInfo( $id )
	{
		
		$query =  $this->db->get_where('tb_module', array('module_name'=> $id));
		$data = array();
		foreach($query->result() as $r)
		{
			$data['id']		= $r->module_id;
			$data['title'] 	= $r->module_title;
			$data['note'] 	= $r->module_note;
			$data['table'] 	= $r->module_db;
			$data['key'] 	= $r->module_db_key;
			//$data['config'] = SiteHelpers::CF_decode_json($r->module_config);
			// $field = array();
			// foreach($data['config']['grid'] as $fs)
			// {
			// 	foreach($fs as $f)
			// 		$field[] = $fs['field'];

			// }
			// $data['field'] = $field;

		}
		$query->free_result();
		return $data;


	}

	function validAccess( $id)
	{

		$query = $this->db->get_where('tb_groups_access', array(
		  'module_id' => $id ,
			'group_id' => $this->session->userdata("gid"),
		));
		
		$row = $query->result();
		$query->free_result();

		if(count($row) >= 1)
		{
			$row = $row[0];
			if($row->access_data !='')
			{
				$data = json_decode($row->access_data,true);
			} else {
				$data = array();
			}
			return $data;

		} else {
			return false;
		}

	}

	function getGroupUser()
	{
		$group = $this->db->get('tb_groups');

		return $group->result();

	}

	public function upload_image()
	{
		$this->load->library('NusaApi', 'nusaapi');
        $this->load->library('SiteForm', '', 'form');
		
		$config = config_item('nusaapi');

        $token = $this->nusaapi->getServerToken();

        $this->data = [
            'title' => 'Create New Banner',
            'atoken' => $token,
            'upload_url' => $config['endpoint']['media'].'/api/upload'
        ];

        

		print_r($_FILES);
		exit;
	}
	


    function show_404(){
        $this->output->set_status_header('404');
        $params = array(
            'title' => 'Page not found'
        );

        $this->load->view('404', $params);
    }
}




?>