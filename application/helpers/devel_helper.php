<?php

function deb(){
    $args = func_get_args();
    ob_start();
    
    echo '<pre>';
    foreach($args as $arg){
        if(is_null($arg)){
            echo 'NULL';
        }elseif(is_bool($arg)){
            echo $arg ? 'TRUE' : 'FALSE';
        }else{
            $arg = print_r($arg, true);
            $arg = hs($arg);
            echo $arg;
        }
        echo PHP_EOL;
    }
    echo '</pre>';
    
    $ctx = ob_get_contents();
    ob_end_clean();
    
    echo $ctx;
    exit;
}

function hs(string $str): string{
    return htmlspecialchars($str, ENT_QUOTES);
}