<?php

if(!defined('BASEPATH'))
    die;

class MediaFile
{
    private $CI;
    
    function __construct(){
        $this->CI =&get_instance();
        
    }
    
    /**
     * Make the name and create the folder of the file.
     * @param string name The file name to prepare.
     * @return array list of the new media data to be use.
     *  @param string local_file_name The file name without extension to use.
     *  @param string local_file The file with extension to use.
     *  @param string local_file_ext The extension of the file.
     *  @param string local_file_path Absolute path to the file.
     *  @param string local_file_folder Absolute path to the file folder.
     *  @param string local_media_folder Relative path to the file folder ( use this on website )
     *  @param string local_media_file Relative path to the file.
     */
    private function _prepareTargetDir($name){
        $exts = explode('.', $name);
        $ext  = end($exts);
        
        $local_file_name = md5(time() . '-' . uniqid());
        $local_file      = $local_file_name . '.' . $ext;
        $local_media     = '/media/';
        $local_path_abs  = dirname(BASEPATH);
        $local_path      = $local_path_abs . $local_media;
        
        for($i=0; $i<3; $i++){
            $sub_path = substr($local_file_name, ($i*2), 2);
            $local_media.= "$sub_path/";
            $local_path = $local_path_abs . $local_media;
            if(!is_dir($local_path))
                mkdir($local_path);
        }
        
        $result = array(
            'local_file_name'   => $local_file_name,
            'local_file'        => $local_file,
            'local_file_ext'    => $ext,
            'local_file_path'   => $local_path . $local_file,
            'local_file_folder' => $local_path,
            'local_media_folder'=> $local_media,
            'local_media_file'  => $local_media . $local_file,
            'original_name'     => $name
        );
        
        if(is_file($result['local_file_path'])){
            sleep(1);
            return $this->_prepareTargetDir($name);
        }
        
        return $result;
    }

    /**
     * Process local file.
     * @param string path Absolute path to local file.
     * @param string name The real name of the file.
     * @param string type The media type. The name of rule on form_validation.
     * @param integer object The object this file belong to.
     * @return as of _prepareTargetDir
     */
    public function processLocal($path, $name, $type, $object=null){
        $target = $this->_prepareTargetDir($name);
        
        copy($path, $target['local_file_path']);
        
        $target['media_id'] = $this->_saveToDB($target, $type, $object);
        return $target;
    }
    
    /**
     * Process Upload
     * @param string name The input form name.
     * @param string filename The original file name.
     * @param string type The media type. The name of rule on form_validation.
     * @param integer object The object id this file belongs to.
     * @return as of _prepareTargetDir
     */
    public function processUpload($name, $filename){
        $target = $this->_prepareTargetDir($filename);

        $config['upload_path']   = $target['local_file_folder'];
        $config['allowed_types'] = '*';
        $config['file_name']     = $target['local_file'];
        $config['file_ext_tolower'] = true;
        
        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);
        
        if(!$this->CI->upload->do_upload($name))
            return $this->CI->upload->display_errors('','');

        return $target;
    }
}