<?php

class NusaApi
{
    function __construct(){
        $this->CI =&get_instance();
        $this->CI->load->config('nusaapi');
    }

    public function getServerToken(){
        $config = config_item('nusaapi');

        $data = [
            'grant_type'    => 'client_credentials',
            'client_id'     => $config['id'],
            'client_secret' => $config['secret'],
        ];

        $json = json_encode($data);

        $ch = curl_init($config['endpoint']['auth'] . '/auth/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: applicationn/json',
            'Accept: application/json',
            'Content-Length: ' . strlen($json)
        ]);

        $response = curl_exec($ch);
        curl_close($ch);

        $token = json_decode($response);

        if(isset($token->error))
            return;

        return $token->access_token;
    }
}