<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groupsmodel extends SB_Model 
{

	public $table = 'tb_groups';
	public $primaryKey = 'group_id';
	public $data = array();
	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT  
	tb_groups.group_id,
	tb_groups.name,
	tb_groups.description,
	tb_groups.level,
	tb_groups.active


FROM tb_groups   ";
	}
	public static function queryWhere(  ){
		
		return "   WHERE tb_groups.group_id IS NOT NULL    ";
	}
	
	public static function queryGroup(){
		return "     ";
	}


	public function getUserGroups()
	{
		// 1 is superadmin
		$this->db->where('group_id !=', 1);
		
		$get = $this->db->get($this->table);

		if ($get->num_rows() > 0) {
            # code...
			$this->data = $get->result_array();
		}

		return $this->data;


	}

	function list(){
		$get = $this->db->get($this->table);

		return $get->result_array();
	}
	
}

?>
