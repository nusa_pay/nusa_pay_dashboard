<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MasterMerchantRequestModel extends CI_Model {

public function __construct()
{	
   
		
        parent::__construct();
        $this->table = "master_request";
        $this->_db = $this->load->database("nusamerchants_request",TRUE);
        $this->type = [
            0 => "type_of_request",
            1 => "company_residence",
            2 => "type_of_company",
            3 => "cooperation_purpose",
            4 => "operating_transaction_per_month",
            5 => "bussiness_status",
            6 => "bussiness_type",
            7 => "bussiness_location",
            8 => "avg_trx"
        ];
    
}

function type_of_request()
{
	$this->_db->where('request_type_name', $this->type[0]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function company_residence()
{
	$this->_db->where('request_type_name',  $this->type[1]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function type_of_company()
{
	$this->_db->where('request_type_name',  $this->type[2]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function cooperation_purpose()
{
	$this->_db->where('request_type_name',  $this->type[3]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function operating_transaction_per_month()
{
	$this->_db->where('request_type_name',  $this->type[4]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function bussiness_status()
{
	$this->_db->where('request_type_name',  $this->type[5]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function bussiness_type()
{
	$this->_db->where('request_type_name',  $this->type[6]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function bussiness_location()
{
	$this->_db->where('request_type_name',  $this->type[7]);
	$res = $this->_db->get($this->table);

	return $res->result();
}

function avg_trx()
{
	$this->_db->where('request_type_name',  $this->type[8]);
	$res = $this->_db->get($this->table);

	return $res->result();
}


}

/* End of file MasterMerchantRequestModel.php */
/* Location: ./application/models/MasterMerchantRequestModel.php */