<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NusaCartModel extends CI_Model {

public function __construct()
{
	parent::__construct();
	//Do your magic here
	$this->cartDb = $this->load->database("cart",TRUE);
	$this->view = "nusa_cart_view";
	$this->viewDetail = "nusa_cart_detail";
	$this->viewMostItem = "nusa_cart_most_selling_item";
	$this->kycStatus = array('not_request' =>0 , 'request' => 1, 'approve' => 2 );
	$this->status = array('deleted' =>0 , 'cancel' => 1, 'waiting' => 2, 'success' => 3 );
	$this->data = array();
}

public function getCartList()
{
	$this->db->select("id,user,users_name,subtotal,promo_amount,discount,fee,tax,total,description,created,status_kyc,
			CASE
			WHEN status_kyc = 0 then 'BELUM REQUEST' 
			WHEN status_kyc = 1 then 'SUDAH PENGAJUAN REQUEST' 
			WHEN status_kyc = 2 then 'APPROVE' 
			END  AS status_kyc_name,
			status,
			CASE
			WHEN status = 0 then 'CANCELED' 
			WHEN status = 1 then 'NEW' 
			WHEN status = 2 then 'PENDING'
			WHEN status = 3 then 'DONE'
			END  AS status_name");
		$res = $this->db->get($this->view);

		return $res->result_array();
}

public function getCartDetail($cart_id)
{
		$data = array();
		$this->db->select("product,product_snap,price,quantity,subtotal,fee,tax,total,merchant_name,created");
		$this->db->where('cart', $cart_id);
		$res = $this->db->get($this->viewDetail);

		if ($res->num_rows() > 0) {
			foreach ($res->result_array() as $key => $val) {
				$data[$key] = $val;
				$data[$key]['product_snap_name'] = "";
				if ($val['product_snap'] != null || $val['product_snap'] != "" ) {
					$data[$key]['product_snap_name'] = json_decode($val['product_snap'])->name;
				}
			}
			
		}

		return $data;
}

public function getMostSellingItem($param = null)
{
	$data=array();

	// $sql = "SELECT
	// 		merchant,
	// 		product,
	// 		product_snap,
	// 		SUM(quantity) AS TotalQuantity,
	// 		SUM(total) AS TotalIdr
	// 	FROM
	// 		`cart_item`
	// 	GROUP BY
	// 		product,merchant
	// 	ORDER BY
	// 		TotalQuantity DESC";

	$res = $this->db->get($this->viewMostItem);

	if ($res->num_rows() > 0) {
		foreach ($res->result_array() as $key => $val) {
			$data[$key] = $val;
				$data[$key]['product_snap_name'] = "";
				if ($val['product_snap'] != null || $val['product_snap'] != "" ) {
					$data[$key]['product_snap_name'] = json_decode($val['product_snap'])->name;
				}
		}
	}

	return $data;

}
	

}

/* End of file NusaCartModel.php */
/* Location: ./application/models/NusaCartModel.php */