<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class NusaMerchantRequestModel extends CI_Model {

public function __construct()
{	
   
		
        parent::__construct();
        $this->table_master_request = "master_request";
        $this->table_merchants = "merchants";
        $this->table_merchants_information = "information";
        $this->table_merchants_address = "addresses";
        $this->table_merchants_dc = "director_commissioners";
        $this->table_merchants_owners = "owners";
        $this->table_merchants_contracts = "contracts";
        $this->table_merchants_accounts = "accounts";
        $this->_db = $this->load->database("nusamerchants_request",TRUE);
        $this->type = [
            0 => "type_of_request",
            1 => "company_residence",
            2 => "type_of_company",
            3 => "cooperation_purpose",
            4 => "operating_transaction_per_month",
            5 => "bussiness_status",
            6 => "bussiness_type",
            7 => "bussiness_location",
            8 => "avg_trx",
            9 =>"bussiness_omzet"
        ];
        $this->data = array();
    
}

function type_of_request()
{
	$this->_db->where('request_type_name', $this->type[0]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function company_residence()
{
	$this->_db->where('request_type_name',  $this->type[1]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function type_of_company()
{
	$this->_db->where('request_type_name',  $this->type[2]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function cooperation_purpose()
{
	$this->_db->where('request_type_name',  $this->type[3]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function operating_transaction_per_month()
{
	$this->_db->where('request_type_name',  $this->type[4]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function bussiness_status()
{
	$this->_db->where('request_type_name',  $this->type[5]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function bussiness_type()
{
	$this->_db->where('request_type_name',  $this->type[6]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function bussiness_location()
{
	$this->_db->where('request_type_name',  $this->type[7]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function bussiness_omzet()
{
	$this->_db->where('request_type_name',  $this->type[9]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function avg_trx()
{
	$this->_db->where('request_type_name',  $this->type[8]);
	$res = $this->_db->get($this->table_master_request);

	return $res->result();
}

function getMerchantRequest() 
{
	$this->_db->select("*,CASE
			WHEN status = 0 then 'PROSES APPROVE' 
			WHEN status = 1 then 'APPROVE'
			WHEN status = 2 then 'REJECT'
			END  AS status_view");
	//$this->_db->where('status','0');
	$res = $this->_db->get($this->table_merchants);

	if ($res->num_rows() > 0) {
		$this->data = $res->result_array();
	}

	return $this->data;
}

function insert_merchants_table($data = array())
{
	$insert = $this->_db->insert($this->table_merchants, $data);
	return $this->_db->insert_id();
}

function insert_merchants_information_table($data = array())
{
	return $this->_db->insert($this->table_merchants_information, $data);
}

function insert_merchants_address($data = array())
{
	return $this->_db->insert($this->table_merchants_address, $data);
}

function updateApprove($id=null,$data= array())
{
	$this->_db->where('id', $id);
	$this->_db->update($this->table_merchants, $data);
}


}

/* End of file MasterMerchantRequestModel.php */
/* Location: ./application/models/MasterMerchantRequestModel.php */