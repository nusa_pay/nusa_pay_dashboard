<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NusaPromotionUsersModel extends CI_Model {

public function __construct()
{
	parent::__construct();
	//Do your magic here
	$this->walletDb = $this->load->database("ewallet",TRUE);
	$this->view = "nusa_promotion_user_view";
	$this->status = array('deleted' =>0 , 'cancel' => 1, 'waiting' => 2, 'Success' => 3 );
	$data = array();
}

public function getTotalPromotionUser($userId)
{
	$query = "SELECT
				SUM(available_point) AS available_point,
				SUM(spent_coin) AS spent_coin,
				SUM(spent_point) AS spent_point,
				SUM(collected_coin) AS collected_coin,
				SUM(collected_point) AS collected_point,
				SUM(expired_point) AS expired_point,
				SUM(available_coin) AS available_coin
				FROM
					`nusa_promotion_user_view`
				WHERE
					user_id =".$userId;
	
	$get = $this->db->query($query);


	if ($get->num_rows() > 0) {
		$this->data = $get->row_array();
	}

	return $this->data;

}

public function getListPromotionUsers($userId = null)
{
	if ($userId != null) {
		$this->db->where('user_id', $userId);	
	}

	$get = $this->db->get($this->view);

	if ($get->num_rows() > 0) {

		$this->data = $get->result_array();
		
	}

	return $this->data;
}

	

}

/* End of file NusaPromotionUsersModel.php */
/* Location: ./application/models/NusaPromotionUsersModel.php */