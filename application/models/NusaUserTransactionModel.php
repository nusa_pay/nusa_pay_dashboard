<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NusaUserTransactionModel extends CI_Model {
	
	public function __construct()
	{
		
		parent::__construct();
		
		//Do your magic here
		$this->nusaUsersDb = $this->load->database("nusausers",TRUE);
		$this->billerDb = $this->load->database("biller",TRUE);
		$this->view = "nusa_user_cart_view";
		
		$data = array();
		
    }
    
    public function getTrxAllUser()
	{
		
		//$get = $this->nusaUsersDb->get("users");
		$get = $this->db->get($this->view);
		
		if ($get->num_rows() > 0) {	
			# code...
			$this->data = $get->result_array();
			
		}
		return $this->data;
		
	}

	
	public function getTrxByUser($id=null)
	{    
		$this->db->select('cart_id,status_cart,created,description,total_cart');
        $this->db->where('user', $id);
        
		//$get = $this->nusaUsersDb->get("users");
		$this->db->order_by('created', 'desc');
		$get = $this->db->get($this->view);
		
		if ($get->num_rows() > 0) {	
			# code...
			$this->data = $get->result_array();
			
		}
		return $this->data;
		
	}

	public function getTotalTrxByUser($id=null,$status = null)
	{    
		$this->db->select('total_cart,status_cart');
        $this->db->where('user', $id);
        if($status != null || $status != '')
        {
        $this->db->where('status_cart', $status);
        }
        $total = 0;
		//$get = $this->nusaUsersDb->get("users");
		$get = $this->db->get($this->view);
		
		if ($get->num_rows() > 0) {	
			# code...
			foreach ($get->result_array() as $value) {
				$total += $value['total_cart'];	
			}

			
		}
		return $total;
		
	}

	public function getUserTrxBiller($userId)
	{
		
	}

	public function getReportTrxUser($param = array())
	{
		$result= array();
		$query = "SELECT
			`name`,
			users_id,
			user_type,
			CASE
			WHEN user_type = 1 then 'REGULER' 
			WHEN user_type = 2 then 'AGENT'
			WHEN user_type = 3 then 'MERCHANT' 
			WHEN user_type = 4 then 'ORGANISASI/VIP'
			END  AS user_type_name,
			status_kyc,
			CASE
			WHEN status_kyc = 0 then 'BELUM REQUEST' 
			WHEN status_kyc = 1 then 'SUDAH PENGAJUAN REQUEST' 
			WHEN status_kyc = 2 then 'APPROVE' 
			END  AS status_kyc_name,
			SUM(total_cart) AS total_trx
		FROM
			`nusa_user_cart_view`
			 WHERE
		 	created_cart >= '".$param['dateFrom']."'
			AND created_cart <= '".$param['dateTo']."'
		GROUP BY
			users_id
			HAVING
			total_trx >".$param['amount'];
			
			$get = $this->db->query($query);

			if ($get->num_rows() > 0) {
				
				$result = $get->result_array();
			}


			return $result;

	}
	

}



/* End of file ModelName.php */

