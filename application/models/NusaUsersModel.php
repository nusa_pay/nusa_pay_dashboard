<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NusaUsersModel extends CI_Model {
	
	public function __construct()
	{
		
		parent::__construct();
		
		//Do your magic here
		$this->nusaUsersDb = $this->load->database("nusausers",TRUE);
		$this->userInformationView = "nusa_users_informations_view";
		$this->addressView = "nusa_users_address_view";
		$this->kycStatus = array('not_request' =>0 , 'request' => 1, 'approve' => 2, 'rejected' => 3 );
		$this->userStatus = array('not_active' =>0 , 'active' => 1, 'suspend' => 2 );
		$data = array();
		
	}
	
	public function getAllUsers()
	{
		$this->db->select('id,name, phone, email, username,created_at,status,user_type,user_type_name,user_status_name');
		//$get = $this->nusaUsersDb->get("users");
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->result_array();
			
		}
		return $this->data;
		
	}
	public function getMerchantUser()
	{
		$this->db->select('id,name, phone, email, username,created_at,status,user_type,user_type_name,user_status_name');
		$this->db->where('user_type', 3);
		//$get = $this->nusaUsersDb->get("users");
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->result_array();
			
		}
		return $this->data;
		
	}
	public function countUser($param = null)
	{
		
		$this->db->select('id');
		
		if ($param != null || $param == 0) {
			# code...
				
			$this->db->where('status', $param);
		}
		
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->num_rows();
			
		}
			
		return $this->data;
		
	}
		public function countStatusKyc($param = null)
	{
		
		$this->db->select('id');
		
		if ($param != null || $param == 0) {
			# code...
				
			$this->db->where('status_kyc', $param);
		}
		
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->num_rows();
			
		}
			
		return $this->data;
		
	}
	public function countNonKyc()
	{
		
		$this->db->select('id');
		$this->db->where('status_kyc !=', 2);
		
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->num_rows();
			
		}
			
		return $this->data;
		
	}
	public function getDetailUser($id)
	{
		//$this->db->select('*');
		//$get = $this->nusaUsersDb->get("users");
		$this->db->where('id', $id);
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->row_array();
			
		}
		return $this->data;
		
	}
	public function getAdressUser($id)
	{
		$this->data['address'] = "";
		$this->data['address_name'] = "";
		$this->data['phone_address'] = "";
		$this->data['provinces'] = "";
		$this->data['cities'] = "";
		$this->data['subdistrict'] = "";
		$this->data['villages'] = "";
		$this->data['postal_code'] = "";
		$this->db->where('id', $id);
		$get = $this->db->get($this->addressView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->row_array();
		}
		return $this->data;
		
	}
	public function getListKycRequest()
	{
		$this->db->select('id,name, phone, email, username,created_at,status,user_type,user_type_name,user_status_name','status');
		$this->db->where('status_kyc', $this->kycStatus['request']);
		
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->result_array();
			
		}
		return $this->data;
	}
	public function getUserKycDetail($id = null)
	{
		$this->db->select('id,name, phone, email, username,created_at,status,user_type,user_type_name,user_status_name,
		status,avatar,photo,indentity_image,indentity_number,indentity_type,mother_name');
		//$this->db->where('status_kyc', $this->kycStatus['request']);
		$this->db->where('id', $id);
		
		$get = $this->db->get($this->userInformationView);
		
			if ($get->num_rows() > 0) {
			# code...
			$this->data = $get->row_array();
			
		}
		return $this->data;
	}
	public function updateVerifyKyc($id,$data,$type=null)
	{
		if ($type == "REJECT") {
			$data['status'] = $this->kycStatus['rejected'];
		} else {
			$data['status'] = $this->kycStatus['approve'];
		}
	
		$this->nusaUsersDb->where('user_id',$id);
		$ret = $this->nusaUsersDb->update('user_informations', $data);
		//create log
		$this->createLogKyc($id,$data);
		
		return $ret;
		
	}
	public function createLogKyc($id,$data)
	{
		$ret = false;
		$object['operator'] = $this->session->userdata('uid');
		$object['user'] = $id;
		$object['created'] = date('y-m-d H:i:s');
		$insert = $this->nusaUsersDb->insert('kyc.kyc_log', $object);
		
		if ($insert) {
			# code...
			$ret = true;
		}
		
		return $ret;
		
	}

	public function getMasterLocationUser()
	{
		$city = array();
		$result_city = array();
		$city_count = 0;
		
		$this->db->where('location !=', null);
		$this->db->where('location !=', '');
		$get = $this->db->get($this->userInformationView);
		if ($get->num_rows() > 0) {
			foreach ($get->result_array() as $key => $value) {
				
				$city[$key] = json_decode($value['location'])->city;
				
			}
		
			
		$result_city =array_values(array_unique($city));
		$city_count = array_count_values($city);
		}
		return $city_count;
		
	}

	public function getListProfilling()
	{
		$get = $this->db->get('nusa_user_master_profilling_view');
		return $get->result_array();
	}

	public function getProvince()
	{
		$res = $this->nusaUsersDb->get("provinces");
		return $res->result();
	}

	public function getReferrals($id)
	{
		$sql  = "SELECT * FROM users WHERE referral_by=(SELECT referral_code FROM users WHERE id=".$id.")";
		$res = $this->nusaUsersDb->query($sql);
		if ($res->num_rows() > 0) {
			$this->data = $res->result_array();
		}
		return $this->data;
	}

	public function getReferralsBy($id)
	{
		$user = $this->getDetailUser($id);
		$this->db->where('referral_by', $user['referral_code']);
		$res = $this->db->get($this->userInformationView);
		
		if ($res->num_rows() > 0) {
			$this->data = $res->row_array();
		}
		return $this->data;
	}
	
	public function suspendUser($id)
	{
		
		$this->nusaUsersDb->where('id', $id);
		$row = $this->nusaUsersDb->get('users')->row();
		if ($row->status == 2) {
			$data['status'] = 1;
		} else $data['status'] = 2;
		$update = $this->nusaUsersDb->update('users',$data);

		return $update;
	}

	function update_profiling($id, $data)
	{
		
		$this->nusaUsersDb->where('user_id', $id);
		$update = $this->nusaUsersDb->update('user_informations',$data);

		return $update;	
	}	
		

	
}
/* End of file ModelName.php */