<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NusaUsersTransferModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->walletDb = $this->load->database("ewallet",TRUE);
		$this->view = "nusa_user_wallet_transfer_view";
		$this->topUp_table = "wallet_topup";
		$this->transaction_table = "wallet_transaction";
		$this->kycStatus = array('not_request' =>0 , 'request' => 1, 'approve' => 2 );
		$this->status = array('deleted' =>0 , 'cancel' => 1, 'waiting' => 2, 'success' => 3 );
		$this->trx_type = array('transfer' =>2 , 'top_up' => 3, 'withdraw' => 4);
		$data = array();
	}

	public function getTransferlist($value='')
	{
		$this->db->select("id,type,from_name,to_name,amount,total,description,created,
			CASE
			WHEN type = 1 then 'TRANSFER IN' 
			WHEN type = 2 then 'TRANSFER OUT' 
			END  AS type_name,
			status,
			CASE
			WHEN status = 0 then 'DELETED' 
			WHEN status = 1 then 'CANCEL' 
			WHEN status = 2 then 'WAITING'
			WHEN status = 3 then 'SUCCESS'
			END  AS status_name");
		$res = $this->db->get($this->view);

		return $res->result_array();
	}

	public function getCountByStatus($status)
	{

			$this->db->select('COUNT(*) AS rowCount');
			$this->db->where('status', $status);
			$this->data = $this->db->get($this->view)->row_array()['rowCount'];

		return $this->data;
		
	}

	public function getTotalByStatus($status)
	{

			$this->db->select('SUM(total) AS sum_total');
			$this->db->where('status', $status);
			$this->data = $this->db->get($this->view)->row_array()['sum_total'];

		return "Rp. ".number_format($this->data,2);
		
	}




}

/* End of file NusaUsersWalletModel.php */
/* Location: ./application/models/NusaUsersWalletModel.php */