<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NusaUsersWalletModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->walletDb = $this->load->database("ewallet",TRUE);
		$this->view = "";
		$this->topUp_table = "wallet_topup";
		$this->transaction_table = "wallet_transaction";
		$this->kycStatus = array('not_request' =>0 , 'request' => 1, 'approve' => 2 );
		$this->status = array('deleted' =>0 , 'cancel' => 1, 'waiting' => 2, 'Success' => 3 );
		$this->trx_type = array('transfer' =>2 , 'top_up' => 3, 'withdraw' => 4);
		$data = array();
	}

	public function getUserBalance($userId)
	{
		$this->walletDb->where('user', $userId);
		$get = $this->walletDb->get('wallet');

		if ($get->num_rows() > 0) {

			$this->data = $get->row_array();
			
		}

		return $this->data;
	}

	public function getTransactionWalletByUser($userId)
	{
		$walletId = 0;
		$getWallet = $this->getUserBalance($userId);
		$walletId = $getWallet['id'];

		$this->walletDb->where('wallet', $walletId);
		$this->walletDb->order_by('id', 'desc');

		$get = $this->walletDb->get($this->transaction_table);

		if ($get->num_rows() > 0) {

			//$this->data = $get->result_array();
			foreach ($get->result_array() as $key => $value) {
				$data[$key]['id'] = $value['id'];
				$data[$key]['wallet'] = $value['wallet'];
				$data[$key]['amount'] = $value['amount'];
				$data[$key]['fee'] = $value['fee'];
				$data[$key]['total'] = $value['total'];
				$data[$key]['reason'] = $value['reason'];
				$data[$key]['location'] = $value['location'];
				$data[$key]['description'] = $value['description'];

				if ($value['type'] == 4) {
					$data[$key]['type_name'] = "WITHDRAW";
				} else if ($value['type'] == 3) {
					$data[$key]['type_name'] = "TOP UP";
				} else if ($value['type'] == 2) {
					$data[$key]['type_name'] = "TRANSFER";
				} else {
					$data[$key]['type_name'] = "-";

				}

				if ($value['status'] == 3) {
					$data[$key]['status_name'] = "SUCCESS";
				} else if ($value['status'] == 2) {
					$data[$key]['status_name'] = "WAITING";
				} else if ($value['status'] == 1) {
					$data[$key]['status_name'] = "CANCELLED";
				} else {
					$data[$key]['status_name'] = "CANCELLED";

				}

				$data[$key]['updated'] = $value['updated'];
				$data[$key]['created'] = $value['created'];
			}

			$this->data = $data; 
		}

		return $this->data;

	}

	public function getTopUpUserList($userId)
	{
		$this->walletDb->where('user', $userId);
		$this->walletDb->order_by('id', 'desc');
		$get = $this->walletDb->get($this->topUp_table);

		if ($get->num_rows() > 0) {

			//$this->data = $get->result_array();
			foreach ($get->result_array() as $key => $value) {
				$data[$key]['id'] = $value['id'];
				$data[$key]['transaction'] = $value['transaction'];
				$data[$key]['amount'] = $value['amount'];
				$data[$key]['status'] = $value['status'];

				if ($value['status'] == 3) {
					$data[$key]['status_name'] = "DONE";
				} else if ($value['status'] == 1) {
					$data[$key]['status_name'] = "PENDING";

				} else {
					$data[$key]['status_name'] = "CANCEL";
				}

				$data[$key]['created'] = $value['created'];
			}

			$this->data = $data; 
		}

		return $this->data;

	}

	public function getMaxUserWalletTrx($userId)
	{
		$walletValue = 0;
		$monthNow = date('m');
		$yearNow = date('Y');

		$walletId = "";
		$getWallet = $this->getUserBalance($userId);
		$walletId = $getWallet['id'];

		$this->walletDb->where('user',$userId);
		$this->walletDb->where('wallet',$walletId);
		$this->walletDb->where('year',$yearNow);
		$this->walletDb->where('month',$monthNow);
		$get = $this->walletDb->get('wallet_transaction_monthly');

		if ($get->num_rows() > 0) {

			$walletValue = $get->row_array()['amount'];
			
		}

		return $walletValue;
	}

	public function getNcashWalletUser($userId)
	{
		$nCash = 0;
		
		$this->walletDb->where('user',$userId);
		$get = $this->walletDb->get('wallet');

		if ($get->num_rows() > 0) {

			$nCash = $get->row_array()['ncash'];
			
		}

		return $nCash;
	}

	function updateLockingWallet($userId,$post)
	{
		$this->walletDb->where('user',$userId);
		$update = $this->walletDb->update('wallet',$post);

		return $update;

	}






}

/* End of file NusaUsersWalletModel.php */
/* Location: ./application/models/NusaUsersWalletModel.php */