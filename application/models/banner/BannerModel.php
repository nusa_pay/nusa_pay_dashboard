<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class BannerModel extends MY_Model
{
    public $table = 'banner';

    public function __construct(){
        parent::__construct('banner');
    }
}