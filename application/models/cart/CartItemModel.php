<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class CartItemModel extends MY_Model
{
    public $table = 'cart_item';

    public function __construct(){
        parent::__construct('cart');
    }

    public function productCountSuccess($product){
        $this->load->model('cart/CartModel', '_Cart');
        $table = $this->table;
        $cart  = $this->_Cart->table;

        $sql = <<<END
            SELECT COUNT(*) AS `total` FROM `$table` AS `ci`
                JOIN `$cart` AS `c` ON `c`.`id` = `ci`.`cart`
            WHERE `c`.`status` = 3
                AND `ci`.`product` = $product
END;

        $rows = $this->_db->query($sql);
        $rows = $this->_db->query($sql);
        if(!$rows->num_rows())
            return false;
        return (int)$rows->row()->total;
    }
}