<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class CartModel extends MY_Model
{
    public $table = 'cart';

    public function __construct(){
        parent::__construct('cart');
    }
}