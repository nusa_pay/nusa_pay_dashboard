<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class LogModel extends MY_Model
{
    public $table = 'kyc_log';
    public $view = "nusa_user_kyclog_view";
    public $return = false;

    public function __construct(){
        parent::__construct('kyc');
        $this->default_DB = $this->load->database("default",TRUE);
    }


    function create($data = null)
    {
    	$insert = $this->_db->insert($this->table, $data);

    	if ($insert) {
    		$this->return = true;
    	}

    	return $this->return;

    }

    function logList()
    {
    	$this->default_DB->select("*,CASE
			 
			WHEN rejection = 1 then 'REJECT' 
			WHEN ISNULL(rejection) then 'APPROVE'
			
			END  AS rejection_name");
    	$get = $this->default_DB->get($this->view);

    	return $get->result_array();
    }
}