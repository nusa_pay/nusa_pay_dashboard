<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class UserModel extends MY_Model
{
    public $table = 'tb_users';

    public function __construct(){
        parent::__construct('default');
    }
}