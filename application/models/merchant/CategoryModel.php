<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class CategoryModel extends MY_Model
{
    public $table = 'categories';

    public function __construct(){
        parent::__construct('nusamerchants');
    }
}