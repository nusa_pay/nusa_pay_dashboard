<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class MerchantModel extends MY_Model
{
    public $table = 'merchants';

    public function __construct(){
        parent::__construct('nusamerchants');
    }
}