<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class ProductModel extends MY_Model
{
    public $table = 'products';

    public function __construct(){
        parent::__construct('nusamerchants');

        $this->load->model('cart/CartModel', '_Cart');
    }

    public function countByStats($cond){
        $fields = [
            'product_name'      => '`p`.`name`',
            'product_price'     => '`p`.`price`',
            'merchant_name'     => '`m`.`name`',
            'merchant_industry' => '`mc`.`tag_name`',
            'cart_sold'         => '`cs`.`sold`'
        ];
        $flikes = [
            'product_name',
            'merchant_name',
            'merchant_industry'
        ];

        $cart_db = $this->_Cart->_db->database;

        $where = ['`p`.`deleted_at` IS NULL'];
        if($cond){
            foreach($cond as $fld => $val){
                if($val == 'All')
                    continue;
                if(!isset($fields[$fld]))
                    continue;
                $whr = $fields[$fld];
                if(in_array($fld, $flikes)){
                    $whr.= ' LIKE \'%' . $this->_db->escape_like_str($val) . '%\' ESCAPE \'!\'';
                }else{
                    $whr.= ' = ' . $this->_db->escape($val);
                }
                $where[] = $whr;
            }
        }
        $where = implode(' AND ', $where);

        $sql = <<<END
    SELECT COUNT(*) AS `total`
    FROM `products` AS `p` 
        JOIN `merchants` AS `m`
            ON `p`.`merchant_id` = `m`.`id`
        LEFT JOIN (
            SELECT `taggable_id`, `tag_name`
                FROM `tagging_tagged`
                WHERE `taggable_type` = 'App\\\\Entities\\\\Merchant'
                GROUP BY `taggable_id`
        ) AS `mc`
            ON `m`.`id` = `mc`.`taggable_id`
        LEFT JOIN (
            SELECT `product`, COUNT(`quantity`) AS `sold`
                FROM `$cart_db`.`cart_item` AS `ci`
                JOIN `$cart_db`.`cart` AS `c` ON `c`.`id` = `ci`.`cart`
                WHERE `c`.`status` = 3
                GROUP BY `ci`.`product`
        ) AS `cs`
            ON `cs`.`product` = `p`.`id`
    WHERE $where;
END;

        $rows = $this->_db->query($sql);
        if(!$rows->num_rows())
            return false;
        return (int)$rows->row()->total;
    }

    public function getByStats($cond, $rpp, $page, $sort){
        $fields = [
            'product_name'      => '`p`.`name`',
            'product_price'     => '`p`.`price`',
            'merchant_name'     => '`m`.`name`',
            'merchant_industry' => '`mc`.`tag_name`',
            'cart_sold'         => '`cs`.`sold`'
        ];
        $flikes = [
            'product_name',
            'merchant_name',
            'merchant_industry'
        ];

        $cart_db = $this->_Cart->_db->database;

        $sorts = [];

        if($sort){
            foreach($sort as $fld => $cnd){
                if(!isset($fields[$fld]))
                    continue;
                $sorts[] = $fields[$fld] . ' ' . ($cnd == 'asc' ? 'ASC' : 'DESC');
            }
        }

        if(!$sorts){
            $sorts = [
                '`p`.`name` ASC'
            ];
        }

        $sorts = implode(', ', $sorts);

        $where = ['`p`.`deleted_at` IS NULL'];
        if($cond){
            foreach($cond as $fld => $val){
                if($val == 'All')
                    continue;
                if(!isset($fields[$fld]))
                    continue;
                $whr = $fields[$fld];
                if(in_array($fld, $flikes)){
                    $whr.= ' LIKE \'%' . $this->_db->escape_like_str($val) . '%\' ESCAPE \'!\'';
                }else{
                    $whr.= ' = ' . $this->_db->escape($val);
                }
                $where[] = $whr;
            }
        }
        $where = implode(' AND ', $where);

        $page--;
        $offset = $page * $rpp;

        $sql = <<<END
    SELECT 
        `p`.`id` AS `Id`,
        `p`.`name` AS `Name`,
        `p`.`price` AS `Price`,
        `m`.`name` AS `Merchant`,
        `mc`.`tag_name` AS `Industry`,
        IF(cs.sold, cs.sold, 0) AS `Sold`
    FROM `products` AS `p` 
        JOIN `merchants` AS `m`
            ON `p`.`merchant_id` = `m`.`id`
        LEFT JOIN (
            SELECT `taggable_id`, `tag_name`
                FROM `tagging_tagged`
                WHERE `taggable_type` = 'App\\\\Entities\\\\Merchant'
                GROUP BY `taggable_id`
        ) AS `mc`
            ON `m`.`id` = `mc`.`taggable_id`
        LEFT JOIN (
            SELECT `product`, COUNT(`quantity`) AS `sold`
                FROM `$cart_db`.`cart_item` AS `ci`
                JOIN `$cart_db`.`cart` AS `c` ON `c`.`id` = `ci`.`cart`
                WHERE `c`.`status` = 3
                GROUP BY `ci`.`product`
        ) AS `cs`
            ON `cs`.`product` = `p`.`id`
    WHERE $where
    ORDER BY $sorts
    LIMIT $rpp OFFSET $offset;
END;

        $rows = $this->_db->query($sql);
        if(!$rows->num_rows())
            return false;
        return $rows->result();
    }
}