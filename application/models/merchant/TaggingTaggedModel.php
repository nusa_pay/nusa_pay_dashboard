<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class TaggingTaggedModel extends MY_Model
{
    public $table = 'tagging_tagged';

    public function __construct(){
        parent::__construct('nusamerchants');
    }

    public function getIndustries(){
        $this->_db->select('tag_name as name');
        $this->_db->where('taggable_type', 'App\Entities\Merchant');
        $this->_db->where('tag_slug !=', '');
        $this->_db->group_by('tag_name');
        $rows = $this->_db->get($this->table);
        if(!$rows->num_rows())
            return false;
        return $rows->result();
    }
}