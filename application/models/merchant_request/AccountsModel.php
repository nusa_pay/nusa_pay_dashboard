<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';


class AccountsModel extends MY_Model {

	public $table = 'accounts';

    public function __construct(){
        parent::__construct('nusamerchants_request');
    }


}

/* End of file AccountsModel.php */
/* Location: ./application/models/merchant_request/AccountsModel.php */