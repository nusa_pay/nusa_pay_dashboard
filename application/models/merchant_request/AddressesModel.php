<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';


class AddressesModel extends MY_Model {

	public $table = 'addresses';

    public function __construct(){
        parent::__construct('nusamerchants_request');
    }

}

/* End of file AddressesModel.php */
/* Location: ./application/models/merchant_request/AddressesModel.php */