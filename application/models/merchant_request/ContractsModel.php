<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';


class ContractsModel extends MY_Model {
	public $table = 'contracts';

    public function __construct(){
        parent::__construct('nusamerchants_request');
    }
	

}

/* End of file ContractsModel.php */
/* Location: ./application/models/merchant_request/ContractsModel.php */