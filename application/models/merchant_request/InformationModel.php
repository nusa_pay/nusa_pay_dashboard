<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';


class InformationModel extends MY_Model {

	public $table = 'information';

    public function __construct(){
        parent::__construct('nusamerchants_request');
    }

}

/* End of file InformationModel.php */
/* Location: ./application/models/merchant_request/InformationModel.php */