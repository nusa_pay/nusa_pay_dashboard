<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';


class OwnerModel extends MY_Model {
public $table = 'owners';

    public function __construct(){
        parent::__construct('nusamerchants_request');
    }
	

}

/* End of file OwnerModel.php */
/* Location: ./application/models/merchant_request/OwnerModel.php */