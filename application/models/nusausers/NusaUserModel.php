<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class NusaUserModel extends MY_Model
{
    public $table = 'users';

    public function __construct(){
        parent::__construct('nusausers');
    }
}