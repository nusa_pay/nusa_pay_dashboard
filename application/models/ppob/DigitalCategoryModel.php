<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class DigitalCategoryModel extends MY_Model
{
    public $table = 'digital_categories';

    public function __construct(){
        parent::__construct('ppob');
    }
}