<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class DigitalProductModel extends MY_Model
{
    public $table = 'digital_products';

    public function __construct(){
        parent::__construct('ppob');
    }

    public function countByStats($cond){
        $fields = [
            'product_code'      => '`p`.`code`',
            'product_name'      => '`p`.`name`',
            'product_category'  => '`p`.`category_id`'
        ];
        $flikes = [
            'product_code',
            'product_name'
        ];

        $where = [];
        if($cond){
            foreach($cond as $fld => $val){
                if($val == 'All')
                    continue;
                if(!isset($fields[$fld]))
                    continue;
                $whr = $fields[$fld];
                if(in_array($fld, $flikes)){
                    $whr.= ' LIKE \'%' . $this->_db->escape_like_str($val) . '%\' ESCAPE \'!\'';
                }else{
                    $whr.= ' = ' . $this->_db->escape($val);
                }
                $where[] = $whr;
            }
        }

        $sql = <<<END
    SELECT COUNT(*) AS `total`
    FROM `digital_products` AS `p` 
END;

        if($where)
            $sql.= ' WHERE ' . implode(' AND ', $where);

        $rows = $this->_db->query($sql);
        if(!$rows->num_rows())
            return false;
        return (int)$rows->row()->total;
    }

    public function getByStats($cond, $rpp, $page, $sort){
        $fields = [
            'product_admin_fee'     => '`p`.`admin_fee`',
            'product_code'          => '`p`.`code`',
            'product_name'          => '`p`.`name`',
            'product_price'         => '`p`.`price`',
            'product_category'      => '`p`.`category_id`',
            'transaction_sold'      => '`ts`.`sold`',
        ];
        $flikes = [
            'product_code',
            'product_name'
        ];

        $sorts = [];

        if($sort){
            foreach($sort as $fld => $cnd){
                if(!isset($fields[$fld]))
                    continue;
                $sorts[] = $fields[$fld] . ' ' . ($cnd == 'asc' ? 'ASC' : 'DESC');
            }
        }

        if(!$sorts){
            $sorts = [
                '`p`.`name` ASC'
            ];
        }

        $sorts = implode(', ', $sorts);

        $where = ['1 = 1'];
        if($cond){
            foreach($cond as $fld => $val){
                if($val == 'All')
                    continue;
                if(!isset($fields[$fld]))
                    continue;

                $whr = $fields[$fld];

                if($fld == 'product_admin_fee'){
                    if($val == 1){
                        $whr.= ' > 0';
                    }elseif($val == 2){
                        $whr.= ' = 0';
                    }
                }else{
                    if(in_array($fld, $flikes)){
                        $whr.= ' LIKE \'%' . $this->_db->escape_like_str($val) . '%\' ESCAPE \'!\'';
                    }else{
                        $whr.= ' = ' . $this->_db->escape($val);
                    }
                }

                $where[] = $whr;
            }
        }
        $where = implode(' AND ', $where);

        $page--;
        $offset = $page * $rpp;

        $sql = <<<END
    SELECT `p`.`id` AS `Id`,
        `p`.`code` AS `Code`,
        `p`.`name` AS `Name`,
        `p`.`price` AS `Price`,
        `p`.`category_id` AS `CId`,
        IF(`p`.`admin_fee` > 0, 1, 2) AS `Type`,
        IF(`pc`.`description` IS NOT NULL, CONCAT( `pc`.`name`, ' ( ', `pc`.`description`, ' ) '), `pc`.`name` ) AS `Category`,
        IF(`ts`.`sold` IS NULL, 0, `ts`.`sold`) AS `Sold`
    FROM `digital_products` AS `p`
        LEFT JOIN `digital_categories` AS `pc`
            ON `p`.`category_id` = `pc`.`id`
        LEFT JOIN (
            SELECT `code`, COUNT(*) AS `sold`
            FROM `digital_transactions`
            WHERE `status` = 'success'
            GROUP BY `code`
        ) AS `ts`
            ON `p`.`code` = `ts`.`code`
    WHERE $where
    ORDER BY $sorts
    LIMIT $rpp OFFSET $offset;
END;

        $rows = $this->_db->query($sql);
        if(!$rows->num_rows())
            return false;
        return $rows->result();
    }
}