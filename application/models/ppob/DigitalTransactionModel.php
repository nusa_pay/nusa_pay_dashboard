<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class DigitalTransactionModel extends MY_Model
{
    public $table = 'digital_transactions';

    public function __construct(){
        parent::__construct('ppob');
    }

    function getList()
    {

    	$this->_db->select("JSON_UNQUOTE(JSON_EXTRACT(a.product_snap, '$.name')) AS `product_name`");
    	$this->_db->select("b.name as users_name");
    	$this->_db->select("a.user_id,a.customer_id,a.order_id,a.uuid");
    	$this->_db->select("a.price,a.admin_fee,a.amount,a.discount_amount,a.voucher_amount,a.ncash,a.status,a.total,a.created_at");
    	$this->_db->from("digital_transactions a");
    	$this->_db->join('nusausers.users b', 'b.id=a.user_id', 'left');

    	$res = $this->_db->get()->result_array();
    	
    	return $res;
    }
}