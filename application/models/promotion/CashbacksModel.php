<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class CashbacksModel extends MY_Model
{
    public $table = 'cashbacks';

    public function __construct(){
        parent::__construct('promotion');
    }
}