<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class ProductFilterModel extends MY_Model
{
    public $table = 'product_filter';

    public function __construct(){
        parent::__construct('promotion');
    }
}