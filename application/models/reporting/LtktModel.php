<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LtktModel extends SB_Model {

	public $table = 'tb_users';
	public $view = 'nusa_user_topup_view';
	public $primaryKey = 'id';
	public $type = 24; //transaction topup agent

	public function __construct() {
		parent::__construct();
		
	}


	public function searchData($param)
	{
		$sql = "SELECT
						agent,
						`user`,
						users_name,
						SUM(amount) AS total_amount,
						SUM(fee) AS total_fee,
						SUM(total) AS grand_total

					FROM
						".$this->view."
					WHERE
						status_top_up = 3
					AND
					created BETWEEN '".$param['from']."' AND '".$param['to']."'
					AND type = ".$this->type."
					GROUP BY
						`user`;";

		$result = $this->db->query($sql);

		if ($result->num_rows() > 0) {
			$this->data = $result->result_array();
		}

		return $this->data;

	}

}

/* End of file LtktModel.php */
/* Location: ./application/models/reporting/LtktModel.php */