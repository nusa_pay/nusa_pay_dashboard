<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class TicketModel extends MY_Model
{
    public $table = 'ticket';

    public function __construct(){
        parent::__construct('default');
    }
}