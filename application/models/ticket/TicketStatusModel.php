<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/MY_Model.php';

class TicketStatusModel extends MY_Model
{
    public $table = 'ticket_status';

    public function __construct(){
        parent::__construct('default');
    }
}