<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/MY_Model.php';


class WalletLimitModel extends MY_Model {

	 public $table = 'wallet_limit';

    public function __construct(){
        parent::__construct('ewallet');
    }

}

/* End of file WalletLimitModel.php */
/* Location: ./application/models/wallet/WalletLimitModel.php */