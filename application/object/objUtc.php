<?php

if(!defined('BASEPATH'))
    die;

class objUtc implements JsonSerializable
{
    public $time;
    public $value;
    
    function __construct($date){
        $this->time = strtotime($date . ' UTC');
        $this->value = $date;
    }
    
    public function format($format){
        return date($format, $this->time);
        
    }
    
    function __toString(){
        return $this->value;
        
    }
    
    function jsonSerialize(){
        return $this->value;
    }
}