           
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 grid-margin grid-margin-md-0">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-flex align-items-center justify-content-start justify-content-sm-center flex-wrap">
                    <img class="img-md rounded" src="https://via.placeholder.com/100x100" alt="">
                    <div class="wrapper ml-4">
                      <p class="font-weight-medium">Tim Cook</p>
                      <p class="text-muted">timcook@gmail.com</p>
                      <p class="text-info mb-0 font-weight-medium">Designer</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 grid-margin grid-margin-md-0">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-flex align-items-center justify-content-start justify-content-sm-center flex-wrap">
                    <img class="img-md rounded" src="https://via.placeholder.com/100x100" alt="">
                    <div class="wrapper ml-4">
                      <p class="font-weight-medium">Sarah Graves</p>
                      <p class="text-muted">Sarah@gmail.com</p>
                      <p class="text-info mb-0 font-weight-medium">Developer</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-flex align-items-center justify-content-start justify-content-sm-center flex-wrap">
                    <img class="img-md rounded" src="https://via.placeholder.com/100x100" alt="">
                    <div class="wrapper ml-4">
                      <p class="font-weight-medium">David Grey</p>
                      <p class="text-muted">David@gmail.com</p>
                      <p class="text-info mb-0 font-weight-medium">Support Lead</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>