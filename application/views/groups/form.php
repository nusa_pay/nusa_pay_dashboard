
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  
                  <form class="cmxform" action="<?= base_url() ?>groups/save/<?= $id ?>" id="UsersFormAdd" method="post">
                  <div class="form-group" style="display: none;">
                  		<label for="">User Name</label>
                  		<input type="hidden" class="form-control" value="<?php echo $row['group_id'] ?>" name="group_id" placeholder="Input field">
                  	</div>
                  	<div class="form-group">
                  		<label for="">Group Name</label>
                  		<input type="text" class="form-control" value="<?php echo $row['name'] ?>" name="name" required id="name" placeholder="Input field">
                  	</div>
                  	<div class="form-group">
                  		<label for="">Description Name</label>
                  		<input type="text" class="form-control" value="<?= $row['description'] ?>" name="description" id="description" placeholder="Input field">
                  	</div>
                  	
                  
                  <input class="btn btn-primary" type="submit" value="Submit">
                  </form>
              </div>
            </div>
</div>
