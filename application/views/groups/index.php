<?php $this->load->view("devextreme");?>
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div class="d-flex table-responsive">
                  <a class="btn btn-sm btn-primary" href="<?=base_url() ?>groups/add">
                            <i class="mdi mdi-plus-circle-outline"></i> Add
                        </a>
                  </div>
                  <hr>
                  <div id="grid-users"></div>
                </div>
              </div>
            </div>
</div>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $listGroups; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-users").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "userLogin"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "Group Name",
                                    dataField: "name",
                                },
                                {
                                    caption: "Description",
                                    dataField: "description"
                                },
                                 {
                                    caption: "Is Active ?",
                                     alignment: "center",
                                    dataField: "users_active",
                                    cellTemplate: function (container, options) {
                                      var id = options.key.group_id;
                                      var active = options.key.active;
                                      var badge = "";
                                      var users_active = "";
                                      if(active == 1)
                                      {
                                      	users_active = "ACTIVE";
                                        badge = "<a href='<?php echo site_url() ?>groups/nonActive/" + id + "'><div class='badge badge-info badge-fw'>"+ users_active + "</div></a>";

                                      } else {
                                      	users_active = "NONACTIVE";
                                      	badge = "<a href='<?php echo site_url() ?>groups/nonActive/" + id + "'><div class='badge badge-danger badge-fw'>" +users_active+ "</div></a>";
                                     }
                                       $("<div>")
                                        .append($(badge))
                                        .appendTo(container);
                                    }
                                },
                                // {
                                //     caption: "DATE CREATED",
                                //     dataField: "created_at",
                                //     dataType: "date",
                                //      format: 'dd-MMMM-yyyy hh:mm:ss',
                                // },
                                //  {
                                //     caption: "Last Login",
                                //     dataField: "last_login",
                                //     dataType: "date",
                                //      format: 'dd-MMMM-yyyy hh:mm:ss',
                                // },
                                 {
                                    caption: "ACTION",
                                    dataField: "id",
                                     alignment: "center",
                                    cellTemplate: function (container, options) {
                                      var usersId = options.key.group_id;
                                      var btnEdit = "<a href='<?php echo site_url() ?>groups/add/" + usersId + "' class='btn btn-sm btn-primary'><i class='fa fa-pencil'></i></a>";
                                       $(btnEdit)
                                        //.append($(btnEdit))
                                        .appendTo(container);
                                    }
                                },

                                 
                            ],
                             
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 

  })(jQuery);
</script>