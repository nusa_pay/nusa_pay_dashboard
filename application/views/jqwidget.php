      <link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets/styles/jqx.base.css" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets/styles/jqx.light.css" type="text/css" />
      <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.js"></script>
     <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxdata.export.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.export.js"></script> 

    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.sort.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.pager.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.edit.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.columnsresize.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxcalendar.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxcombobox.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets/jqxdatetimeinput.js"></script>

 