<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("dashboard") ?>">
              <i class="mdi mdi-view-dashboard-outline menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("nusa/users/profilling") ?>">
              <i class="mdi mdi-verified menu-icon"></i>
              <span class="menu-title">Master Profiling</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-account menu-icon"></i>
              <span class="menu-title">Nusa Users</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users"> All Users </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/reporting"> Statistic & Reporting </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/kyc-request"> KYC Request </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/kyc-log"> KYC Logs </a></li>
              </ul>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#merchants" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-nature-people menu-icon"></i>
              <span class="menu-title">Merchants</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="merchants">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/merchantsRequest"> Request </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> List </a></li>

                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/reporting"> Statistic & Reporting </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/kyc-request"> KYC Request </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/kyc-log"> KYC Logs </a></li> -->
              </ul>
            </div>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("nusa/transaction") ?>" aria-expanded="false" aria-controls="transaction">
              <i class="mdi mdi-currency-usd menu-icon"></i>
              <span class="menu-title">Transaction</span>
               <!-- <i class="menu-arrow"></i> -->
            </a>
           <!--  <div class="collapse" id="transaction">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/merchantsRequest"> Top Up </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Bank Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Nusapay Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Merchant Purchases </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> ppob </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Transfer Alert </a></li>

              
              </ul>
            </div> -->
          </li>
          <li class="nav-item">
            <a class="nav-link"data-toggle="collapse" href="#reporting" aria-expanded="false" aria-controls="reporting"><?php //echo site_url("nusa/reporting") ?>
              <i class="mdi mdi-currency-usd menu-icon"></i>
              <span class="menu-title">Reporting</span>
               <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reporting">
              <ul class="nav flex-column sub-menu">
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/merchantsRequest"> LTKM </a></li> -->
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>reporting/ltkt"> LTKT </a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/merchantsRequest"> SIPESAT </a></li> -->
              <!--   <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Bank Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Nusapay Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Merchant Purchases </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> ppob </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Transfer Alert </a></li> -->

              
              </ul>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link"data-toggle="collapse" href="#wallet" aria-expanded="false" aria-controls="wallet"><?php //echo site_url("nusa/wallet") ?>
              <i class="mdi mdi-currency-usd menu-icon"></i>
              <span class="menu-title">Wallet</span>
               <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="wallet">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>wallet/voucher"> Voucher </a></li>
              <!--   <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Bank Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Nusapay Transfer </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Merchant Purchases </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> ppob </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() ?>nusa/users/merchants"> Transfer Alert </a></li> -->

              
              </ul>
            </div>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("ticket") ?>">
              <i class="mdi mdi-ticket-outline menu-icon"></i>
              <span class="menu-title">Ticket</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-cart-outline menu-icon"></i>
              <span class="menu-title">Products</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="products">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('/product/merchant') ?>">Merchants</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('/product/ppob') ?>">PPOB</a>
                </li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#promotion" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-sale menu-icon"></i>
              <span class="menu-title">Promotion</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="promotion">
              <ul class="nav flex-column sub-menu">
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="#">Promo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Report</a>
                </li>
                -->
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url() ?>promotion/banner">Banner</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url() ?>promotion/cashback">Cashback</a>
                </li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="false" aria-controls="auth">
              <i class="mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Settings</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="settings">
              <ul class="nav flex-column sub-menu">
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="#">Promo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Report</a>
                </li>
                -->
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url() ?>users">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url() ?>users">Groups</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url() ?>modules">Modules n Permission</a>
                </li>
              </ul>
            </div>
          </li>

           <!-- <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("dashboard") ?>">
              <i class="mdi mdi-view-dashboard-outline menu-icon"></i>
              <span class="menu-title">LIST CART</span>
            </a>
          </li>  
           <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url("dashboard") ?>">
              <i class="mdi mdi-view-dashboard-outline menu-icon"></i>
              <span class="menu-title">LIST WALLET</span>
            </a>
          </li>         -->
        </ul>
      </nav>