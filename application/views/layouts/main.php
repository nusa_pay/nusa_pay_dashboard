<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo  CNF_APPNAME ;

?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/serein/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/serein/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/serein/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/serein/vendors/iconfonts/font-awesome/css/font-awesome.min.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/serein/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/serein/js/toastr/toastr.css" type="text/css"  />
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico" />
  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>assets/serein/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>assets/serein/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/js/template.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/js/settings.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url() ?>assets/serein/js/dashboard.js"></script>
  <!-- <script src="<?php echo base_url() ?>assets/serein/js/js-grid.js"></script>
  <script src="<?php echo base_url() ?>assets/serein/js/db.js"></script> -->
  <script src="<?php echo base_url();?>assets/serein/js/toastr/toastr.js"></script>

  <script src="<?php echo base_url() ?>assets/serein/vendors/tinymce/tinymce.min.js"></script>
  <style type="text/css" media="screen">
  .options {
     padding: 20px;
     margin-top: 20px;
     background-color: rgba(191, 191, 191, 0.15);
  }

  .caption {
      font-size: 18px;
      font-weight: 500;
  }

  .option {
      margin-top: 10px;
  }  
  </style>
</head>
<body class="sidebar-fixed sidebar-dark">
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view("layouts/_navbar") ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
         <?php $this->load->view("layouts/_settings-panel") ?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
         <?php $this->load->view("layouts/_sidebar") ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php echo $content;?>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->load->view("layouts/_footer") ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <script type="text/javascript">
      
<?php 
if( $msg = $this->session->flashdata("message")){
?>  
  toastr["<?php echo $msg['type'] ?>"]("<?php echo $msg['caption'] ?>","<?php echo $msg['title'] ?>"); 
<?php 
}
?>

  </script>
</body>

</html>

