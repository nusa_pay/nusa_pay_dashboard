<?php $sidebar = SiteHelpers::menus('sidebar');?>
<?php foreach ($sidebar as $menu) : ?>
<li class="nav-item">
            <?php if(count($menu['childs']) > 0) :?>
             <a class="nav-link" data-toggle="collapse" href="#<?php echo $menu['module'] ?>" aria-expanded="false" aria-controls="<?php echo $menu['module'] ?>">
            <?php else:?>
            <a class="nav-link"
                <?php 
if ($menu['menu_type'] == 'external') {
	echo 'href="' . $menu['url'] . '"';
}else {
	echo 'href="' . site_url($menu['module']) . '"';	
} ?>>
            
 <?php endif;


?>
            
              <i class="<?php echo $menu['menu_icons'];


?> menu-icon"></i>
              <span class="menu-title"><?php echo $menu['menu_name'];


?></span>
              <?php if(count($menu['childs'] > 0)) : ?>
              <i class="menu-arrow"></i>
              <?php endif ?>
            </a>
             <?php if (count($menu['childs']) > 0) : ?>
             <div class="collapse" id="<?php echo $menu['module'] ?>">
              <ul class="nav flex-column sub-menu">
                  <?php foreach ($menu['childs'] as $menu2) : ?>
                  <li class="nav-item"> <a class="nav-link" 
                    <?php 
if ($menu2['menu_type'] == 'external') {
	
	
	
	echo 'href="' . $menu2['url'] . '"';
	
	
	
}


else {
	
	
	
	echo 'href="' . site_url($menu2['module']) . '"';
	
	
	
}



?>				
                  > <?php echo $menu2['menu_name'];


?> </a></li>
                  <?php endforeach ?>
              </ul>
            </div>
             <?php endif?>
          </li>
<?php endforeach ?>