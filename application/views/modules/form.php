
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  
                  <form class="cmxform" action="<?= base_url() ?>modules/save/<?= $id ?>" id="UsersFormAdd" method="post">
                  <div class="form-group" style="display: none;">
                  	
                  		<input type="hidden" class="form-control" value="<?php echo $row['module_id'] ?>" name="module_id" placeholder="Input field">
                  	</div>
                  	<div class="form-group">
                  		<label for="">Module Name</label>
                  		<input type="text" class="form-control" value="<?php echo $row['module_name'] ?>" name="module_name" required id="module_name" placeholder="" <?php if ($row['module_id'] != ""): ?>
                        readonly
                      <?php endif ?>>
                  	</div>


                    <div class="form-group">
                      <label for="">Module folder Name</label>
                      <input type="text" class="form-control" value="<?php echo $row['module_folder'] ?>" name="module_folder" required id="module_name" placeholder="" <?php if ($row['module_id'] != ""): ?>
                        readonly
                      <?php endif ?>>
                    </div>

              

                  	<div class="form-group">
                  		<label for="">Module title</label>
                  		<input type="text" required class="form-control" value="<?= $row['module_title'] ?>" name="module_title" id="module_title" placeholder="">
                  	</div>

                  	<div class="form-group">
                  		<label for="">Module Note</label>
                  		<input type="text" class="form-control" value="<?= $row['module_note'] ?>" name="module_note" id="module_note" placeholder="" required>
                  	</div>

                  	
                  
                  	
                  
                  <input class="btn btn-primary" type="submit" value="Submit">
                  </form>
              </div>
            </div>
</div>
