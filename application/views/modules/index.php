<?php $this->load->view("devextreme");?>
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div class="d-flex table-responsive">
                  <a class="btn btn-sm btn-primary" href="<?=base_url() ?>modules/add">
                            <i class="mdi mdi-plus-circle-outline"></i> Add
                        </a>
                  </div>
                  <hr>
                  <div id="grid-users"></div>
                </div>
              </div>
            </div>
</div>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $listModule; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-users").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "userLogin"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "Controller Name",
                                    dataField: "module_name",
                                },
                                {
                                    caption: "Module Title",
                                    dataField: "module_title"
                                },
                                {
                                    caption: "Module Note",
                                    dataField: "module_note"
                                },
                                
                                 {
                                    caption: "Is Active ?",
                                     alignment: "center",
                                    dataField: "active",
                                    cellTemplate: function (container, options) {
                                      var id = options.key.id;
                                      var active = options.key.active;
                                      var users_active = "";
                                      var is_core = options.key.module_type;
                                      var badge = "";

                                      if(is_core != "core")
                                      {
                                         if(active == 1)
                                        {
                                          users_active = "ACTIVE";
                                          badge = "<a href='<?php echo site_url() ?>modules/nonActive/" + id + "'><div class='badge badge-info badge-fw'>"+ users_active + "</div></a>";

                                        } else{
                                          users_active = "NONACTIVE";
                                           badge = "<a href='<?php echo site_url() ?>modules/nonActive/" + id + "'><div class='badge badge-danger badge-fw'>" +users_active+ "</div></a>";
                                        }
                                      } else {
                                         users_active = "ACTIVE";
                                         badge = "<div class='badge badge-info badge-fw'>"+ users_active + "</div>";

                                      }
                                     
                                      
                                      
                                       $("<div>")
                                        .append($(badge))
                                        .appendTo(container);
                                    }
                                },
                                {
                                    caption: "DATE CREATED",
                                    dataField: "module_created",
                                    dataType: "date",
                                     format: 'dd-MMMM-yyyy hh:mm:ss',
                                },
                                 {
                                    caption: "ACTION",
                                    dataField: "id",
                                     alignment: "center",
                                    cellTemplate: function (container, options) {
                                      var id = options.key.module_id;
                                      var btnEdit = "<a href='<?php echo site_url() ?>modules/add/" + id + "' class='btn btn-sm btn-primary'><i class='fa fa-pencil'></i></a>" ;
                                      var btnSetting = "<a href='<?php echo site_url() ?>modules/permission/" + id + "' class='btn btn-sm btn-primary'><i class='mdi mdi-settings'></i></a>";
                                       $(btnEdit)
                                        //.append($(btnEdit))
                                        .appendTo(container);

                                         $(btnSetting)
                                        //.append($(btnEdit))
                                        .appendTo(container);
                                    }
                                },

                                 
                            ],
                             
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 

  })(jQuery);
</script>