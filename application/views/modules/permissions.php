
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  
                  <form class="cmxform" action="<?= base_url() ?>modules/savePermissions/<?= $id ?>" id="UsersFormAdd" method="post">
                  
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Groups</th>
                          <?php foreach ($tasks as $item): ?>
                            <th><?= $item ?></th>
                          <?php endforeach ?>
                          <!-- <th>ADD</th>
                          <th>VIEW</th>
                          <th>EDIT</th>
                          <th>DETAIL</th>
                          <th>REMOVE</th>
                          <th>EXPORT</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($access as $key => $gp): ?>
                          <tr>
                          <td><?= $gp['group_name'];?><input type="hidden" name="group_id[]" value="<?= $gp['group_id'] ?>"></td>
                          <?php foreach ($tasks as $item=>$val): ?>
                             <td><input type="checkbox" <?php if($gp[$item] ==1) echo ' checked="checked" ';?> class="checkbox" name="<?php echo $item;?>[<?php echo $gp['group_id'];?>]" value="1"></td>
                          <?php endforeach ?>
                          </tr>
                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                  	
                  
                  	
                  
                  <input class="btn btn-primary" type="submit" value="Submit">
                  </form>
              </div>
            </div>
</div>
