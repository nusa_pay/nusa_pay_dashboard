<?php// print_r($row_information['request_type']); exit; ?>
<?php $this->load->view("devextreme"); ?>
<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">MERCHANTS <?= $row['store_name'] ?></h4>
      <form class="form-sample" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>nusa/merchantsRequest/save/<?=$row['id'] ?>" id="form-merchantsRequest">
        <p class="card-description">
          STATUS REQ : <?php if($row['status'] == '0') echo "Waiting" ?> <?php if($row['status'] == 1) echo "Approved" ?> <?php if($row['status'] == '2') echo "Decline" ?>
        </p>
        <p class="card-description">
          INFO
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Type Of Request</label>
              <div class="col-sm-9">
                <?php foreach ($type_of_request as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" required <?php if($row_information['request_type'] == $val->id) echo 'checked'; ?> class="form-check-input" name="type_of_request" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Company Resident</label>
              <div class="col-sm-9">
                <?php foreach ($company_residence as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['company_resident'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="company_residence" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Cooperation Purpose</label>
              <div class="col-sm-9">
                <?php foreach ($cooperation_purpose as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['cooperation_purpose'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="cooperation_purpose" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                
                <?php endforeach ?>
                
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Operating transaction per month</label>
              <div class="col-sm-9">
                
                <?php foreach ($operating_transaction_per_month as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['operating_transaction_per_month'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="operating_transaction_per_month" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>                  
              <?php endforeach ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Business Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="business_name" value="<?= $row['business_name'] ?>" required />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Store Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="store_name" value="<?= $row['store_name'] ?>" required />
              </div>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Email</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" name="email" value="<?= $row['email'] ?>" required />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tax Payer Number</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="taxpayer_number" value="<?= $row['taxpayer_number'] ?>" required />
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Phone</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="phone" value="<?= $row['phone'] ?>" required />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Facsimile</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="facsimile" value="<?= $row['facsimile'] ?>" />
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Image</label>
              <div class="col-sm-9">
                <!-- <input type="file" class="form-control" name="file_image" value="" /> -->
                <input type="hidden" class="form-control" name="file_image" value="<?= $row['image'] ?>" id="file_image" />
                <div id="file-uploader-images"></div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Logo</label>
              <div class="col-sm-9">
                <input type="hidden" class="form-control" name="file_logo" value="<?= $row['logo'] ?>" id="file_logo" />
                <div id="file-uploader-logo"></div>

              </div>
            </div>
          </div>
        </div>
        <hr>
        <p class="card-description">
          ADDRESS INFO
        </p>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Address</label>
              <div class="col-sm-9">
                <textarea name="address" class="form-control" rows="5" cols="62" ><?= $row_address['address'] ?></textarea>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Provinces</label>
              <div class="col-sm-9">
                <select name="provinces" id="provinces" class="form-control" required="required">
                  <option value=""></option>
                  <?php foreach ($provinces as $key => $val): ?>
                  <option <?php if($row_address['province'] == $val->id) echo 'selected' ?> value="<?= $val->id ?>"><?= $val->name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">ZIP CODE</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="zip_code" id="zip_code" value="<?= $row_address['zip_code'] ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <!--  <div class="form-group row">
              <label class="col-sm-3 col-form-label">Provinces</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="merchants_provinces" value="" />
              </div>
            </div> -->
          </div>
        </div>

        <?php $this->load->view('nusa/Merchants_Request/partial_director'); ?>
        <?php $this->load->view('nusa/Merchants_Request/partial_owner'); ?>
        <?php $this->load->view('nusa/Merchants_Request/partial_authorized_data'); ?>
        <?php $this->load->view('nusa/Merchants_Request/partial_tempat_usaha'); ?>
        <hr>
        <button type="submit" class="btn btn-primary">SAVE</button>&nbsp;
        <?php if ($row['id'] != ""): ?>
           <?php if($access['is_edit'] == 1): ?>
                <button type="button" class="btn btn-default" onClick="onApprove()">Approve</button>&nbsp;
                <button type="button" class="btn btn-danger" onClick="onReject()">Reject</button>&nbsp;
        <?php endif; ?>
        <?php endif ?>
       <a class="btn btn-warning" href="<?= base_url() ?>nusa/merchantsRequest">Cancel</a>&nbsp;
        
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
   $("#file-uploader-images").dxFileUploader({
        multiple: false,
        uploadMode: "useButtons",
        uploadUrl: "/",
        allowedFileExtensions: [".jpg", ".jpeg", ".gif", ".png"],
        onUploadStarted: function(e){
          var file = e.file;
          var formData = new FormData(),
            xhr      = new XMLHttpRequest();

            formData.append('file', file, file.name);
            // formData.append('disk', 'local');

            xhr.open('POST', '<?= $upload_url ?>', true);
            xhr.setRequestHeader('Authorization', '<?= $atoken ?>');

             xhr.onreadystatechange = function(){
            if(xhr.readyState != 4)
                return;

            if(xhr.status != 200)
                return DevExpress.ui.notify("Upload Failed", 'error', 600);

            var res;
            try{
                res = JSON.parse(xhr.responseText);
            }catch(e){
            }

            if(!res)
                return DevExpress.ui.notify("invalid json", 'error', 600);

            if(!res.success)
                return  DevExpress.ui.notify(res.message, 'error', 600);

            $("#file_image").val(res.data.url);

            //$inp.val(res.data.url);
        }

        xhr.send(formData);

        }
    });

   $("#file-uploader-logo").dxFileUploader({
        multiple: false,
        uploadMode: "useButtons",
        uploadUrl: "/",
        allowedFileExtensions: [".jpg", ".jpeg", ".gif", ".png"],
        onUploadStarted: function(e){
          var file = e.file;
          var formData = new FormData(),
            xhr      = new XMLHttpRequest();

            formData.append('file', file, file.name);
            // formData.append('disk', 'local');

            xhr.open('POST', '<?= $upload_url ?>', true);
            xhr.setRequestHeader('Authorization', '<?= $atoken ?>');

             xhr.onreadystatechange = function(){
            if(xhr.readyState != 4)
                return;

            if(xhr.status != 200)
                return DevExpress.ui.notify("Upload Failed", 'error', 600);

            var res;
            try{
                res = JSON.parse(xhr.responseText);
            }catch(e){
            }

            if(!res)
                return DevExpress.ui.notify("invalid json", 'error', 600);

            if(!res.success)
                return  DevExpress.ui.notify(res.message, 'error', 600);

            $("#file_logo").val(res.data.url);

            //$inp.val(res.data.url);
        }

        xhr.send(formData);

        }
    });

  function onApprove()
  {
    $.ajax({
      url: '<?= base_url() ?>nusa/merchantsRequest/approve/<?= $row['id'] ?>',
      type: 'post',
      dataType: 'json',
      data: {status: 1},
    })
    .done(function(result) {
     DevExpress.ui.notify("Successfull Approve", 'success', 800);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

   function onReject()
  {
   $.ajax({
      url: '<?= base_url() ?>nusa/merchantsRequest/rejectApprove/<?= $row['id'] ?>',
      type: 'post',
      dataType: 'json',
      data: {status: 2},
    })
    .done(function(result) {
     DevExpress.ui.notify("Successfull Decline", 'error', 800);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }
</script>