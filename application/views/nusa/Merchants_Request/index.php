<div class="col-12 grid-margin">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title"><?= "Merchants Request" ?></h4>

        <p class="card-description"><?= "" ?></p>

        <p><button type="button" onclick="window.location.href='<?=base_url() ?>nusa/merchantsRequest/add'" class="btn btn-primary">add</button></p>
        <div id="grid"></div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('devextreme'); ?>
<script>
  (function($) {
    var no_avatar = "<?php echo base_url() ?>/assets/images/no_avatar.jpg"

    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $request_list; ?>;
        items.resolve(data);
        return items.promise();
    }
  };


$("#grid").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportTopUp"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },
                            columns: [

                                {
                                    caption: "Merchants Req Status",
                                    dataField: "status_view"
                                },
                                
                                {
                                    caption: "Business Name",
                                    dataField: "business_name"
                                },
                                {
                                    caption: "Store Name",
                                    dataField: "store_name"
                                },
                                {
                                    caption: "Email",
                                    dataField: "email"
                                },
                                {
                                    caption: "Tax Number",
                                    dataField: "taxpayer_number"
                                },
                                {
                                    caption: "Phone",
                                    dataField: "phone"
                                },
                          //       {
                          //           caption: "Image",
                          //           dataField: "image",
                          //           cellTemplate: function (container, options) {
                          //             var src = options.value;
                          //             if (src == null) {
                          //               src = no_avatar;
                          //             }
                                      
                          //     $("<div>")
                          //       .append($("<img>", { "src": src, "width": "120", "height": "120" }))
                          //         .appendTo(container);
                          // }
                          //       },
                          //       {
                          //           caption: "Logo",
                          //           dataField: "logo",
                          //           cellTemplate: function (container, options) {
                          //             var src = options.value;
                          //             if (src == null) {
                          //               src = no_avatar;
                          //             }
                                      
                          //               $("<div>")
                          //                 .append($("<img>", { "src": src, "width": "120", "height": "120" }))
                          //                   .appendTo(container);
                          //           }
                          //       },
                              
                                {
                                    caption: "ACTION",
                                    alignment: "center",
                                    dataField: "id",
                                    cellTemplate: function (container, options) {
                                      console.log(container);
                                      console.log(options);
                                      var id = options.key.id;
                                      var status = options.key.status;
                                      var disabled = false;
                                      if (status == 1) disabled = true;
                                     

                                    <?php if($access['is_edit'] == 1) : ?>
                                    $("<div id='btnApprove' />").dxButton({
                                              //icon: 'f',
                                              id:"btnApprove",
                                              text:"Detail / Edit",
                                              //disabled: disabled,
                                              onClick: function (e) {
                                                    window.location.href =  "<?= base_url() ?>nusa/merchantsRequest/add/" + id;
                                              }
                                          }).appendTo(container);

                                     <?php endif; ?>
                                    }
                                }
                             
                            ]
                             
                        });


  })(jQuery);
</script>