<hr>
<p class="card-description">
Data yang diberi Kuasa / Authorized data
</p>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">FULL NAME</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="authorized_name" value="<?= $row_account['name'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">ID NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="authorized_ktp" value="<?= $row_account['identity_number'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">MOBILE NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="authorized_mobile_number" value="<?= $row_account['phone'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">JOB POSITION</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="authorized_position" value="<?= $row_account['title'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>

<!-- address -->

<script>

</script>