<hr>
<p class="card-description">
	DIREKSI DAN KOMISARIS / DIRECTORS AND COMMISSIONERS
</p>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">DIRECTORS NAME</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="directors_name" value="<?= $row_director['name'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">GENDER</label>
			<div class="col-sm-9">
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" <?php if($row_director['gender'] == 0) echo "checked"; ?> required class="form-check-input" name="directors_gender" id="directors_gender" value="0">
						MALE
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" <?php if($row_director['gender'] == 1) echo "checked"; ?> required class="form-check-input" name="directors_gender" id="directors_gender" value="1">
						FEMALE
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">DIRECTORS DATE OF BIRTH</label>
			<div class="col-sm-9">
				<div id="directors_date_of_birth"></div>
				<input type="hidden" name="directors_date_of_birth" required id="directors_date_of_birth_val" value="<?= $row_director['date_of_birth'] ?>">
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">IDENTITY NUMBER</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="directors_identity" value="<?= $row_director['identity_number'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">TAX NUMBER</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="directors_tax_number" value="<?= $row_director['identity_number'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
	$("#directors_date_of_birth").dxDateBox({
	displayFormat: "yyyy-MM-dd",
	type:"date",
	value: "<?= $row_director['date_of_birth'] ?>",
	pickerType: "rollers",
        onValueChanged: function (e) {
            var previousValue = e.previousValue;
            var newValue = new Date(e.value);
            var date = newValue.getFullYear() + '-' + (newValue.getMonth() + 1) + '-' + newValue.getDate();
            // Event handling commands go here
            $('#directors_date_of_birth_val').val(date);
        }
	});
	});
	
</script>