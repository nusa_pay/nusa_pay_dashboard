<hr>
<p class="card-description">
	Pengendali Akhir / Individual Ultimate Beneficial Owner
</p>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">FULL NAME</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="owner_name" value="<?= $row_owner['name'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">ID NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_ktp" value="<?= $row_owner['identity_number'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">PHONE NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_phone_number" required id="owner_date_of_birth_val" value="<?= $row_owner['phone'] ?>">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">MOBILE NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_mobile_number" value="<?= $row_owner['mobile_phone'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">TAX NUMBER</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_tax_number" value="<?= $row_owner['taxpayer_number'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">EMAIL</label>
			<div class="col-sm-9">
				<input type="email" class="form-control" required name="owner_email" value="<?= $row_owner['email'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">SIBLINGS CONTACT NAME</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_sibling_contact" value="<?= $row_owner['siblings_contact'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">SIBLING PHONE</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="owner_sibling_phone" value="<?= $row_owner['siblings_phone'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">JOB POSITION</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="owner_position" value="<?= $row_owner['job_position'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<!-- address -->
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">CITY</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_city" value="<?= $row_owner['city'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">VILLAGES</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="owner_village" value="<?= $row_owner['village'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">SUBDISTRICT</label>
			<div class="col-sm-9">
				<input type="text" required class="form-control" name="owner_subdistrict" value="<?= $row_owner['subdistrict'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">ZIP CODE</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="owner_zip_code" value="<?= $row_owner['zip_code'] ?>" placeholder="">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">PROVINCE</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" required name="owner_province" value="<?= $row_owner['province'] ?>" placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">FULL ADDRESS</label>
			<div class="col-sm-9">
				<textarea name="owner_address" class="form-control"><?= $row_owner['address'] ?></textarea>
			</div>
		</div>
	</div>
	
</div>
<script>

</script>