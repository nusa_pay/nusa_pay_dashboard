<hr>
<p class="card-description">
Data Tempat Usaha
</p>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">ESTABLISHED</label>
			<div class="col-sm-9">
				<div id="tempat_usaha_established"></div>
				<input type="hidden" name="tempat_usaha_established" required id="tempat_usaha_established_val" value="<?= $row_information['established'] ?>">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">STATUS KEPIMILIKAN USAHA</label>
			<div class="col-sm-9">
				<!-- <input type="text" required class="form-control" name="tempat_usaha_status" value="" placeholder=""> -->
				<?php foreach ($bussiness_status as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['store_ownership_status'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="tempat_usaha_status" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">BENTUK BADAN USAHA</label>
			<div class="col-sm-9">
				<?php foreach ($bussiness_type as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['business_entity'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="bentuk_badan_usaha" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">LOKASI USAHA</label>
			<div class="col-sm-9">
				<?php foreach ($bussiness_location as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['store_location'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="tempat_usaha_lokasi" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">OMZET PER BULAN</label>
			<div class="col-sm-9">
				<?php foreach ($bussiness_omzet as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['omzet'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="tempat_usaha_omzet" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">RATA RATA PER TRANSAKSI</label>
			<div class="col-sm-9">
				<?php foreach ($avg_trx as $key => $val): ?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" <?php if($row_information['avg_transaction'] == $val->id) echo 'checked'; ?> required class="form-check-input" name="tempat_usaha_avgTrx" id="cp_<?= $val->id ?>" value="<?= $val->id ?>">
                    <?= $val->desc_name ?>
                  </label>
                </div>
                <?php endforeach ?>
			</div>
		</div>
	</div>
</div>

<!-- address -->

<script>
	$(function(){
	$("#tempat_usaha_established").dxDateBox({
	displayFormat: "yyyy-MM-dd",
	type:"date",
	value: "<?= $row_information['established'] ?>",
	pickerType: "rollers",
        onValueChanged: function (e) {
            var previousValue = e.previousValue;
            var newValue = new Date(e.value);
            var date = newValue.getFullYear() + '-' + (newValue.getMonth() + 1) + '-' + newValue.getDate();
            // Event handling commands go here
            $('#tempat_usaha_established_val').val(date);
        }
	});
	});
</script>