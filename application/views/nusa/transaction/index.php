<div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Reporting</h4>
                  <p class="card-description"><b></b></p>
                 
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Top-up</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="withdraw-tab" data-toggle="tab" href="#withdraw" role="tab" aria-controls="withdraw-1" aria-selected="true">Bank Transfer</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="transaction-tab" data-toggle="tab" href="#detail_trx" role="tab" aria-controls="detail_trx" aria-selected="false">Nusapay Transfer</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="merchant-tab" data-toggle="tab" href="#merchant" role="tab" aria-controls="merchant" aria-selected="false">Merchant Purchases</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" id="ppob-tab" data-toggle="tab" href="#ppob" role="tab" aria-controls="ppob" aria-selected="false">PPOB</a>
                    </li>
                 <!--     <li class="nav-item">
                      <a class="nav-link" id="bonus-tab" data-toggle="tab" href="#bonus" role="tab" aria-controls="contact-1" aria-selected="false">Filter Transaction By Loc </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="referral-tab" data-toggle="tab" href="#referral" role="tab" aria-controls="contact-1" aria-selected="false">Most Selling Items </a>
                    </li> -->
                    <li class="nav-item">
                      <a class="nav-link" id="transferAlert-tab" data-toggle="tab" href="#transferAlert" role="tab" aria-controls="contact-1" aria-selected="false">Transfer Alert </a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                      <?php $this->load->view('nusa/transaction/top_withdraw'); ?>
                    </div>
                    <div class="tab-pane fade" id="withdraw" role="tabpanel" aria-labelledby="withdraw-tab">
                      <?php $this->load->view('nusa/transaction/withdraw'); ?>
                    </div>
                    <div class="tab-pane fade" id="detail_trx" role="tabpanel" aria-labelledby="transaction-tab">
                     <?php $this->load->view('nusa/transaction/transfer'); ?>
                    </div>
                    <div class="tab-pane fade" id="merchant" role="tabpanel" aria-labelledby="contact-tab">
                    <?php $this->load->view('nusa/transaction/merchant'); ?>
                    </div>
                    <div class="tab-pane fade" id="ppob" role="tabpanel" aria-labelledby="ppob-tab">
                       <?php $this->load->view('nusa/transaction/ppob'); ?>
                    </div>
                    <!-- <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="contact-tab">
                      
                    </div>
                    <div class="tab-pane fade" id="referral" role="tabpanel" aria-labelledby="contact-tab">
                      
                    </div> -->
                    <div class="tab-pane fade" id="transferAlert" role="tabpanel" aria-labelledby="contact-tab">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

  