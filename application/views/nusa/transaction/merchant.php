
<!-- withdraw -->
    <!--       <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total WITHDRAW Deleted</h6>
                  <h2 class="mb-0"><?= $countTransfer['deleted'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total WITHDRAW Cancel</h6>
                   <h2 class="mb-0"><?= $countTransfer['cancel']?></h2>

                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total TRANSFER Waiting</h6>
                  <h2 class="mb-0"><?= $countTransfer['waiting'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total TRANSFER Success</h6>
                   <h2 class="mb-0"><?= $countTransfer['success']?></h2>

                </div>
              </div>
            </div>

            
          </div>
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Deleted</h6>
                  <h2 class="mb-0"><?= $totalTransfer['deleted'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Cancel</h6>
                   <h2 class="mb-0"><?= $totalTransfer['cancel']?></h2>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Waiting</h6>
                  <h2 class="mb-0"><?= $totalTransfer['waiting'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Success</h6>
                   <h2 class="mb-0"><?= $totalTransfer['success']?></h2>
                </div>
              </div>
            </div>

            
          </div> -->


<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">MERCHANT Purchases Report Table</h4>
                  <div class="google-chart-container">
                     <div id="grid-merchant"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Most Selling Item Table</h4>
                  <div class="google-chart-container">
                     <div id="grid-most-selling-item"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $merchantList; ?>;
        items.resolve(data);
        return items.promise();
    }
};

 var dataSourceSellingItem = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $mostSellingItem; ?>;
        items.resolve(data);
        return items.promise();
    }
};

 $("#grid-most-selling-item").dxDataGrid({
                    dataSource: dataSourceSellingItem,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportMercantSale"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "Product",
                                    dataField: "product_snap_name"
                                },
                                {
                                    caption: "Total Quantity",
                                    dataField: "TotalQuantity"
                                },
                                {
                                    caption: "TOTAL IDR",
                                    dataField: "TotalIdr",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             summary: {
                                  totalItems: [{
                                      column: "Product",
                                      summaryType: "count"
                                  },
                                  {
                                      column: "TotalIdr",
                                      summaryType: "sum",
                                      valueFormat: "currency",
                                       displayFormat: "Total Rp {0}"

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

   $("#grid-merchant").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportMerchant"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                {
                                    caption: "User Name",
                                    dataField: "users_name"
                                },
                                {
                                    caption: "Status Kyc User",
                                    dataField: "status_kyc_name"
                                },
                                {
                                    caption: "Description",
                                    dataField: "description"
                                },
                                {
                                    caption: "STATUS",
                                    dataField: "status_name"
                                },
                                {
                                    caption: "DATE",
                                    dataField: "created",
                                    dataType: "date"
                                },
                                {
                                    caption: "TOTAL",
                                    dataField: "total",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    var masterData = options.data;
                                    $("<div>")
                                        .addClass("show-detail")
                                        .text("Loading....")
                                        .appendTo(container);
                                    var url_cart_detail = "<?php echo base_url() ?>nusa/transaction/json_getCartDetail"
                                    $.ajax({
                                      url: url_cart_detail,
                                      type: 'post',
                                      dataType: 'json',
                                      data: {cart_id: masterData.id },
                                    })
                                    .done(function(result) {
                                      data_detail = result;
                                      $("<div>")
                                        .addClass("master-detail-caption")
                                        .text(masterData.users_name + " Purchases Detail")
                                        .appendTo(container);

                                    $("<div>")
                                        .dxDataGrid({
                                            columnAutoWidth: true,
                                            showBorders: true,
                                            columns: [
                                            {
                                                caption: "Merchant Name",
                                                dataField: "merchant_name",
                                                dataType: "string"
                                            },{
                                                caption: "Product Name",
                                                dataField: "product_snap_name",
                                                dataType: "string"
                                            },
                                            {
                                                caption: "Created",
                                                dataField: "created",
                                                dataType: "date"
                                            },{
                                                caption: "Price",
                                                dataField: "price",
                                                dataType: "number",
                                                 format: "Rp #,##0.##"
                                            },{
                                                caption: "Quantity",
                                                dataField: "quantity",
                                                dataType: "number"
                                            },{
                                                caption: "Sub total",
                                                dataField: "subtotal",
                                                dataType: "number",
                                                 format: "Rp #,##0.##"
                                            },{
                                                caption: "Fee",
                                                dataField: "fee",
                                                dataType: "number",
                                                 format: "Rp #,##0.##"
                                            },{
                                                caption: "Tax",
                                                dataField: "tax",
                                                dataType: "number",
                                                 format: "Rp #,##0.##"
                                            },{
                                                caption: "Total",
                                                dataField: "total",
                                                dataType: "number",
                                                format: "Rp #,##0.##"
                                            }],
                                            dataSource: result
                                        }).appendTo(container);

                                        $('.show-detail').css("display","none");
                                    })
                                    .fail(function() {
                                      console.log("error");
                                    })
                                    .always(function() {
                                      
                                    });
                                }
                            },
                             summary: {
                                  totalItems: [{
                                      column: "user_name",
                                      summaryType: "count"
                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      format: {
                                          type: "fixedPoint",
                                          precision: 0
                                      },
                                      displayFormat: "Total Rp {0}"
                                      // valueFormat: "currency",
                                      // editorOptions: {
                                      //   format: "Rp #,##0.##"
                                      // }

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });
  })(jQuery);
</script>