


<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">PPOB Summary</h4>
                  <div class="google-chart-container">
                    <div class="options">
                        <div class="caption">Options</div>
                        <div class="option">            
                            <div id="autoExpandP"></div>
                        </div>    
                    </div>
                    <hr>
                     <div id="ppob"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<script>
  (function($) {
   
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $ppobList; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#ppob").dxDataGrid({
                    dataSource: dataSource,
                   showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "PPOBTRX"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "USERS NAME",
                                    dataField: "users_name",
                                      groupIndex: 0
                                },
                              
                                {
                                    caption: "ORDER ID",
                                    dataField: "order_id",
                                },
                                
                                {
                                    caption: "DATE CREATED",
                                    dataField: "created_at",
                                    dataType: "date",
                                      format: 'dd-MMMM-yyyy hh:mm:ss',
                                    width:200
                                },
                                 {
                                    caption: "PRODUCT NAME",
                                    dataField: "product_name",
                                    dataType:"string"
                                },
                                {
                                    caption: "STATUS",
                                    dataField: "status",
                                    dataType:"string"
                                },

                                {
                                    caption: "PRICE",
                                    dataField: "price",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "FEE",
                                    dataField: "admin_fee",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }, 
                                {
                                    caption: "AMOUNT",
                                    dataField: "amount",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "DSC%",
                                    dataField: "discount_amount",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "VOUCHER AMOUNT",
                                    dataField: "voucher_amount",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                 {
                                    caption: "NCASH",
                                    dataField: "ncash",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "TOTAL",
                                    dataField: "total",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             summary: {
                                  totalItems: [{
                                      column: "users_name",
                                      summaryType: "count"
                                  },
                                  {
                                      column: "price",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                  {
                                      column: "admin_fee",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                   {
                                      column: "amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                   {
                                      column: "discount_amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                   {
                                      column: "voucher_amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                   {
                                      column: "ncash",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  }],
                                   groupItems: [{
                                      column: "users_name",
                                      summaryType: "count",
                                      displayFormat: "{0} count"
                                  },
                                  {
                                      column: "price",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "admin_fee",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                   {
                                      column: "amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "discount_amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                   {
                                      column: "voucher_amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "ncash",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },

                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

  $("#autoExpandP").dxCheckBox({
        value: false,
        text: "Expand All Groups",
        onValueChanged: function(data) {
           var dataGrid = $("#ppob").dxDataGrid("instance");
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
  });

  })(jQuery);
</script>