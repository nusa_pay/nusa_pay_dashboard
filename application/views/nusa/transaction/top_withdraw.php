<?php $this->load->view("devextreme");?>
<div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Top Up Deleted</h6>
                  <h2 class="mb-0"><?= $countTopUp['deleted'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Top Up Cancel</h6>
                   <h2 class="mb-0"><?= $countTopUp['cancel']?></h2>

                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Top Up Waiting</h6>
                  <h2 class="mb-0"><?= $countTopUp['waiting'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Top Up Success</h6>
                   <h2 class="mb-0"><?= $countTopUp['success']?></h2>

                </div>
              </div>
            </div>

            
          </div>
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR Top Up Deleted</h6>
                  <h2 class="mb-0"><?= $totalTopUp['deleted'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR Top Up Cancel</h6>
                   <h2 class="mb-0"><?= $totalTopUp['cancel']?></h2>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR Top Up Waiting</h6>
                  <h2 class="mb-0"><?= $totalTopUp['waiting'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR Top Up Success</h6>
                   <h2 class="mb-0"><?= $totalTopUp['success']?></h2>
                </div>
              </div>
            </div>

            
          </div>


<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Top Up Summary</h4>
                  <div class="google-chart-container">
                    <div class="options">
                        <div class="caption">Options</div>
                        <div class="option">            
                            <div id="autoExpand"></div>
                        </div>    
                    </div>
                    <hr>
                     <div id="grid-topup"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $topUpList; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-topup").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportTopUp"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "",
                                    dataField: "users_name",
                                     groupIndex: 0
                                },
                                {
                                    caption: "Description",
                                    dataField: "description"
                                },
                                {
                                    caption: "STATUS TRX",
                                    dataField: "status_name"
                                },
                                {
                                    caption: "KYC STATUS",
                                    dataField: "status_kyc_name"
                                },
                                {
                                    caption: "BANK",
                                    dataField: "bank"
                                },
                                {
                                    caption: "DATE CREATED",
                                    dataField: "created",
                                    dataType: "date",
                                },
                                 {
                                    caption: "AMOUNT TOP UP",
                                    dataField: "amount_top_up",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                 {
                                    caption: "UNIQUE CODE",
                                    dataField: "unique_code",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "ADMIN FEE",
                                    dataField: "admin_fee",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "TOTAL",
                                    dataField: "total",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             summary: {
                               groupItems: [{
                                      column: "users_name",
                                      summaryType: "count",
                                        displayFormat: "{0} Top Up",
                                  },
                                   {
                                      column: "amount_top_up",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                   {
                                      column: "unique_code",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  }],

                                  totalItems: [{
                                      column: "users_name",
                                      summaryType: "count",
                                        displayFormat: "{0} Top Up",
                                  },
                                   {
                                      column: "amount_top_up",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      

                                  },
                                   {
                                      column: "unique_code",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      

                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 $("#autoExpand").dxCheckBox({
        value: false,
        text: "Expand All Groups",
        onValueChanged: function(data) {
           var dataGrid = $("#grid-withdraw").dxDataGrid("instance");
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
  });

  })(jQuery);
</script>