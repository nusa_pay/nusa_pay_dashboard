
<!-- withdraw -->
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total WITHDRAW Deleted</h6>
                  <h2 class="mb-0"><?= $countTransfer['deleted'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total WITHDRAW Cancel</h6>
                   <h2 class="mb-0"><?= $countTransfer['cancel']?></h2>

                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total TRANSFER Waiting</h6>
                  <h2 class="mb-0"><?= $countTransfer['waiting'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total TRANSFER Success</h6>
                   <h2 class="mb-0"><?= $countTransfer['success']?></h2>

                </div>
              </div>
            </div>

            
          </div>
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Deleted</h6>
                  <h2 class="mb-0"><?= $totalTransfer['deleted'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Cancel</h6>
                   <h2 class="mb-0"><?= $totalTransfer['cancel']?></h2>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Waiting</h6>
                  <h2 class="mb-0"><?= $totalTransfer['waiting'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR TRANSFER Success</h6>
                   <h2 class="mb-0"><?= $totalTransfer['success']?></h2>
                </div>
              </div>
            </div>

            
          </div>


<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">TRANSFER Report Table</h4>
                  <div class="google-chart-container">
                     <div id="grid-transfer"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $transferList; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-transfer").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "Reportwithdraw"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "FROM Name",
                                    dataField: "from_name"
                                },
                                {
                                    caption: "TO Name",
                                    dataField: "to_name"
                                },
                                {
                                    caption: "TRANSFER TYPE",
                                    dataField: "type_name"
                                },
                                {
                                    caption: "Description",
                                    dataField: "description"
                                },
                                {
                                    caption: "STATUS TRX",
                                    dataField: "status_name"
                                },
                                {
                                    caption: "DATE CREATED",
                                    dataField: "created",
                                    dataType: "date",
                                },
                                 {
                                    caption: "AMOUNT TRANSFER",
                                    dataField: "amount",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "TOTAL",
                                    dataField: "total",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             summary: {
                                  totalItems: [{
                                      column: "from_name",
                                      summaryType: "count"
                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "currency",
                                      editorOptions: {
                                        format: "Rp #,##0.##"
                                      }

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });
  })(jQuery);
</script>