
<!-- withdraw -->
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Bank Transfers Deleted</h6>
                  <h2 class="mb-0"><?= $countWithdraw['deleted'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Bank Transfers Cancel</h6>
                   <h2 class="mb-0"><?= $countWithdraw['cancel']?></h2>

                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Bank Transfers Waiting</h6>
                  <h2 class="mb-0"><?= $countWithdraw['waiting'] ?></h2>

                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total Bank Transfers Success</h6>
                   <h2 class="mb-0"><?= $countWithdraw['success']?></h2>

                </div>
              </div>
            </div>

            
          </div>
          <div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR WITHDRAW Deleted</h6>
                  <h2 class="mb-0"><?= $totalWithdraw['deleted'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR WITHDRAW Cancel</h6>
                   <h2 class="mb-0"><?= $totalWithdraw['cancel']?></h2>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR WITHDRAW Waiting</h6>
                  <h2 class="mb-0"><?= $totalWithdraw['waiting'] ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">Total IDR WITHDRAW Success</h6>
                   <h2 class="mb-0"><?= $totalWithdraw['success']?></h2>
                </div>
              </div>
            </div>

            
          </div>


<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Bank Transfer Summary</h4>
                  <div class="google-chart-container">
                    <div class="options">
                        <div class="caption">Options</div>
                        <div class="option">            
                            <div id="autoExpandW"></div>
                        </div>    
                    </div>
                    <hr>
                     <div id="grid-withdraw"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
<script>
  (function($) {
   
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $withdrawList; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-withdraw").dxDataGrid({
                    dataSource: dataSource,
                   showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "Reportwithdraw"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "Users Name",
                                    dataField: "users_name",
                                      groupIndex: 0
                                },
                                // {
                                //     caption: "Description",
                                //     dataField: "description"
                                // },
                                {
                                    caption: "STATUS TRX",
                                    dataField: "status_name",
                                    width:90
                                },
                                // {
                                //     caption: "KYC STATUS",
                                //     dataField: "status_kyc_name"
                                // },
                                {
                                    caption: "DATE CREATED",
                                    dataField: "created",
                                    dataType: "date",
                                      format: 'dd-MMMM-yyyy hh:mm:ss',
                                    width:200
                                },
                                 {
                                    caption: "BANK NAME",
                                    dataField: "bank_name",
                                    dataType:"string"
                                },
                                {
                                    caption: "BANK ACCOUNT NAME",
                                    dataField: "bank_account_name",
                                    dataType:"string"
                                },
                                {
                                    caption: "BANK ACCOUNT NUMBER",
                                    dataField: "bank_account_number",
                                    dataType:"string"
                                },
                                {
                                    caption: "AMOUNT TRANSFER",
                                    dataField: "amount",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "FEE",
                                    dataField: "fee",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "TOTAL",
                                    dataField: "total",
                                    dataType:"number",
                                    format: "Rp #,##0.##"
                                }
                            ],
                             summary: {
                                  totalItems: [{
                                      column: "users_name",
                                      summaryType: "count"
                                  },
                                  {
                                      column: "amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                  {
                                      column: "fee",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",

                                  }],
                                   groupItems: [{
                                      column: "users_name",
                                      summaryType: "count",
                                      displayFormat: "{0} BT"
                                  },
                                  {
                                      column: "amount",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "fee",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  },
                                  {
                                      column: "total",
                                      summaryType: "sum",
                                      valueFormat: "fixedPoint",
                                      precision: '2',
                                      displayFormat: "Total Rp {0}",
                                      showInGroupFooter: false,
                                      alignByColumn: true

                                  }]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

  $("#autoExpandW").dxCheckBox({
        value: false,
        text: "Expand All Groups",
        onValueChanged: function(data) {
           var dataGrid = $("#grid-withdraw").dxDataGrid("instance");
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
  });

  })(jQuery);
</script>