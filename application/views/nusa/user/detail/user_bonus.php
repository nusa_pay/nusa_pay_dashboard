<div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL AVAILABLE POINT</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['available_point'])?></h2>
                </div>
              </div>

</div>
<div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL SPENT COIN</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['spent_coin'])?></h2>
                </div>
              </div>
</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL SPENT POINT</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['spent_point'])?></h2>
                </div>
              </div>
</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL COLLECTED COIN</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['collected_coin'])?></h2>
                </div>
              </div>
</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL COLLECTED POINT</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['collected_point'])?></h2>
                </div>
              </div>
          </div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

			<div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL EXPIRED POINT</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['expired_point'])?></h2>
                </div>
              </div>            
</div>
<div class="col-md-6 col-lg-3 grid-margin stretch-card">

      <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL AVAILABLE COIN</h6>
                  <h2 class="mb-0"><?= number_format($total_promotion_user['available_coin'])?></h2>
                </div>
              </div>            
</div>
</div>

<div id="row grid-margin">
  <div id="grid-promotion"></div>
</div>

<script type="text/javascript">
            var url = "<?php echo base_url() ?>nusa/users/json_get_promotion_user/<?php echo $userId ?>";
            
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id',type: 'string' },
                    { name: 'name',type: 'string' },
                    { name: 'available_point', type: 'int'},
                    { name: 'spent_point', type: 'int'},
                    { name: 'collected_point', type: 'int'},
                    { name: 'expired_point', type: 'int'},
                    { name: 'available_coin', type: 'int'},
                    { name: 'spent_coin', type: 'int'},
                    { name: 'collected_coin', type: 'int'},
                    { name: 'status', type: 'string'},
                    { name: 'created_at', type: 'date' },
                    { name: 'updated_at', type: 'date' },
                
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-promotion").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [
                   {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'id', hidden: true },
                  { text: 'Merchant Name', dataField: 'name', width: "10%" },
                  { text: 'Available Point', dataField: 'available_point', width: "10%", cellsformat: "number" },
                  { text: 'Spent Point', dataField: 'spent_point', width: "10%", cellsformat: "number" },
                  { text: 'Collected Point', dataField: 'collected_point', width: "10%", cellsformat: "number" },
                  { text: 'Expired Point', dataField: 'expired_point', width: "10%", cellsformat: "number" },
                  { text: 'Available Coin', dataField: 'available_coin', width: "10%", cellsformat: "number" },
                  { text: 'Spent Coin', dataField: 'spent_coin', width: "10%", cellsformat: "number" },
                  { text: 'Collected Coin', dataField: 'collected_coin', width: "10%", cellsformat: "number" },
                  { text: 'status', dataField: 'status', width: "10%" },
                  { text: 'Created', dataField: 'created', width: "10%" ,filtertype: 'date',cellsformat: 'd-MMM-yyyy H:m:s'}
                ]
            });
       
    </script>
