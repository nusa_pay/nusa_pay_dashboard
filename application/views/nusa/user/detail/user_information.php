<div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Detail</h4>
                  <form class="form-sample">
                    <p class="card-description">
                      Personal info
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['name'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nickname</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['nickname'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['email'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['phone'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                      <!-- <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Gender</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>Male</option>
                              <option>Female</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Date of Birth</label>
                          <div class="col-sm-9">
                            <input class="form-control" placeholder="dd/mm/yyyy"/>
                          </div>
                        </div>
                      </div> -->
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Gender</label>
                          <div class="col-sm-9">
                            <?php if ($user_detail['gender'] == '0') :?>
                            <input type="text" class="form-control" value="<?= 'FEMALE' ?>" readonly="readonly" />
                            <?php elseif($user_detail['gender'] == 1): ?>
                            <input type="text" class="form-control" value="<?= 'MALE' ?>" readonly="readonly" />
                            <?php else : ?>
                               <input type="text" class="form-control" value="<?= '-' ?>" readonly="readonly" />
                            <?php endif; ?>
                          </div>
                        </div>
                      </div>

                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">User Status</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['user_status_name'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>

                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Place Of Birth</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['place_of_birth'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Date Of Birth</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['date_of_birth'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">User Type</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['user_type_name'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Last Login</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['last_login'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Status KYC</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['status_kyc_name'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Indentity Number</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_detail['indentity_number'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>
                     <div class="row">
                       
                    </div>
                    
                    <p class="card-description">
                      Address
                    </p>
                   
                   <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['address_name'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Adress</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['address'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Phone Address</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['phone_address'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Provinces</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['provinces'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>

                       <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Cities</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['cities'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sub District</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['subdistrict'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Village</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['villages'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Postal Code</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= $user_address['postal_code'] ?>" readonly="readonly" />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                     <p class="card-description">
                      Profiling
                    </p>

                    <div id="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job</label>
                          <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" value=""  /> -->
                            <select name="job_profiling" class="form-control" id="job_profiling">
                              <option value=""></option>
                              <?php foreach ($jobs as $value): ?>
                              <option <?php if ($user_detail['job_profilling'] == $value['id']): ?>
                                selected
                              <?php endif ?> value="<?=$value['id'] ?>"><?= $value['job_name'] ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Country</label>
                          <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" value=""  /> -->
                            <div id="country"></div>
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <button type="button" id="btnSaveProfiling" onClick="saveProfiling()" class="btn btn-default">Update Profiling</button>
                        </div>
                      </div>
                    </div>


                  </form>
                </div>
              </div>
            </div>
            <script>

              function saveProfiling()
              {
                $.ajax({
                  url: '<?= base_url() ?>nusa/users/update_profiling/<?= $userId ?>',
                  type: 'POST',
                  dataType: 'json',
                  data: {job_profilling: $("#job_profiling :selected").val(), nationality: $("#country input:hidden").val()}
                })
                .done(function() {
                   DevExpress.ui.notify("Update Profiling Success", 'success', 600);
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });

              
                
              }

               $("#country").dxSelectBox({
                    dataSource: "<?=base_url()  ?>assets/country.json" ,
                    displayExpr: "name",
                    valueExpr: "name",
                    value: "<?= $user_detail['nationality'] ?>",
                    searchEnabled: true
                });

             
            </script>