<div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">  WALLET LOCKING IN AND OUT</h4>
                  <form class="form-sample" id="formUpdateLocking" action="<?php echo base_url() ?>nusa/users/walletSetting/<?php echo $userId ?>" method="post">
                    <p class="card-description">
                     <!-- <input type="hidden" name="setting_type" value="locking"> -->
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 value="lock_in" col-form-label">LOCK IN</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="lock_in" id="lock_in">
                              <option value="0" <?php if ($wallet_balance['lock_in'] == 0): ?>
                                selected
                              <?php endif ?> >ACTIVE</option>
                              <option value="1" <?php if ($wallet_balance['lock_in'] == 1): ?>
                                selected
                              <?php endif ?>>LOCKED</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">LOCK OUT</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="lock_out" id="lock_out">
                              <option value="0" <?php if ($wallet_balance['lock_out'] == 0): ?>
                                selected
                              <?php endif ?> >ACTIVE</option>
                              <option value="1" <?php if ($wallet_balance['lock_out'] == 1): ?>
                                selected
                              <?php endif ?>>LOCKED</option>
                            </select>
                          </div>
                        </div>
                      </div>
                     
                    </div>
                    <div class="row">
                      <button type="button"  data-toggle="modal" data-target="#modalLockingWallet" class="btn btn-default">Update</button>
                    </div>
          
                   
                 
                  </form>
                </div>
              </div>
            </div>

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">  WALLET LIMIT</h4>
                  <form class="form-sample" action="<?php echo base_url() ?>nusa/users/walletBalanceSetting/<?php echo $userId ?>" method="post">
                    <p class="card-description">
                     
                    </p>
                    
                    
                    
                   <div class="row">
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Max Balance</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" value="<?= $walletLimit['balance'] ?>" id="max_balance" required="required" />
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Max Transaction</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" id="max_transaction" value="<?= $walletLimit['transaction'] ?>" required="required"  />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Documents</label>
                          <div class="col-sm-9">
                            <!-- <input type="file" name="limit_document" value="" placeholder="upload doc" required="required"> -->
                            <input type="hidden" class="form-control" name="limit_document" value="<?= $walletLimit['documents'] ?>" id="limit_document" />
                            <div id="file-uploader-doc"></div>
                          </div>

                        </div>
                      </div>
                      <?php if ($walletLimit['documents'] != ""): ?>
                            <?php $walletLimit['documents'] = json_decode($walletLimit['documents']); ?>
                            <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Documents</label>
                          <div class="col-sm-9">
                             <a href="<?= $walletLimit['documents']->url ?>" target="_blank"><?= $walletLimit['documents']->name?></a>
                          </div>
                        </div>
                      </div>
                            <?php endif ?>
                   </div>

                   <div class="row">
                     <button type="button" data-toggle="modal" data-target="#modalLimitWallet" class="btn btn-default">Update</button>
                   </div>
                   
                 
                  </form>
                </div>
              </div>
            </div>

<!-- Modal locking -->
<div class="modal fade" id="modalLockingWallet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Wallet Locking</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        
        <span aria-hidden="true">&times;</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p>Are you sure want to Locking / Unlocking Wallet data current user ? Please explain to the Reason.</p>
        <form id="formUpdateLockingReason" method="post" action="<?= site_url() ?>nusa/users/suspendAccount/<?= $userId ?>" accept-charset="utf-8">
          
          <div class="form-group">
            <label for="field-subject">Ref Ticket</label>
            <select name="ref_ticket_id" class="form-control">
              <option value=""></option>
              <?php foreach ($tickets as $value): ?>
              <option value="<?= $value['id'] ?>"><?= $value['subject'] ?></option>
              <?php endforeach ?>
            </select>
          </div>

          <div class="form-group">
            <label for="field-subject">Subject</label>
            <input type="text" required id="suspend-subject" name="subject" placeholder="Suspend Subject" class="form-control" value="">
          </div>
          <div class="form-group">
            <label for="field-content">Reason</label>
            <textarea id="suspend-content" required name="content" rows="4" cols="50" placeholder="Suspend Reason" class="form-control"></textarea>
            
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeLocking">Close</button>
        <button type="button" id="btnLockingSave" class="btn btn-primary">Save</button>
      </div>
        </form>
        
      </div>
      
    </div>
  </div>
</div>

<!-- Modal locking -->
<div class="modal fade" id="modalLimitWallet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Wallet Limit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        
        <span aria-hidden="true">&times;</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p>Are you sure want to Limit Wallet data current user ? Please explain to the Reason.</p>
        <form id="formUpdateLimitReason" method="post" action="<?= site_url() ?>nusa/users/updateLimitWallet/<?= $userId ?>" accept-charset="utf-8">
          
          <div class="form-group">
            <label for="field-subject">Ref Ticket</label>
            <select name="ref_ticket_id" class="form-control">
              <option value=""></option>
              <?php foreach ($tickets as $value): ?>
              <option value="<?= $value['id'] ?>"><?= $value['subject'] ?></option>
              <?php endforeach ?>
            </select>
          </div>

          <div class="form-group">
            <label for="field-subject">Subject</label>
            <input type="text" required id="suspend-subject" name="subject" placeholder="Suspend Subject" class="form-control" value="">
          </div>
          <div class="form-group">
            <label for="field-content">Reason</label>
            <textarea id="suspend-content" required name="content" rows="4" cols="50" placeholder="Suspend Reason" class="form-control"></textarea>
            
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeLocking">Close</button>
        <button type="button" id="btnLimitSave" class="btn btn-primary">Save</button>
      </div>
        </form>
        
      </div>
      
    </div>
  </div>
</div>

<script>
  $(function () {

    $("#btnLockingSave").on('click', function(event) {
      
      /* Act on the event */
       $.ajax({
      url: '<?= base_url() ?>nusa/users/updateLockingWallet/<?= $userId ?>',
      type: 'POST',
      dataType: 'json',
      data: {formUpdateLocking: $("#formUpdateLocking").serializeArray(), formUpdateLockingReason: $("#formUpdateLockingReason").serializeArray() },
    })
    .done(function(data) {
      DevExpress.ui.notify("Update Locking Wallet Success", 'success', 600);
      window.location = "<?= base_url() ?>nusa/users/detail/<?= $userId ?>";

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

    });

    $("#btnLimitSave").on('click', function(event) {
      
      /* Act on the event */
       $.ajax({
      url: '<?= base_url() ?>nusa/users/updateLimitWallet/<?= $userId ?>',
      type: 'POST',
      dataType: 'json',
      data: {balance: $("#max_balance").val(), transaction: $("#max_transaction").val(), documents: $('#limit_document').val(),formUpdateLimitReason: $("#formUpdateLimitReason").serializeArray()  },
    })
    .done(function(data) {
      DevExpress.ui.notify("Update Limit Wallet Success", 'success', 600);
      window.location = "<?= base_url() ?>nusa/users/detail/<?= $userId ?>";

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

    });

    $("#file-uploader-doc").dxFileUploader({
        multiple: false,
        uploadMode: "useButtons",
        uploadUrl: "/",
        allowedFileExtensions: [".pdf", ".xls", ".xlsx", ".doc", ".docx"],
        onUploadStarted: function(e){
          var file = e.file;
          var formData = new FormData(),
            xhr      = new XMLHttpRequest();

            formData.append('file', file, file.name);
            // formData.append('disk', 'local');

            xhr.open('POST', '<?= $upload_url ?>', true);
            xhr.setRequestHeader('Authorization', '<?= $atoken ?>');

             xhr.onreadystatechange = function(){
            if(xhr.readyState != 4)
                return;

            if(xhr.status != 200)
                return DevExpress.ui.notify("Upload Failed", 'error', 600);

            var res;
            try{
                res = JSON.parse(xhr.responseText);
            }catch(e){
            }

            if(!res)
                return DevExpress.ui.notify("invalid json", 'error', 600);

            if(!res.success)
                return  DevExpress.ui.notify(res.message, 'error', 600);

            $("#limit_document").val(JSON.stringify(res.data));

            //$inp.val(res.data.url);
        }

        xhr.send(formData);

        }
    });

  

   });
</script>