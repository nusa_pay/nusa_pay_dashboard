<div class="col-12 grid-margin">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$user_detail['name'];?></h4>
				<p class="card-description"><?= "REFERRALS BY" ?></p>
				<div id="grid-referralby"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-12 grid-margin">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$user_detail['name'];?></h4>
				<p class="card-description"><?= "REFERRALS" ?></p>
				<div id="grid-referral"></div>
			</div>
		</div>
	</div>
</div>
<script>
  (function($) {
    var no_avatar = "<?php echo base_url() ?>/assets/images/no_avatar.jpg"
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $user_referral; ?>;
        items.resolve(data);
        return items.promise();
    }
	};

	var dataSourceBy = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $user_referral_by; ?>;
        items.resolve(data);
        return items.promise();
    }
	};

$("#grid-referralby").dxDataGrid({
                    dataSource: dataSourceBy,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportTopUp"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },
                            columns: [
                                
                                {
                                    caption: "Name",
                                    dataField: "name"
                                },
                                {
                                    caption: "Nickname",
                                    dataField: "nickname"
                                },
                                {
                                    caption: "Phone",
                                    dataField: "phone"
                                },
                                {
                                    caption: "Avatar",
                                    dataField: "avatar",
                                    cellTemplate: function (container, options) {
                                    	var src = options.value;
                                    	if (src == null) {
                                    		src = no_avatar;
                                    	}
                                    	console.log(options.value);
					                    $("<div>")
				                        .append($("<img>", { "src": src, "width": "120", "height": "120" }))
					                        .appendTo(container);
					                }
                                }
                            ]
                             
                        });

   $("#grid-referral").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportTopUp"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },
                            columns: [
                                
                                {
                                    caption: "Name",
                                    dataField: "name"
                                },
                                {
                                    caption: "Nickname",
                                    dataField: "nickname"
                                },
                                {
                                    caption: "Phone",
                                    dataField: "phone"
                                },
                                {
                                    caption: "Avatar",
                                    dataField: "avatar",
                                    cellTemplate: function (container, options) {
                                    	var src = options.value;
                                    	if (src == null) {
                                    		src = no_avatar;
                                    	}
                                    	console.log(options.value);
					                    $("<div>")
				                        .append($("<img>", { "src": src, "width": "120", "height": "120" }))
					                        .appendTo(container);
					                }
                                }
                            ]
                             
                        });
  })(jQuery);
</script>