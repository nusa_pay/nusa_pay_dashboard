<div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL ALL TRANSACTION</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($user_total_trx,2)?></h2>
                </div>
              </div>

</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL TRANSACTION CANCELED</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($user_total_trx_cancel,2)?></h2>
                </div>
              </div>
</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL TRANSACTION Pending</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($user_total_trx_pending,2)?></h2>
                </div>
              </div>
</div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL TRANSACTION NEW</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($user_total_trx_new,2)?></h2>
                </div>
              </div>
          </div>
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">

			<div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL TRANSACTION DONE</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($user_total_trx_done,2)?></h2>
                </div>
              </div>            
</div>
</div>
  
<div id="grid-transaction"></div>
<script type="text/javascript">
            var url = "<?php echo base_url() ?>nusa/users/json_getTrxUser/<?= $userId ?>";
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'cart_id',type: 'string' },
                    { name: 'status_cart',type: 'string' },
                    { name: 'created', type: 'date'},
                    { name: 'description', type: 'string' },
                    { name: 'total_cart', type: 'int' }
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-transaction").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                altrows: true,
                sortable: true,
                adaptive: true,
                columns: [

                     {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'cart_id', dataField: 'cart_id', hidden:true },
                  { text: 'Status Trx', dataField: 'status_cart', width: "30%" },
                  { text: 'Trx Date', dataField: 'created', width: "20%", filtertype: 'date',cellsformat: 'd-MMM-yyyy'},
                  { text: 'Description', dataField: 'description', width: "30%" },
                  { text: 'Total', dataField: 'total_cart', width: "10%", cellsformat: "decimal" }
                ]
            });
      
       
    
    </script>