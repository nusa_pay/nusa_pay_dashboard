<div class="row grid-margin">
 <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">WALLET BALANCE</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($wallet_balance['balance'],2)?></h2>
                </div>
              </div>

</div>

<div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">WALLET TRANSACTION</h6>
                  <h2 class="mb-0"><?= 'Rp. '.number_format($wallet_limit_trx,2)?></h2>
                </div>
              </div>

</div>

</div>
 <h6 class="font-weight-normal">Top Up</h6>
<div id="grid-wallet"></div>
<div id="row">
	
</div>
 <h6 class="font-weight-normal">Transaction History</h6>

<div id="grid-wallet-transaction"></div>
<script type="text/javascript">
            var url = "<?php echo base_url() ?>nusa/users/json_get_wallet_top_up/<?php echo $userId ?>";
            
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id',type: 'string' },
                    { name: 'amount',type: 'int' },
                    { name: 'status', type: 'string'},
                    { name: 'status_name', type: 'string'},
                    { name: 'created', type: 'date' }
                
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-wallet").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [
                   {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'id', hidden: true },
                  { text: 'Amount', dataField: 'amount', width: "32%", cellsformat: "number" },
                  { text: 'status', dataField: 'status_name', width: "32%" },
                  { text: 'Created', dataField: 'created', width: "32%" ,filtertype: 'date',cellsformat: 'd-MMM-yyyy H:m:s'}
                ]
            });
       
    </script>

<!-- user transaction -->
    <script type="text/javascript">
            var url = "<?php echo base_url() ?>nusa/users/json_get_wallet_trx/<?php echo $userId ?>";
            
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id',type: 'string' },
                    { name: 'amount',type: 'int' },
                    { name: 'type_name',type: 'int' },
                    { name: 'status', type: 'string'},
                    { name: 'status_name', type: 'string'},
                    { name: 'reason', type: 'string'},
                    { name: 'description', type: 'string'},
                    { name: 'created', type: 'date' }
                
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-wallet-transaction").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [
                   {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'id', hidden: true },
                  { text: 'Description', dataField: 'description', width: "20%" },
                  { text: 'Amount', dataField: 'amount', width: "20%", cellsformat: "number" },
                  { text: 'Type', dataField: 'type_name', width: "20%" },
                  { text: 'Status', dataField: 'status_name', width: "20%" },
                  { text: 'Created', dataField: 'created', width: "20%" ,filtertype: 'date',cellsformat: 'd-MMM-yyyy H:m:s'}
                ]
            });
       
    </script>