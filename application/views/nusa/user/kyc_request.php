
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?= $cardTitle; ?></h4>
                  <div class="d-flex table-responsive">
                    <div class="btn-group mr-2">
                      <!-- <button class="btn btn-sm btn-primary" id="refreshGrid"><i class="fa fa refresh"></i> Refresh</button> -->
                    </div>
                  </div>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div id="grid-nusa-users"></div>
                </div>
              </div>
            </div>
          </div>
          <?php $this->load->view("jqwidget");
            ?>
<script type="text/javascript">
            var url = "<?php echo base_url() ?>nusa/users/json_getKycUser";
            var url_kycDetail = "<?php echo base_url() ?>nusa/users/kycRequestDetail/";
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id',type: 'string' },
                    { name: 'name',type: 'string' },
                    { name: 'email', type: 'string'},
                    { name: 'username', type: 'int' },
                    { name: 'phone', type: 'int' },
                    { name: 'created_at', type: 'date' },
                    { name: 'user_status_name',type:'string' },
                    { name: 'user_type_name',type:'string' }
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-nusa-users").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [

                     {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'id', hidden:true },
                  { text: 'Name', dataField: 'name', width: "30%" },
                  { text: 'Email', dataField: 'email', width: "20%" },
                  { text: 'Phone', dataField: 'phone', width: "16%" },
                  { text: 'User Type', dataField: 'user_type_name', width: "10%" },
                  { text: 'User Status', dataField: 'user_status_name', width: "10%" },
                  { text: 'Register', dataField: 'created_at', width: "16%" ,filtertype: 'date',cellsformat: 'd-MMM-yyyy'}
                ]
            });
      
       $('#grid-nusa-users').on('rowdoubleclick', function (event) {
            // event.args.rowindex is a bound index.
            var id = event.args.row.bounddata.id;
            window.open(url_kycDetail+id,"_self");
            
        });
      
       $("#refreshGrid").click(function (e) { 
                //alert('test');
                //$("#grid-nusa-users").jqxGrid('updatebounddata');
            //$("#grid-nusa-users").jqxGrid({ source: dataAdapter });
        });
 

    
    </script>
  

