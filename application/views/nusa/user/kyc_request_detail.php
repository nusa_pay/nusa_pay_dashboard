<div class="row">
  <div class="col-md-4 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Avatar</h4>
        <a href="<?= $kycReqData['avatar'] ?>" target="_blank"><img class="" src="<?= $kycReqData['avatar'] ?>" alt="profile" width="431" height="557"></a>
      </div>
    </div>
  </div>
  
  <div class="col-md-4 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Photo</h4>
        <a href="<?= $kycReqData['avatar'] ?>" target="_blank"><img class="" src="<?= $kycReqData['photo'] ?>" alt="profile" width="431" height="557"></a>
      </div>
    </div>
  </div>
  
  <!-- <div class="col-md-4 grid-margin stretch-card">
                  <div class="card">
                      <div class="card-body">
                          <h4 class="card-title">Indentity Image</h4>
        <a href="<?= $kycReqData['avatar'] ?>" target="_blank"><img class="" src="<?= $kycReqData['indentity_image'] ?>" alt="profile" width="431" height="557"></a>
      </div>
    </div>
  </div> -->
</div>
<div class="row">
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">User Detail</h4>
        <p class="card-description">
          
        </p>
        <form class="forms-sample" method="post" action="<?= base_url()?>nusa/users/saveKycRequest">
          <div class="form-group">
            <label for="exampleInputName1">Name</label>
            <input type="hidden" class="form-control" name="id" id="id" placeholder="id" readonly value="<?= $kycReqData['id'] ?>"">
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" readonly value="<?= $kycReqData['name'] ?>"">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail3">Email address</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Email" readonly value="<?= $kycReqData['email'] ?>"">
          </div>
          <div class="form-group">
            <label for="exampleInputName1">Mother Name</label>
            <input type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Mother Name" readonly value="<?= $kycReqData['mother_name'] ?>">
          </div>
          
          <div class="form-group">
            <label for="exampleSelectGender">Identity Type</label>
            <select class="form-control" name="indentity_type">
              <option value="KTP" <?php ($kycReqData['indentity_type'] == "KTP") ?  "selected": ""  ?> >KTP</option>
              <option value="SIM" <?php ($kycReqData['indentity_type'] == "SIM") ?  "selected": ""  ?> >SIM</option>
              <option value="Kartu Pelajar" <?php ($kycReqData['indentity_type'] == "Kartu Pelejar") ?  "selected": ""  ?> >Kartu Pelajar</option>
              <option value="Paspor" <?php ($kycReqData['indentity_type'] == "Paspor") ?  "selected" :  ""  ?> >Paspor</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputName1">Identity Number</label>
            <input type="text" class="form-control" name="indentity_number" id="exampleInputName1" placeholder="Identity Number" value="<?= $kycReqData['indentity_number'] ?>">
          </div>
          
          
          <button type="submit" class="btn btn-primary mr-2">Verify</button>
          <button type="button" class="btn btn-danger mr-2" data-toggle="modal" data-target="#modalReject">Reject</button>
          <button type="button" class="btn btn-light" onclick="window.open(<?php echo site_url('nusa/user/kyc-request'); ?>)">Cancel</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">KYC Rejection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        
        <span aria-hidden="true">&times;</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p>Are you sure want to reject current user KYC request? Please explain to the user rejection reason.</p>
        <form id="formRejectionKYC" method="post" action="<?= site_url() ?>kyc/Kyclog/saveRejection/<?= $id ?>" accept-charset="utf-8">
          
          <div class="form-group">
            <label for="field-subject">Rejection Subject</label>
            <input type="text" required id="rejection-subject" name="subject" placeholder="Rejection Subject" class="form-control" value="">
          </div>
          <div class="form-group">
            <label for="field-content">Rejection Reason</label>
            <textarea id="rejection-content" required name="content" rows="4" cols="50" placeholder="Rejection Reason" class="form-control"></textarea>
            
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="btnKycReject" class="btn btn-primary">Save</button>
      </div>
        </form>
        
      </div>
      
    </div>
  </div>
</div>
<script>
(function($) {
  // $('#btnKycReject').on('click', function(event) {
  // event.preventDefault();
  // /* Act on the event */
  // alert("test");
  // });
})(jQuery);
</script>