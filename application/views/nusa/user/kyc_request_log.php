
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?= $cardTitle; ?></h4>
                  <div class="d-flex table-responsive">
                    <div class="btn-group mr-2">
                      <!-- <button class="btn btn-sm btn-primary" id="refreshGrid"><i class="fa fa refresh"></i> Refresh</button> -->
                    </div>
                  </div>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div id="grid-container"></div>
                </div>
              </div>
            </div>
          </div>
          <?php $this->load->view("devextreme");
            ?>
<script type="text/javascript">
              (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $logData; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#grid-container").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                     groupPanel: {
                        visible: true
                    },
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "logKYC"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                 {
                                    caption: "operator name",
                                    dataField: "first_name",
                                },
                                {
                                    caption: "Users Name",
                                    dataField: "name",
                                },
                                 {
                                    caption: "DATE CREATED",
                                    dataField: "created",
                                    dataType: "date",
                                     format: 'dd-MMMM-yyyy hh:mm:ss',
                                },
                                {
                                    caption: "Status KYC",
                                    dataField: "rejection_name"
                                },
                                {
                                    caption: "Rejection Subject",
                                    dataField: "rejection_subject"
                                },
                                {
                                    caption: "Reason",
                                    dataField: "rejection_reason"
                                }
                            ],
                             summary: {
                               groupItems: [
                               {
                                      column: "first_name",
                                      summaryType: "count",
                                        displayFormat: "{0} COUNT",
                                  },
                                  // {
                                  //     column: "name",
                                  //     summaryType: "count",
                                  //       displayFormat: "{0} COUNT",
                                  // },
                                  // {
                                  //     column: "rejection_name",
                                  //     summaryType: "count",
                                  //       displayFormat: "{0} COUNT",
                                  // },
                                   ],

                                  totalItems: [{
                                      column: "first_name",
                                      summaryType: "count",
                                        displayFormat: "{0} COUNT",
                                  },
                                   ]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 $("#autoExpand").dxCheckBox({
        value: false,
        text: "Expand All Groups",
        onValueChanged: function(data) {
           var dataGrid = $("#grid-withdraw").dxDataGrid("instance");
            dataGrid.option("grouping.autoExpandAll", data.value);
        }
  });

  })(jQuery);
</script>
  

