
<div class="row grid-margin">
 <!-- <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL USER</h6>
                  <h2 class="mb-0"><?= $countAllUser?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL USER ACTIVE</h6>
                   <h2 class="mb-0"><?= $countActiveUser?></h2>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL USER NOT ACTIVE</h6>
                  <h2 class="mb-0"><?= $countNotActiveUser ?></h2>
                </div>
              </div>
            </div>
             <div class="col-md-6 col-lg-3 grid-margin stretch-card">
              <div class="card bg-gradient-info text-white text-center card-shadow-info">
                <div class="card-body">
                  <h6 class="font-weight-normal">TOTAL USER SUSPEND</h6>
                   <h2 class="mb-0"><?= $countSuspendUser?></h2>
                </div>
              </div>
            </div> -->

            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div id="gridcontainer"></div>
                </div>
              </div>
            </div>
          </div>
         <?php $this->load->view("devextreme");?>
<script>
  (function($) {
    
    var dataSource = {
    load: function() {
        var items = $.Deferred();
        var data= <?php echo $merchants; ?>;
        items.resolve(data);
        return items.promise();
    }
};

   $("#gridcontainer").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "Merchants"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "Merchant Name",
                                    dataField: "name"
                                },
                                {
                                    caption: "Email",
                                    dataField: "email"
                                },
                                {
                                    caption: "User Name",
                                    dataField: "username"
                                },
                                {
                                    caption: "Phone",
                                    dataField: "phone"
                                },
                                {
                                    caption: "DATE CREATED",
                                    dataField: "created_at",
                                    dataType: "date",
                                },
                                 {
                                    caption: "USER STATUS",
                                    dataField: "user_status_name"
                                },
                                {
                                  caption: "",
                                   alignment: "center",
                                  cellTemplate: function (container, options) {
                                      var id = options.key.id;
                                      var url = "<?php echo base_url() ?>nusa/users/detail/" + id
                                       $("<div id='btnDetail' />").dxButton({
                                              //icon: 'f',
                                              id:"btnDetail",
                                              text:"Detail",
                                              type: "default",
                                              //disabled: disabled,
                                              onClick: function (e) {
                                                    window.open(url)
                                              }
                                          }).appendTo(container);
                                    }
                                }
                               
                            ],
                             summary: {
                                  totalItems: [{
                                      column: "name",
                                      summaryType: "count"
                                  }
                                  // {
                                  //     column: "total",
                                  //     summaryType: "sum",
                                  //     valueFormat: "currency",
                                  //     editorOptions: {
                                  //       format: "Rp #,##0.##"
                                  //     }

                                  // }
                                  ]
                              },
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });
  })(jQuery);
</script>

