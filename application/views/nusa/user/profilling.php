<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Master Profilling</h4>
                  <p><button type="button" class="btn btn-primary" data-toggle="modal" href='#profilling-modal'><i class="fa fa-plus"></i></button></p>
                 <div id="grid-profilling"></div>
                </div>
              </div>
            </div>
          
          </div>

<div class="modal fade" id="profilling-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url() ?>nusa/users/save_profilling" method="POST" role="form">
          <input type="hidden" name="id" id="profilling_id" value="">
          <div class="form-group">
            <label>Job Name</label>
            <input type="text" class="form-control" name="job_name" id="job_name" placeholder="Job_name" required="required">
          </div>

          <div class="form-group">
            <label>Risk Type</label>
            <?php $risk_type = $this->db->get('job_profiling_type')->result_array(); ?>
            <select name="risk_type" id="risk_type" class="form-control" required="required">
              <?php foreach ($risk_type as $ket =>$value) :?>
              <option value="<?= $value['id'] ?>"><?= $value['risk_name'] ?></option>
            <?php endforeach; ?>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
          <?php $this->load->view("jqwidget");?>


  <script type="text/javascript">
            //var dateFrom = '2018-01-01';//$("#dateFrom").val();
            //var dateTo = '2018-10-30';//$("#dateTo").val();
            //var amount = $("#amount").val();
            var url = "<?php echo base_url() ?>nusa/users/json_get_list_master_profilling";
            // var url = "<?php echo base_url() ?>nusa/users/json_get_user_transaction_report?dateFrom="+dateFrom+"&dateTo="+dateTo+"&amount="+amount;
            
            // prepare the data
            var source =
            {
                datatype: "json",
                 updaterow: function (rowid, rowdata, commit) {
                    // synchronize with the server - send update command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failder.
                    $.ajax({
                      url: '<?php echo base_url() ?>nusa/users/save_profilling/'+rowdata.id,
                      type: 'post',
                      dataType: 'json',
                      data: {id:rowdata.id,job_name: rowdata.job_name,risk_name:rowdata.risk_name},
                    })
                    .done(function(data) {
                     $("#grid-profilling").jqxGrid("updatebounddata");
                    })
                    .fail(function() {
                      console.log("error");
                    })
                    .always(function() {
                      console.log("complete");
                    });
                    
                },
                datafields: [
                    { name: 'id',type: 'string' },
                    { name: 'job_name',type: 'string' },
                    { name: 'risk_type',type: 'string' }, 
                    { name: 'risk_name', type: 'string'}
                
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-profilling").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                editable: true,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [
                   {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'id', hidden: true },
                  { text: 'Name', dataField: 'job_name', width: "50%" },
                  { text: 'Type', dataField: 'risk_name', width: "50%",columntype:'combobox' }
                ]
            });

            $('#grid-profilling').on('rowdoubleclick', function (event) {
            // event.args.rowindex is a bound index.
            var id = event.args.row.bounddata.id;
            var job_name = event.args.row.bounddata.job_name;
            var risk_type = event.args.row.bounddata.risk_type;
            //console.log(event.args.row.bounddata);
            //$('#profilling_id').val(id);
            //$('#job_name').val(job_name);
            //$('#risk_type :selected').val(risk_type);
            // $('#profilling-modal').modal("show");
            //window.open(url_userDetail+id,"_self");
            
        });
       
    </script>
    <script type="text/javascript">
      $(window).load(function() {
    
      });
    </script>
