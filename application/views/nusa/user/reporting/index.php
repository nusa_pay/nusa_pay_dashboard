<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">User Location</h4>
                  <div class="google-chart-container">
                     <div id="Donut-chart" class="google-charts"></div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
          

<!-- user reporting -->
    <div class="row">
             <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">User Transaction Report</h4>
                  
                  <form action="" method="POST" role="form" id="filter_form">
                  
                  
                    <div class="form-group">
                      <label for="">Date From</label>
                      <input type="date" class="form-control" id="dateFrom" name="dateFrom" value=""  placeholder="Input field">
                    </div>

                     <div class="form-group">
                      <label for="">Date To</label>
                      <input type="date" class="form-control" id="dateTo" name="dateTo" value=""  placeholder="Input field">
                    </div>

                    <div class="form-group">
                      <label for="">Amount Transaction > </label>
                      <input type="number" class="form-control" value=0 id="amount" name="amount" placeholder="Input field">
                    </div>
                  
                    
                  
                    <button type="button" id="btnSearchReport" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
    </div>

    <div class="row">
             <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Report Table</h4>
                  <p><button type="button" id="btnExportExcel" class="btn btn-default">Export To excel</button></p>
                 <div id="grid-userReport"></div>
                </div>
              </div>
            </div>
    </div>
  

    
  <!-- Plugin js for this page-->

  <script src="https://www.gstatic.com/charts/loader.js"></script>
  <!-- End plugin js for this page-->
  <!-- Custom js for this page-->
  <!-- <script src="<?php echo base_url() ?>assets/serein/js/google-charts.js"></script> -->
  <script type="text/javascript">
google.charts.load('current', {
    'packages': ['geochart'],
    // Note: you will need to get a mapsApiKey for your project.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
});
// Donut Chart Starts
google.charts.load("current", {
    packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
   
    var data = google.visualization.arrayToDataTable([
        ['city', 'amount'],
        <?php foreach ($userMasterLocation as $key => $value){
          echo "['{$key}', {$value}],";
        }?>
    ]);

    var options = {
        //title: 'My Daily Activities',
        pieHole: 0.4,
        colors: ['#76C1FA', '#63CF72', '#F36368', '#FABA66','black'],
        chartArea: {
            width: 500
        },
    };

    var Donutchart = new google.visualization.PieChart(document.getElementById('Donut-chart'));
    Donutchart.draw(data, options);
}


// Donut Chart Ends

  </script>
  <!-- End custom js for this page-->
<?php $this->load->view("jqwidget");?>


  <script type="text/javascript">
            //var dateFrom = '2018-01-01';//$("#dateFrom").val();
            //var dateTo = '2018-10-30';//$("#dateTo").val();
            //var amount = $("#amount").val();
            var url = "<?php echo base_url() ?>nusa/users/json_get_user_transaction_report";
            // var url = "<?php echo base_url() ?>nusa/users/json_get_user_transaction_report?dateFrom="+dateFrom+"&dateTo="+dateTo+"&amount="+amount;
            
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'user_id',type: 'string' },
                    { name: 'name',type: 'string' },
                    { name: 'user_type_name', type: 'string'},
                    { name: 'status_kyc_name', type: 'string'},
                    { name: 'total_trx', type: 'int' }
                
                ],
                 id: 'id',
                url: url,
                root: 'data'
            };
            var theme = 'light';
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid-userReport").jqxGrid(
            {
                width: "100%",
                pageable: true,
                source: dataAdapter,
                columnsresize: true,
                 showfilterrow: true,
                filterable: true,
                theme: theme,
                 altrows: true,
                sortable: true,
                adaptive: true,
                columns: [
                   {
                      text: '#', sortable: false, filterable: false, editable: false,
                      groupable: false, draggable: false, resizable: false,
                      datafield: '', columntype: 'number', width: 50,
                      cellsrenderer: function (row, column, value) {
                          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                      }
                  },
                  { text: 'id', dataField: 'user_id', hidden: true },
                  { text: 'Name', dataField: 'name', width: "20%" },
                  { text: 'User Type', dataField: 'user_type_name', width: "20%" },
                  { text: 'Status Kyc', dataField: 'status_kyc_name', width: "20%" },
                  { text: 'TOTAL', dataField: 'total_trx', width: "30%", filtertype:'number' ,cellsformat: 'decimal'}
                ]
            });

             
       
    </script>

    <script>
  (function($) {

    $('#btnExportExcel').click(function(event) {
      /* Act on the event */
      $("#grid-userReport").jqxGrid('exportdata', 'xls', 'userTrxReport');
    });

$("#btnSearchReport").click(function(event) {
    
              $("#grid-userReport").jqxGrid('updatebounddata');
        /* Act on the event */
        $.ajax({
          url: url,
          type: "post",
          dataType: 'json',
          data: $('#filter_form').serialize(),
        })
        .done(function(data) {
          var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'user_id',type: 'string' },
                    { name: 'name',type: 'string' },
                    { name: 'user_type_name', type: 'string'},
                    { name: 'status_kyc_name', type: 'string'},
                    { name: 'total_trx', type: 'int' }
                
                ],
                 id: 'id',
                localdata: data
             };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $("#grid-userReport").jqxGrid({ source: dataAdapter });
                //$("#grid-userReport").jqxGrid('updatebounddata');
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
        });
          console.log("complete");
        
       // $("#grid-userReport").jqxGrid('updatebounddata');
      });
})(jQuery);
</script>