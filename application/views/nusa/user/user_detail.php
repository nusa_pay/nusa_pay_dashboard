<?php $this->load->view("jqwidget");?>
<?php $this->load->view('devextreme'); ?>

<div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">User Detail</h4>
                  <p class="card-description"><b><?= $user_detail['name'] ?></b></p>
                  <p class="card-description">
                    <?php if ($user_detail['user_status_name'] == 'SUSPEND'): ?>
                    <button type="button"  data-toggle="modal" data-target="#modalFreeze" class="btn btn-primary">UN FREEZE ACCOUNT</button></p>
                       <?php else : ?>
                    <button type="button"  data-toggle="modal" data-target="#modalFreeze" class="btn btn-primary">FREEZE ACCOUNT</button></p>

                    <?php endif ?>
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Information Detail</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="transaction-tab" data-toggle="tab" href="#detail_trx" role="tab" aria-controls="detail_trx" aria-selected="false">Transaction</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="wallet-tab" data-toggle="tab" href="#wallet" role="tab" aria-controls="wallet" aria-selected="false">Wallet</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" id="setting-wallet-tab" data-toggle="tab" href="#setting-wallet" role="tab" aria-controls="contact-1" aria-selected="false">LOCKING & SET LIMIT WALLET </a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" id="bonus-tab" data-toggle="tab" href="#bonus" role="tab" aria-controls="contact-1" aria-selected="false">Bonus </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="referral-tab" data-toggle="tab" href="#referral" role="tab" aria-controls="contact-1" aria-selected="false">Referral</a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                      <?php $this->load->view('nusa/user/detail/user_information');?>
                    </div>
                    <div class="tab-pane fade" id="detail_trx" role="tabpanel" aria-labelledby="transaction-tab">
                      <?php $this->load->view('nusa/user/detail/user_transaction'); ?>
                    </div>
                    <div class="tab-pane fade" id="wallet" role="tabpanel" aria-labelledby="contact-tab">
                      <?php $this->load->view('nusa/user/detail/user_wallet'); ?>
                    </div>
                    <div class="tab-pane fade" id="setting-wallet" role="tabpanel" aria-labelledby="contact-tab">
                      <?php $this->load->view('nusa/user/detail/user_locking_wallet'); ?>
                    </div>
                    <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="contact-tab">
                      <?php $this->load->view('nusa/user/detail/user_bonus'); ?>
                    </div>
                    <div class="tab-pane fade" id="referral" role="tabpanel" aria-labelledby="referra-tab">
                      <?php $this->load->view('nusa/user/detail/user_referral'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

  <!-- Modal -->
<div class="modal fade" id="modalFreeze" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">USER FREEZE / SUSPEND</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        
        <span aria-hidden="true">&times;</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p>Are you sure want to Freeze / Un Frezee data current user ? Please explain the Reason.</p>
        <form id="formRejectionKYC" method="post" action="<?= site_url() ?>nusa/users/suspendAccount/<?= $userId ?>" accept-charset="utf-8">
          
          <div class="form-group">
            <label for="field-subject">Ref Ticket</label>
            <select name="ref_ticket_id" class="form-control">
              <option value=""></option>
              <?php foreach ($tickets as $value): ?>
              <option value="<?= $value['id'] ?>"><?= $value['subject'] ?></option>
              <?php endforeach ?>
            </select>
          </div>

          <div class="form-group">
            <label for="field-subject">Subject</label>
            <input type="text" required id="suspend-subject" name="subject" placeholder="Suspend Subject" class="form-control" value="">
          </div>
          <div class="form-group">
            <label for="field-content">Reason</label>
            <textarea id="suspend-content" required name="content" rows="4" cols="50" placeholder="Suspend Reason" class="form-control"></textarea>
            
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="btnKycReject" class="btn btn-primary">Save</button>
      </div>
        </form>
        
      </div>
      
    </div>
  </div>
</div>