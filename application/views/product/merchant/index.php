<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Merchant Products</h4>
                <div id="js-grid"></div>
            </div>
        </div>
    </div>
</div>

<script>
    var url = "<?php echo base_url() ?>product/merchant/filter";
$('#js-grid').jsGrid({
    width: '100%',
    paging: true,
    pageLoading: true,
    filtering: true,
    sorting: true,
    autoload: true,
    controller: {
        loadData: function(filter){
            var query = {};
            for(var k in filter){
                if(filter[k])
                    query[k] = filter[k];
            }

            return $.ajax({
                type: 'GET',
                url: url,
                data: query
            })
        }
    },
    fields: [
        { name: 'Name', type: 'text' },
        { name: 'Price', type: 'number', filtering: false },
        { name: 'Merchant', type: 'text' },
        { name: 'Industry', type: 'select', items: <?= json_encode(($industries?$industries:[])) ?>, valueField: 'name', textField: 'name' },
        { name: 'Sold', type: 'number', filtering: false }
    ],
    rowClick: function(args){
        var next = '/product/merchant/item/' + args.item.Id;
        location.href = next;
    }
})
</script>