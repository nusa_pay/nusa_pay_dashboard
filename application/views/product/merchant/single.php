<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
            <div class="card-body">
                <h6 class="font-weight-normal">Purchases</h6>
                <h2 class="mb-0"><?= number_format($purchased) ?></h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
            <div class="card-body">
                <h6 class="font-weight-normal">Price Changes</h6>
                <h2 class="mb-0">0</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-warning text-white text-center card-shadow-warning">
            <div class="card-body">
                <h6 class="font-weight-normal">Tickets</h6>
                <h2 class="mb-0">0</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-info text-white text-center card-shadow-info">
            <div class="card-body">
                <h6 class="font-weight-normal">Current Price</h6>
                <h2 class="mb-0"><?= number_format($product->price) ?></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-0">
                <h4 class="card-title">Details</h4>

                <table class="table">
                    <tr>
                        <th>ID</th>
                        <td><?= $product->id ?></td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td><?= hs(($product->name?$product->name:'N/A')) ?></td>
                    </tr>
                    <tr>
                        <th>Code</th>
                        <td><?= hs($product->code?$product->code:'N/A') ?></td>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <td><?= (is_object($product->category_id)?$product->category_id->name:'N/A') ?></td>
                    </tr>
                    <tr>
                        <th>Stock</th>
                        <td><?= number_format($product->stock?$product->stock:0) ?></td>
                    </tr>
                    <tr>
                        <th>Discount</th>
                        <td><?= number_format($product->discount?$product->discount:0) ?></td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td><?= number_format($product->price?$product->price:0) ?></td>
                    </tr>
                    <tr>
                        <th>Agent Price</th>
                        <td><?= number_format($product->price_agent?$product->price_agent:0) ?></td>
                    </tr>
                    <tr>
                        <th>Price Before Discount</th>
                        <td><?= number_format($product->price_before_discount?$product->price_before_discount:0) ?></td>
                    </tr>
                    <tr>
                        <th>Admin Fee</th>
                        <td><?= number_format($product->admin_fee?$product->admin_fee:0) ?></td>
                    </tr>
                    <tr>
                        <th>Service Fee</th>
                        <td><?= number_format($product->service_fee?$product->service_fee:0) ?></td>
                    </tr>
                    <tr>
                        <th>Tax</th>
                        <td><?= ($product->tax?$product->tax:0) ?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td><?= hs($product->description?$product->description:'...') ?></td>
                    </tr>
                    <tr>
                        <th>Created</th>
                        <td><?= $product->created_at->format('Y-m-d H:i:s') ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Merchant</h4>
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <td><?= $product->merchant_id->id ?></td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td><?= $product->merchant_id->name ?></td>
                    </tr>
                    <tr>
                        <th>Owner</th>
                        <td><?= $product->merchant_id->owner ?></td>
                    </tr>
                    <tr>
                        <th>Owner Contact</th>
                        <td><?= $product->merchant_id->owner_contact ?></td>
                    </tr>
                    <tr>
                        <th>Website</th>
                        <td><?= ($product->merchant_id->website?$product->merchant_id->website:'-') ?></td>
                    </tr>
                    <tr>
                        <th>Industry</th>
                        <td>
                            <?php if($industries): ?>
                                <?= $industries ?>
                            <?php else: ?>
                                -
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tickets</h4>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card px-3">
            <div class="card-body">
                <h4 class="card-title">Price Changelogs</h4>
            </div>
        </div>
    </div>
</div>