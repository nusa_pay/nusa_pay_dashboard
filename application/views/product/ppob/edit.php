<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-4">
                <h4 class="card-title">Edit <?= $product->name ?></h4>

                <form method="post">
                    <div class="form-group">
                        <label for="field-category">Category</label>
                        <select class="form-control" id="field-category" name="category_id">
                            <?php
                                function print_opts_rec($global, $key, $active, $space=0){
                                    $options = $global[$key];
                                    foreach($options as $opt){
                                        echo '<option value="' . $opt->id . '"';
                                        if($opt->id == $active)
                                            echo ' selected="selected"';
                                        echo '>';
                                        echo str_repeat('&#160;', $space);
                                        echo $opt->name;
                                        echo '</option>';
                                        if(isset($options[$opt->id]))
                                            print_opts_rec($global, $opt->id, $active, $space+4);
                                    }
                                }

                                print_opts_rec($categories, 0, $product->category_id);
                            ?>
                        </select>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-base_price">Base Price</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-base_price"
                                    name="base_price"
                                    data-inputmask-prefix="Rp. "
                                    data-inputmask-allowMinus="false"
                                    data-inputmask-digits="0"
                                    data-inputmask="'alias': 'currency'"
                                    value="<?= $product->base_price ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-price">User Price</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-price"
                                    name="price"
                                    data-inputmask-prefix="Rp. "
                                    data-inputmask-allowMinus="false"
                                    data-inputmask-digits="0"
                                    data-inputmask="'alias': 'currency'"
                                    value="<?= $product->price ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-price_agent">Agent Price</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-price_agent"
                                    name="price_agent"
                                    data-inputmask-prefix="Rp. "
                                    data-inputmask-allowMinus="false"
                                    data-inputmask-digits="0"
                                    data-inputmask="'alias': 'currency'"
                                    value="<?= $product->price_agent ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-reseller_price">Reseller Price</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-reseller_price"
                                    name="reseller_price"
                                    data-inputmask-prefix="Rp. "
                                    data-inputmask-allowMinus="false"
                                    data-inputmask-digits="0"
                                    data-inputmask="'alias': 'currency'"
                                    value="<?= $product->reseller_price ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-ppn">PPn</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-ppn"
                                    name="ppn"
                                    data-inputmask-allowMinus="false"
                                    data-inputmask="'alias': 'percentage'"
                                    value="<?= $product->ppn ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-pph">PPh</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="field-pph"
                                    name="pph"
                                    data-inputmask-allowMinus="false"
                                    data-inputmask="'alias': 'percentage'"
                                    value="<?= $product->pph ?>">
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <a href="/product/ppob/item/<?= $product->id ?>" class="btn btn-light">Cancel</a>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>