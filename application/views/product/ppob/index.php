<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">PPOB Products</h4>

                <div class="d-flex table-responsive mb-2">
                    <div class="btn-group mr-2">
                        <a href="#" id="file-picker" class="btn btn-primary">
                            Bulk Update
                            <input type="file" accept="text/csv">
                        </a>
                        <a href="/assets/example/bulk-update-ppob.csv"
                            download="bulk-update-ppob.csv"
                            class="btn btn-light">Download Example</a>
                    </div>
                </div>

                <div id="js-grid"></div>
            </div>
        </div>
    </div>
</div>

<script>
$('#js-grid').jsGrid({
    width: '100%',
    paging: true,
    pageLoading: true,
    filtering: true,
    sorting: true,
    autoload: true,
    pageButtonCount: 8,
    controller: {
        loadData: function(filter){
            var query = {};
            for(var k in filter){
                if(filter[k])
                    query[k] = filter[k];
            }

            return $.ajax({
                type: 'GET',
                url: '/product/ppob/filter',
                data: query
            })
        }
    },
    fields: [
        { name: 'Code',     type: 'text' },
        { name: 'Name',     type: 'text' },
        { name: 'Price',    type: 'number', filtering: false },
        { name: 'Type',     type: 'select',
            items: [
                {Id: 0, Name: 'All'},
                {Id: 1, Name: 'Service'},
                {Id: 2, Name: 'Goods'}
            ],
            valueField: 'Id',
            textField: 'Name',
            itemTemplate: function(value, items) {
                return value.Name;
            }
        },
        { name: 'Category', type: 'select',
            items: <?= json_encode($categories) ?>,
            valueField: 'Id',
            textField: 'Name',
            itemTemplate: function(value, items) {
                return value.Name;
            }
        },
        { name: 'Sold',     type: 'number', filtering: false }
    ],
    rowClick: function(args){
        var next = '/product/ppob/item/' + args.item.Id;
        location.href = next;
    }
})
</script>
<style>
#file-picker{ position: relative; overflow: hidden; }
#file-picker input{
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    cursor: pointer;
    opacity: 0;
}
</style>
<script src="/assets/js/papaparse.min.js"></script>
<script>
    $(function(){
        $('#file-picker input').on('change', function(){
            var file = this.files[0];

            swal({
                title: 'Updating Products',
                text: 'Please wait a moment. Don\'t close the window during update process',
                closeOnClickOutside: false,
                closeOnEsc: false,
                button: false,
                content: $('<div class="circle-loader"></div>').get(0)
            });

            Papa.parse(file, {
                complete: function(result){
                    if(result.errors.length){
                        swal.close();
                        return swal({
                            title: 'Invalid Data',
                            text: result.errors[0].message,
                            icon: 'error'
                        });
                    }

                    var data = result.data;
                    var props = data.shift();

                    if(props.length < 2){
                        swal.close();
                        return swal({
                            title: 'Invalid Data',
                            text: 'There should be at least 2 column',
                            icon: 'error'
                        });
                    }

                    if(!~props.indexOf('code')){
                        swal.close();
                        return swal({
                            title: 'Invalid Data',
                            text: 'Column `code` is required',
                            icon: 'error'
                        });
                    }

                    var knownColumns = [
                        'code',
                        'price',
                        'price_agent',
                        'reseller_price',
                        'profit_fee',
                        'admin_fee',
                        'base_price'
                    ];

                    for(var i=0; i<props.length; i++){
                        var prop = props[i];
                        if(!~knownColumns.indexOf(prop)){
                            swal.close();
                            return swal({
                                title: 'Invalid Data',
                                text: 'Unknown column `'+prop+'`',
                                icon: 'error'
                            });
                        }
                    }

                    var rows = [];
                    for(var i=0; i<data.length; i++){
                        var datum = data[i];
                        if(datum.length < 2)
                            continue;

                        var row = {};
                        for(var j=0; j<props.length; j++){
                            var prop = props[j];
                            row[prop] = datum[j];

                            if(prop != 'code'){
                                if(datum[j].match(/[^0-9]/)){
                                    swal.close();
                                    return swal({
                                        title: 'Invalid Data',
                                        text: 'Column `'+prop+'` of row '+(i+2)+' should be numeric',
                                        icon: 'error'
                                    });
                                }
                            }
                        }

                        rows.push(row);
                    }

                    var rowTotal = rows.length;
                    var rowFinish= 0;
                    var errors = [];

                    for(var i=0; i<rowTotal; i++){
                        var row = rows[i];

                        (function(row,i){
                            $.post('/product/ppob/set', row, function(res){
                                if(res.error)
                                    errors.push([res.message, i+2]);

                                setTimeout(function(){
                                    rowFinish++;
                                    if(rowTotal > rowFinish)
                                        return;
                                    var ul = $('<div></div>');
                                    for(var j=0; j<errors.length; j++)
                                        ul.append('<div>Row '+errors[j][1]+': '+errors[j][0]+'</div>');
                                    swal.close();
                                    swal({
                                        title: 'Success',
                                        text: 'All produts already updated ' + (errors.length?'with some errors':''),
                                        icon: 'success',
                                        content: ul.get(0)
                                    }).then(function(){
                                        location.reload();
                                    });
                                }, 1000);
                            });
                        })(row,i);
                    }
                }
            });
        })
    });
</script>