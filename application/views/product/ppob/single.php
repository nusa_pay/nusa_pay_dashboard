<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
            <div class="card-body">
                <h6 class="font-weight-normal">Purchases</h6>
                <h2 class="mb-0"><?= number_format($purchased) ?></h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
            <div class="card-body">
                <h6 class="font-weight-normal">Price Changes</h6>
                <h2 class="mb-0">0</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-warning text-white text-center card-shadow-warning">
            <div class="card-body">
                <h6 class="font-weight-normal">Tickets</h6>
                <h2 class="mb-0">0</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-info text-white text-center card-shadow-info">
            <div class="card-body">
                <h6 class="font-weight-normal">Current Price</h6>
                <h2 class="mb-0"><?= number_format($product->price) ?></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-0">
                <h4 class="card-title">Details</h4>
                <table class="table">
                    <tr><th>ID</th><td><?= $product->id ?></td></tr>
                    <tr><th>Name</th><td><?= hs(($product->name?$product->name:'N/A')) ?></td></tr>
                    <tr><th>Code</th><td><?= hs($product->code?$product->code:'N/A') ?></td></tr>
                    <tr><th>Category</th><td><?= (is_object($product->category_id)?$product->category_id->name:'N/A') ?></td></tr>
                    <tr><th>Price</th><td>Rp. <?= number_format($product->price?$product->price:0) ?></td></tr>
                    <tr><th>Agent Price</th><td>Rp. <?= number_format($product->price_agent?$product->price_agent:0) ?></td></tr>
                    <tr><th>Reseller Price</th><td>Rp. <?= number_format($product->reseller_price?$product->reseller_price:0) ?></td></tr>
                    <tr><th>Base Price</th><td>Rp. <?= number_format($product->base_price?$product->base_price:0) ?></td></tr>
                    <tr><th>Admin Fee</th><td>Rp. <?= number_format($product->admin_fee?$product->admin_fee:0) ?></td></tr>


                    <tr><th>PPn</th><td><?= number_format($product->ppn?$product->ppn:0) ?> %</td></tr>
                    <tr><th>PPh</th><td><?= number_format($product->pph?$product->pph:0) ?> %</td></tr>

                    <tr><th>Description</th><td><?= hs($product->description?$product->description:'...') ?></td></tr>
                    <tr><th>Created</th><td><?= $product->created_at->format('M d, Y H:i') ?></td></tr>
                </table>

                <div class="text-right pb-4">
                    <a href="/product/ppob/edit/<?= $product->id ?>" class="btn btn-light">Edit</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-7 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Last Purchase</h4>
                <table class="table">
                    <thead><tr><th>Order</th><th>User</th><th class="text-right">Total</th></tr></thead>
                    <tbody>
                        <?php if($purchases): ?>
                            <?php foreach($purchases as $purchase): ?>
                                <tr>
                                    <td><?= $purchase->order_id ?></td>
                                    <td><?= ( is_object($purchase->user_id) ? $purchase->user_id->username : 'N/A' ) ?></td>
                                    <td class="text-right">Rp. <?= number_format($purchase->total) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card px-3">
            <div class="card-body">
                <h4 class="card-title">Price Changelogs</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card px-3">
            <div class="card-body">
                <h4 class="card-title">Tickets</h4>
            </div>
        </div>
    </div>
</div>