<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <form class="card-body" method="POST">
                <h4 class="card-title"><?= $title ?></h4>

                <div class="row">
                    <div class="col-12">
                        <?= $this->form->field('title') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <?= $this->form->field('image') ?>
                        <?= $this->form->field('cover') ?>
                    </div>
                    <div class="col-8">
                        <?= $this->form->field('highlight') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <?= $this->form->field('web') ?>
                    </div>
                    <div class="col-3">
                        <?= $this->form->field('phone') ?>
                    </div>
                    <div class="col-3">
                        <?= $this->form->field('email') ?>
                    </div>
                    <div class="col-3">
                        <?= $this->form->field('status', [
                            1 => 'Hidden',
                            2 => 'Active'
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?= $this->form->field('terms') ?>
                    </div>
                </div>

                <div class="d-flex table-responsive mb-2">
                    <button class="btn btn-primary mr-2">Save</button>
                    <a href="/promotion/banner" class="btn btn-light">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
tinymce.init({
  selector: '#field-terms',
  height: 500,
  theme: 'modern',
  menubar: false,
  plugins: [
    'lists link image'
  ],
  toolbar1: 'bold italic | bullist numlist | link'
});
</script>

<script>
    $('.file-uploader').on('change', function(){
        var file = this.files[0];
        var $inp = $('#'+$(this).data('target'));

        swal({
            title: 'Uploading',
            button: false,
            content: $('<div class="circle-loader"></div>').get(0),
            closeOnClickOutside: false,
            closeOnEsc: false
        });

        var formData = new FormData(),
            xhr      = new XMLHttpRequest();

        formData.append('file', file, file.name);
        // formData.append('disk', 'local');

        console.log(file);

        xhr.open('POST', '<?= $upload_url ?>', true);
        xhr.setRequestHeader('Authorization', '<?= $atoken ?>');

        xhr.onreadystatechange = function(){
            if(xhr.readyState != 4)
                return;

            swal.close();

            if(xhr.status != 200)
                return swal({title: 'Error', text: 'Unable to upload the file'});

            var res;
            try{
                res = JSON.parse(xhr.responseText);
            }catch(e){
            }

            if(!res)
                return swal({title: 'Error', text: 'Server response is invalid json'});

            if(!res.success)
                return swal({title: 'Error', text: res.message});

            $inp.val(res.data.url);
        }

        xhr.send(formData);
    });
</script>