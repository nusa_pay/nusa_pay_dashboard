 <div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Banners</h4>

                <div class="d-flex table-responsive mb-2">
                    <a href="<?=base_url() ?>/promotion/banner/0" class="btn btn-primary">Add New</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php if($banners): ?>
        <?php foreach($banners as $banner): ?>
            <div class="col-md-3 grid-margin grid-margin-md-0">
                <div class="card">
                    <div class="card-body">
                        <div class="wrapper">
                            <div class="rounded" style="height:100px;background: transparent url(<?= $banner->image ?>) center center no-repeat;background-size: cover;">
                            </div>
                            <div class="wrapper mt-3">
                                <p class="font-weight-medium" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                                    <?= $banner->title ?>
                                </p>
                                <p class="text-muted" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $banner->email ?></p>
                                <p class="text-info mb-0 font-weight-medium"><?= $banner->created ?></p>

                                <div class="btn btn-group">
                                    <a class="btn btn-primary" href="/promotion/banner/<?= $banner->id ?>">Edit</a>
                                    <a class="btn btn-danger btn-removal" href="/promotion/banner/<?= $banner->id ?>/remove">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

<script>
$('.btn-removal').click(function(){
    var $this = $(this);
    swal({
        icon: 'warning',
        title: 'Removal Confirmation',
        text: 'Are you sure want to remove this banner image? This action can\'t be undone',
        buttons: {
            cancel: true,
            confirm: true
        }
    }).then(function(res){
        if(res)
            location.href = $this.attr('href');
    });
    return false;
});
</script>