<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <form class="card-body" method="POST">
                <h4 class="card-title"><?= $title ?></h4>

                <div class="row">
                    <div class="col-12">
                        <?= $this->form->field('promotion_name') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <?= $this->form->field('max_claim_amount_per_user') ?>
                        <?= $this->form->field('max_claim_amount_per_order') ?>
                        <?= $this->form->field('budget') ?>
                        <?= $this->form->field('max_claim_per_day') ?>
                    </div>
                    <div class="col-3">
                        <?= $this->form->field('minimum_transaction_amount') ?>
                        <?= $this->form->field('promotion_value') ?>
                        <?= $this->form->field('max_claim_per_user') ?>
                    </div>
                    <div class="col-3">
                        <?= $this->form->field('date_from') ?>
                        <?= $this->form->field('date_to') ?>
                        <?= $this->form->field('claim_from') ?>
                        <?= $this->form->field('claim_to') ?>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>Product</label>
                            <div class="panel panel-default">
                                <div class="panel-body" style="max-height:310px;overflow: auto;">
                                    <?php if($products): ?>
                                        <?php foreach($products as $prod): ?>
                                            <div>
                                                <input type="checkbox" value="<?= $prod->id ?>" name="product[]"<?= (in_array($prod->id, $cashback->product)?' checked':'') ?>> <?= $prod->name ?>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex table-responsive mb-2">
                    <button class="btn btn-primary mr-2">Save</button>
                    <a href="/promotion/cashback<?= ( isset($cashback->id) ? '/item/' . $cashback->id : '' ) ?>" class="btn btn-light">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>