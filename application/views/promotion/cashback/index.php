<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Promotion Cashback</h4>

                <div class="d-flex table-responsive mb-2">
                    <a href="<?= base_url() ?>promotion/cashback/edit/0" class="btn btn-primary">Add New</a>
                </div>

                <div id="js-grid"></div>
            </div>
        </div>
    </div>
</div>

<script>
$('#js-grid').jsGrid({
    width: '100%',
    paging: true,
    pageLoading: true,
    filtering: true,
    sorting: true,
    autoload: true,
    pageButtonCount: 8,
    controller: {
        loadData: function(filter){
            var query = {};
            for(var k in filter){
                if(filter[k])
                    query[k] = filter[k];
            }

            return $.ajax({
                type: 'GET',
                url: '/promotion/cashback/filter',
                data: query
            })
        }
    },
    fields: [
        { name: 'Name',     type: 'text' },
        { name: 'Budget',   type: 'number', filtering: false },
        { name: 'Status',   type: 'select',
            items: [
                {Id: 0, Name: 'All'},
                {Id: -1, Name: 'New'},
                {Id: 1, Name: 'Active'},
                {Id: 6, Name: 'Terminated'}
            ],
            valueField: 'Id',
            textField: 'Name',
            itemTemplate: function(value, items) {
                return value.Name;
            }
        },
        { name: 'DateFrom', type: 'text' },
        { name: 'DateTo',   type: 'text' }
    ],
    rowClick: function(args){
        var next = '/promotion/cashback/item/' + args.item.Id;
        location.href = next;
    }
})
</script>