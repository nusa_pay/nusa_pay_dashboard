<?php 
    $statuses = [
        'New',
        'Active',
        'Need Revision',
        'In Review',
        'New Revision',
        'Termination Process',
        'Terminated'
    ];
?>
<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
            <div class="card-body">
                <h6 class="font-weight-normal">Budget</h6>
                <h2 class="mb-0"><?= number_format($cashback->budget )?></h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
            <div class="card-body">
                <h6 class="font-weight-normal">Budget Available</h6>
                <h2 class="mb-0"><?= number_format($cashback->budget_available) ?></h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-warning text-white text-center card-shadow-warning">
            <div class="card-body">
                <h6 class="font-weight-normal">Transactions</h6>
                <h2 class="mb-0"><?= number_format($used) ?></h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-info text-white text-center card-shadow-info">
            <div class="card-body">
                <h6 class="font-weight-normal">Status</h6>
                <h2 class="mb-0">
                    <?= $statuses[$cashback->status] ?>
                </h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-0">
                <h4 class="card-title">Details</h4>
                <table class="table">
                    <tr><th>ID</th><td><?= $cashback->id ?></td></tr>
                    <tr><th>Min Trans Amount</th><td><?= number_format($cashback->minimum_transaction_amount) ?></td></tr>
                    <tr><th>Promotion Value</th><td><?= $cashback->promotion_value ?></td></tr>
                    <tr><th>Max Claim Amount Per Order</th><td><?= number_format($cashback->max_claim_amount_per_order) ?></td></tr>
                    <tr><th>Max Claim Amount Per User</th><td><?= number_format($cashback->max_claim_amount_per_user) ?></td></tr>
                    <tr><th>Max Claim Per User</th><td><?= number_format($cashback->max_claim_per_user) ?></td></tr>
                    <tr><th>Max Claim Per Day</th><td><?= number_format($cashback->max_claim_per_day) ?></td></tr>
                    <tr><th>Date From</th><td><?= $cashback->date_from ?></td></tr>
                    <tr><th>Date To</th><td><?= $cashback->date_to ?></td></tr>
                    <tr><th>Claim From</th><td><?= $cashback->claim_from ?></td></tr>
                    <tr><th>Claim To</th><td><?= $cashback->claim_to ?></td></tr>
                </table>

                <div class="text-right pb-4">
                    <div class="btn btn-group">
                        <a href="/promotion/cashback/edit/<?= $cashback->id ?>" class="btn btn-light">Edit</a>
                        <?php if(in_array($cashback->status, [0,1])): ?>
                            <a class="btn btn-danger btn-removal" href="/promotion/cashback/terminate/<?= $cashback->id ?>">
                                Terminate
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-7 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Last Transaction</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Reff</th>
                            <th>User</th>
                            <th class="text-right">Trans Amount</th>
                            <th class="text-right">Percent</th>
                            <th class="text-right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($transactions as $tran): ?>
                            <tr>
                                <td><?= $tran->transaction_ref ?></td>
                                <td><?= $tran->redeemed_by->phone ?></td>
                                <td class="text-right"><?= number_format($tran->transaction_amount) ?></td>
                                <td class="text-right"><?= $tran->percentage ?>%</td>
                                <td class="text-right"><?= number_format($tran->amount) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card px-3">
            <div class="card-body">
                <h4 class="card-title">Tickets</h4>
            </div>
        </div>
    </div>
</div>

<script>
$('.btn-removal').click(function(){
    var $this = $(this);
    swal({
        icon: 'warning',
        title: 'Removal Confirmation',
        text: 'Are you sure want to terminate this promotion?',
        buttons: {
            cancel: true,
            confirm: true
        }
    }).then(function(res){
        if(res)
            location.href = $this.attr('href');
    });
    return false;
});
</script>