<?php $this->load->view("devextreme");?>
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div class="d-flex table-responsive">
                  <div id="dateBoxFrom"></div>
                  <div id="dateBoxTo"></div>
                   <button class="btn btn-sm btn-primary" id="btnSearchReport" onClick="searchReport()">
                            <i class="mdi mdi-file-find"></i> Search
                        </button>
                  </div>
                  <hr>
                  <div id="grid-users"></div>
                </div>
              </div>
            </div>
</div>
<script>
   function searchReport()
  {
     $("#grid-users").dxDataGrid("instance").refresh();
  }

  (function($) {
    var now = new Date();
    $("#dateBoxFrom").dxDateBox({
  displayFormat: "yyyy-MM-dd",
  type:"date",
  value: now
  //pickerType: "rollers"
  });

  $("#dateBoxTo").dxDateBox({
  displayFormat: "yyyy-MM-dd",
  type:"date",
  value: now
  //pickerType: "rollers"
  });


    
    var dataSource = {
    load: function() {
        var deferred = $.Deferred();
        var from = $("#dateBoxFrom input:hidden").val();
        var to = $("#dateBoxTo input:hidden").val();
        var this_url = "<?php echo base_url() ?>reporting/ltkt/searchData";

    
            $.ajax({
                url: this_url,
                dataType: "json",
                type:"POST",
                data: {from: from, to:to},
                success: function(data) {
                    deferred.resolve(data);
                },
                error: function() {
                    deferred.reject("Data Loading Error");
                },
                timeout: 5000
            });
    
            return deferred.promise();
    }
  };

   $("#grid-users").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "ReportingLtkt"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "User Name",
                                    dataField: "users_name",
                                },
                                {
                                    caption: "Total Amount",
                                    dataField: "total_amount",
                                     dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "Total Fee",
                                    dataField: "total_fee",
                                     dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                {
                                    caption: "Grand total",
                                    dataField: "grand_total",
                                     dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                 

                                 
                            ],
                             
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 

  })(jQuery);
</script>