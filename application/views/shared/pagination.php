<ul class="pagination pagination-primary mb-0">
    <?php foreach($pagination as $label => $link): ?>
    <li class="page-item<?= ($link=='#'&&is_numeric($label)?' active':'') ?>">
        <a class="page-link" href="<?= $link ?>"><?= $label ?></a>
    </li>
    <?php endforeach; ?>
</ul>