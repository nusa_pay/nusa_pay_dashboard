<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Create New Ticket</h4>
                <form method="POST">

                    <div class="form-group">
                        <label for="field-subject">Subject</label>
                        <input type="text" class="form-control" id="field-subject" placeholder="Subject" required name="subject">
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-assign-to">Assign to</label>
                                <select id="field-assigned_to" class="select2" style="width:100%" name="assigned_to">
                                    <?php foreach($users as $user): ?>
                                        <option value="<?= $user->id ?>">
                                            <?= $user->first_name ?>
                                            <?= $user->last_name ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-category-parent">Category</label>
                                <select id="field-category-parent" class="select2" style="width:100%">
                                    <?php foreach($categories[0] as $item): ?>
                                        <option value="<?= $item->id ?>">
                                            <?= $item->owner ?>:<?= $item->name ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-category">Topic</label>
                                <select id="field-category" data-placeholder="Select a topic" class="select2" style="width:100%" name="category"></select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-scope">Scope</label>
                                <?php 
                                $scopex = [
                                    [1],
                                    [1,2],
                                    [1,2,3],
                                    [1,2,3,4]
                                ];
                                ?>
                                <select id="field-scope" class="select2" style="width:100%" name="scope">
                                    <?php foreach($scopex as $vals): ?>
                                        <?php
                                            $val = [];
                                            $labels = [];
                                            foreach($vals as $v){
                                                if(!isset($scopes[$v]))
                                                    continue;
                                                $val[] = $v;
                                                $labels[] = $scopes[$v];
                                            }
                                        ?>
                                        <option value="<?= implode(',', $val) ?>">
                                            <?= implode(', ', $labels) ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-priority">Priority</label>
                                <select id="field-priority" class="select2" style="width:100%" name="priority">
                                    <?php foreach($priorities as $priority): ?>
                                        <option value="<?= $priority->id ?>"><?= $priority->label ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Body</label>
                                <textarea name="body" id="body" placeholder="Body"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="attachment" id="ticket-attachement">
                            <button class="btn btn-default" type="button" id="btn-ticket-attachement">
                                Attach
                            </button>
                            <button class="btn btn-default" type="button" id="btn-ticket-file" title="Click to remove"></button>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn btn-light" href="/ticket">Cancel</a>
                            <button class="btn btn-primary mr-2">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.CodeMirror, .CodeMirror-scroll {
    min-height: 200px;
}
</style>
<script>
    var url_upload = "<?php echo base_url() ?>upload/";
</script>
<script src="<?php echo base_url() ?>assets/js/upload.js"></script>
<script>
$(function(){
    var categories = <?= json_encode($categories); ?>;
    $(".select2").select2();

    new SimpleMDE({
        element: $('#body').get(0),
        spellChecker: false,
        status: false,
        toolbar: false,
    });

    $('#btn-ticket-file').click(function(){
        $('#ticket-attachement').val('');
        $(this).hide();
        $('#btn-ticket-attachement').show();
    }).hide();
    $('#btn-ticket-attachement').click(function(){
        $this = $(this);
        uploadMediaFile($this, 'Attach', 'Loading...', function(res){
            $this.hide();
            if(res.media_file){
                $('#btn-ticket-file').text(res.original_name).show();
                $('#ticket-attachement').val(res.media_file)
            }
        })
    })

    $('#field-category-parent').change(function(){
        var data = [];
        for(var i=0; i<categories[this.value].length; i++)
            data.push({id: categories[this.value][i].id, text: categories[this.value][i].name});

        $('#field-category').html('');
        $('#field-category').select2({data: data})
    });
})
</script>