<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-primary text-white text-center card-shadow-primary">
            <div class="card-body">
                <h6 class="font-weight-normal">TOTAL</h6>
                <h2 class="mb-0"><?= number_format($resume->total,0,'.','.') ?></h2>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-danger text-white text-center card-shadow-danger">
            <div class="card-body">
                <h6 class="font-weight-normal">ACTIVE</h6>
                <h2 class="mb-0"><?= number_format($resume->active,0,'.','.') ?></h2>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-warning text-white text-center card-shadow-warning">
            <div class="card-body">
                <h6 class="font-weight-normal">RESOLVED</h6>
                <h2 class="mb-0"><?= number_format($resume->resolved,0,'.','.') ?></h2>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card bg-gradient-info text-white text-center card-shadow-info">
            <div class="card-body">
                <h6 class="font-weight-normal">REJECTED</h6>
                <h2 class="mb-0"><?= number_format($resume->rejected,0,'.','.') ?></h2>
            </div>
        </div>
    </div>
</div>

<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tickets</h4>
                
                <div class="d-flex table-responsive">
                    <form class="btn-group mr-2" action="<?= base_url() ?>ticket/ticket/create">
                        <button class="btn btn-sm btn-primary">
                            <i class="mdi mdi-plus-circle-outline"></i> Add
                        </button>
                    </form>

                    <div class="btn-group mr-2">
                        <?php
                            $btns = [
                                1 => 'Rejected',
                                2 => 'Active',
                                3 => 'Resolved'
                            ];
                            $cond = [];
                            if($q)
                                $cond['q'] = $q;
                            if($priority)
                                $cond['priority'] = $priority;
                        ?>
                        <?php foreach($btns as $idx => $label): ?>
                            <?php
                                $cond['status'] = $idx;
                            ?>
                            <a href="?<?= http_build_query($cond) ?>" class="btn btn-light<?= ($idx==$status?' active':'') ?>">
                                <?= $label ?>
                            </a>
                        <?php endforeach; ?>
                    </div>

                    <div class="btn-group mr-2">
                        <?php
                            $btns = [
                                1 => 'Low',
                                2 => 'Medium',
                                3 => 'High',
                                4 => 'Super'
                            ];
                            $cond = [];
                            if($q)
                                $cond['q'] = $q;
                            if($status)
                                $cond['status'] = $status;
                        ?>
                        <?php foreach($btns as $idx => $label): ?>
                            <?php
                                $cond['priority'] = $idx;
                            ?>
                            <a href="?<?= http_build_query($cond) ?>" class="btn btn-light<?= ($idx==$priority?' active':'') ?>">
                                <?= $label ?>
                            </a>
                        <?php endforeach; ?>
                    </div>

                    <form class="btn-group ml-auto mr-2 border-0 d-none d-md-block">
                        <input type="search" class="form-control" placeholder="Search Here" name="q" value="<?= hs($q) ?>">
                        <?php if($status): ?>
                            <input type="hidden" value="<?= $status ?>" name="status">
                        <?php endif; ?>
                        <?php if($priority): ?>
                            <input type="hidden" value="<?= $priority ?>" name="priority">
                        <?php endif; ?>
                    </form>
                </div>
                
                <div class="table-responsive mt-2">
                    <table class="table mt-3 border-top">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Subject</th>
                                <th>Meta</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($tickets): ?>
                                <?php foreach($tickets as $ticket): ?>
                                    <tr>
                                        <td>
                                            <a href="/ticket/item/<?= $ticket->id ?>">
                                                #<?= $ticket->id ?>
                                            </a>
                                        </td>
                                        <td><?= $ticket->subject->safe() ?></td>
                                        <td style="line-height:20px">
                                            <?= $ticket->priority->label ?><br>
                                            <?php if(is_object($ticket->category->parent)): ?>
                                                <?= $ticket->category->parent->name ?><br>
                                            <?php endif; ?>
                                            <?= $ticket->category->owner ?><br>
                                            <?= $ticket->category->name ?><br>
                                            <?= $ticket->created->format('M d, Y H:i') ?>
                                        </td>
                                        <td>
                                            <?php 
                                            $by_status = ['null','danger','info','success'];
                                            ?>
                                            <div class="badge badge-<?= ($by_status[$ticket->status->id]) ?> badge-fw">
                                                <?= $ticket->status->label ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <?php if($pagination): ?>
                <div class="d-flex align-items-center justify-content-between flex-column flex-sm-row mt-4">
                    <p class="mb-3 mb-sm-0"></p>
                    <nav>
                        <?php $this->load->view("shared/pagination") ?>
                    </nav>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>