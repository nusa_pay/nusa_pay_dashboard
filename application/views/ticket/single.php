<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?= $ticket->subject->safe() ?></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="pt-4">
                            <p class="clearfix">
                                <span class="float-left">
                                    ID
                                </span>
                                <span class="float-right text-muted">
                                    #<?= $ticket->id ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Issued by
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->issuer->first_name ?>
                                    <?= $ticket->issuer->last_name ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Status
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->status->label ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Assigned to
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->assigned_to->first_name ?>
                                    <?= $ticket->assigned_to->last_name ?>
                                </span>
                            </p>

                            <?php if($ticket->rejected_by): ?>
                            <p class="clearfix">
                                <span class="float-left">
                                    Rejected by
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->rejected_by->first_name ?>
                                    <?= $ticket->rejected_by->last_name ?>
                                </span>
                            </p>
                            <?php endif; ?>

                            <?php if($ticket->resolved_by): ?>
                            <p class="clearfix">
                                <span class="float-left">
                                    Resolved by
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->resolved_by->first_name ?>
                                    <?= $ticket->resolved_by->last_name ?>
                                </span>
                            </p>
                            <?php endif; ?>

                            <p class="clearfix">
                                <span class="float-left">
                                    Category
                                </span>
                                <span class="float-right text-muted">
                                    <?php if($ticket->category->parent): ?>
                                        <?= $ticket->category->parent->name ?>
                                    <?php else: ?>
                                        -
                                    <?php endif; ?>
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Owner
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->category->owner ?>
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Topic
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->category->name ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Priority
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->priority->label ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Solving time
                                </span>
                                <span class="float-right text-muted">
                                    <?php if($ticket->solving_time->value): ?>
                                        <?= $ticket->solving_time->format('M d, Y H:i') ?>
                                    <?php else: ?>
                                        -
                                    <?php endif; ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Time to solve
                                </span>
                                <span class="float-right text-muted">
                                    <?php if($ticket->time_to_solve->time): ?>
                                        <?= $ticket->time_to_solve->format('M d, Y H:i') ?>
                                    <?php else: ?>
                                        -
                                    <?php endif; ?>
                                </span>
                            </p>

                            <p class="clearfix">
                                <span class="float-left">
                                    Created
                                </span>
                                <span class="float-right text-muted">
                                    <?= $ticket->created->format('M d, Y H:i') ?>
                                </span>
                            </p>

                        </div>

                        <?php if($ticket->scope): ?>
                        <div class="border-bottom mb-3 pb-3">
                            <p>Scopes</p>
                            <div>
                                <?php foreach($ticket->scope as $scope): ?>
                                <label class="badge badge-outline-dark"><?= $scope->label ?></label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="d-flex justify-content-between">
                            <?php if($ticket->status->id != 2): ?>
                                <form method="POST" action="/ticket/item/<?= $ticket->id ?>/activate">
                                    <button class="btn btn-default btn-block">Reactivate</button>
                                </form>
                            <?php else: ?>
                                <form method="POST" action="/ticket/item/<?= $ticket->id ?>/reject">
                                    <button class="btn btn-danger">Reject</button>
                                </form>
                                <form method="POST" action="/ticket/item/<?= $ticket->id ?>/resolve">
                                    <button class="btn btn-success">Resolve</button>
                                </form>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="col-lg-8">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="ticket-tab" data-toggle="tab" href="#tab-ticket" role="tab" aria-controls="tab-ticket" aria-selected="true">Ticket</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="guidline-tab" data-toggle="tab" href="#tab-guidline" role="tab" aria-controls="tab-guidline" aria-selected="false">Guidline</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tab-ticket" role="tabpanel" aria-labelledby="ticket-tab">
                                <div class="profile-feed">
                                    <div class="d-flex align-items-start profile-feed-item">
                                        <div>
                                            <h6>
                                                <?= $ticket->issuer->first_name ?>
                                                <?= $ticket->issuer->last_name ?>
                                                <small class="ml-4 text-muted">
                                                    <i class="mdi mdi-clock mr-1"></i><?= $ticket->created->format('M d, Y H:i') ?>
                                                </small>
                                            </h6>
                                            <div class="card-description">
                                                <?= $ticket->body ?>
                                                <?php if($ticket->attachment): ?>
                                                    <a href="<?= $ticket->attachment ?>" target="_blank">
                                                        <i class="mdi mdi-attachment"></i>
                                                        Attachement
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php if($ticket->status->id == 2): ?>
                                    <form class="profile-feed-item loading" id="comment-form" method="POST" action="/ticket/item/<?= $ticket->id ?>/comment">
                                        <div class="mb-3">
                                            <textarea name="content" id="comment" placeholder="Add comment"></textarea>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <input type="hidden" name="attachment" id="comment-attachement">
                                            <button class="btn btn-default" type="button" id="btn-comment-attachement">
                                                Attach
                                            </button>
                                            <button class="btn btn-default" type="button" id="btn-comment-file" title="Click to remove"></button>
                                            <button class="btn btn-success">Send</button>
                                        </div>
                                    </form>

                                    <?php else: ?>
                                    <div class="profile-feed-item">
                                        <div class="card-description">
                                            Comment disabled
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if($comments): ?>
                                        <?php foreach($comments as $cmn): ?>
                                            <div class="d-flex align-items-start profile-feed-item">
                                                <div>
                                                    <h6>
                                                        <?= $cmn->user->first_name ?>
                                                        <?= $cmn->user->last_name ?>
                                                        <small class="ml-4 text-muted">
                                                            <i class="mdi mdi-clock mr-1"></i><?= $cmn->created->format('M d, Y H:i') ?>
                                                        </small>
                                                    </h6>
                                                    <div class="card-description">
                                                        <?= $cmn->content ?>
                                                        <?php if($cmn->attachment): ?>
                                                            <a href="<?= $cmn->attachment ?>" target="_blank">
                                                                <i class="mdi mdi-attachment"></i>
                                                                Attachement
                                                            </a>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="tab-pane fade show" id="tab-guidline" role="tabpanel" aria-labelledby="guidline-tab">
                                <?php if($guidelines): ?>
                                    <?php foreach($guidelines as $gl): ?>
                                        <div class="mb-4 border-bottom pb-4">
                                            <h3><?= $gl->subject->safe() ?></h3>
                                            <div><?= $gl->content ?></div>
                                        </div>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    No guidline found.
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($ticket->status->id == 2): ?>
    <style>
    </style>
    <script src="/assets/js/upload.js"></script>
    <script>
        new SimpleMDE({
          element: $('#comment').get(0),
          spellChecker: false,
          status: false,
          toolbar: false,
        });
    </script>
    <script>
        $('#btn-comment-file').click(function(){
            $('#comment-attachement').val('');
            $(this).hide();
            $('#btn-comment-attachement').show();
        }).hide();
        $('#btn-comment-attachement').click(function(){
            $this = $(this);
            uploadMediaFile($this, 'Attach', 'Loading...', function(res){
                $this.hide();
                if(res.media_file){
                    $('#btn-comment-file').text(res.original_name).show();
                    $('#comment-attachement').val(res.media_file)
                }
            })
        })
    </script>
<?php endif; ?>