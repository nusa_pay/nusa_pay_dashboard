
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  
                  <form class="cmxform" action="<?= base_url() ?>users/save/<?= $id ?>" id="UsersFormAdd" method="post">
                  <div class="form-group" style="display: none;">
                  		<label for="">User Name</label>
                  		<input type="text" class="form-control" value="<?php echo $row['id'] ?>" name="id" placeholder="Input field">
                  	</div>
                  	<div class="form-group">
                  		<label for="">User Name</label>
                  		<input type="text" class="form-control" value="<?php echo $row['username'] ?>" name="username" required id="username" placeholder="Input field">
                  	</div>

                  	<div class="form-group">
                  		<label for="">Group</label>
                  		<select required name="group_id" class="form-control" value="<?= $row['group_id'] ?>">
                  			<option value=""></option>
                  			<?php foreach ($listGroups as $value): ?>
                  				<option <?php if ($row['group_id'] == $value->group_id ): ?>
                  					selected
                  				<?php endif ?> value="<?= $value->group_id ?>"><?= $value->name ?></option>
                  			<?php endforeach ?>
                  		</select>
                  	</div>

                  	<div class="form-group">
                  		<label for="">First Name</label>
                  		<input type="text" class="form-control" value="<?= $row['first_name'] ?>" name="first_name" id="firstname" placeholder="Input field">
                  	</div>

                  	<div class="form-group">
                  		<label for="">Last Name</label>
                  		<input type="text" class="form-control" value="<?= $row['last_name'] ?>" name="last_name" id="lastname" placeholder="Input field">
                  	</div>

                  	<div class="form-group">
                  		<label for="">Password</label>
                  		<input type="password" class="form-control" name="password" id="password" placeholder="Input field" <?php if ($row['id'] == ''): ?>
                  			required
                  		<?php endif ?> >
                  	</div>

                  	<div class="form-group">
                  		<label for="">Confirm Password</label>
                  		<input type="password" class="form-control" id="confirm_password" placeholder="Input field" <?php if ($row['id'] == ''): ?>
                  			required
                  		<?php endif ?> >
                  	</div>
                  
                  	
                  
                  <input class="btn btn-primary" type="submit" value="Submit">
                  </form>
              </div>
            </div>
</div>
<script type="text/javascript">
	(function($) {
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

  })(jQuery);
</script>