<?php $this->load->view("devextreme");?>
<div class="row grid-margin">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?=$cardTitle;?></h4>
                  <p class="card-description"><?= $cardDesc; ?></p>
                  <div class="d-flex table-responsive">
                  <!-- <div id="dateBoxFrom"></div>
                  <div id="dateBoxTo"></div>
                   <button class="btn btn-sm btn-primary" id="btnSearchReport" onClick="searchReport()">
                            <i class="mdi mdi-file-find"></i> Search
                        </button> -->
                  </div>
                  <hr>
                  <div id="grid-users"></div>
                </div>
              </div>
            </div>
</div>
<script>
  

  (function($) {
   
    
    var dataSource = {
    load: function() {
        var deferred = $.Deferred();
        var this_url = "<?php echo base_url() ?>wallet/voucher/searchData";

    
            $.ajax({
                url: this_url,
                dataType: "json",
               
                success: function(data) {
                    deferred.resolve(data);
                },
                error: function() {
                    deferred.reject("Data Loading Error");
                },
                timeout: 5000
            });
    
            return deferred.promise();
    }
  };

   $("#grid-users").dxDataGrid({
                    dataSource: dataSource,
                    showBorders: true,
                    filterRow: {
                    visible: true,
                    applyFilter: "auto"
                    },
                     headerFilter: {
                        visible: true
                    },
                      "export": {
                      enabled: true,
                      fileName: "VoucherRedeemCreate"
                  },

                            paging: {
                                pageSize: 10
                            },
                            pager: {
                                showPageSizeSelector: true,
                                allowedPageSizes: [5, 10, 20],
                                showInfo: true
                            },

                            columns: [
                                
                                {
                                    caption: "User Name",
                                    dataField: "user_name",
                                },
                                {
                                    caption: " Amount",
                                    dataField: "amount",
                                     dataType:"number",
                                    format: "Rp #,##0.##"
                                },
                                 {
                                    caption: "DATE CREATED",
                                    dataField: "created",
                                    dataType: "date",
                                      format: 'dd-MMMM-yyyy hh:mm:ss',
                                },
                                 {
                                    caption: "DATE Expired",
                                    dataField: "expires",
                                    dataType: "date",
                                      format: 'dd-MMMM-yyyy hh:mm:ss',
                                },
                                 {
                                    caption: "DATE CREATED",
                                    dataField: "redeemed",
                                    dataType: "date",
                                      format: 'dd-MMMM-yyyy hh:mm:ss',
                                },
                                {
                                    caption: "Redeemer Name",
                                    dataField: "redemeer_name",
                                    
                                },
                                {
                                    caption: "Status Redeem",
                                    dataField: "status_redeem",
                                    
                                },
                                 

                                 
                            ],
                             
                              onToolbarPreparing: function(e) {
                                var dataGrid = e.component;

                                e.toolbarOptions.items.unshift({
                                    location: "after",
                                    widget: "dxButton",
                                    options: {
                                        icon: "refresh",
                                        onClick: function() {
                                            dataGrid.clearFilter();
                                            dataGrid.refresh();
                                        }
                                    }
                                });
                            }
                        });

 

  })(jQuery);
</script>