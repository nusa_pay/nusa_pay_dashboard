function uploadMediaFile(btn, ntext, ltext, callback){
    var input = $('<input type="file">');
    input.on('change', function(){
        var file = this.files[0];
        var formData = new FormData(),
            xhr = new XMLHttpRequest();
        formData.append('name', file.name);
        formData.append('file', file, file.name);
        xhr.open('POST', '/upload', true);

        xhr.onreadystatechange = function(){
            if(xhr.readyState == 4){
                if(xhr.status == 200){
                    var res = JSON.parse(xhr.responseText);
                    callback(res);
                    $(btn).text(ntext);
                }
            }
        }

        $(btn).text(ltext);
        xhr.send(formData);
    })

    setTimeout(function(){
        input.click();
    });
}