CREATE TABLE IF NOT EXISTS `ticket` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `issuer` INTEGER NOT NULL,
    `subject` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
    `body` TEXT COLLATE utf8_unicode_ci NOT NULL,
    `attachment` VARCHAR(200),
    `assigned_to` INTEGER,
    `category` INTEGER,
    `scope` VARCHAR(10) NOT NULL,
    `priority` INTEGER,
    -- 0 Deleted
    -- 1 Rejected
    -- 2 Active
    -- 3 Resolved
    `status` TINYINT NOT NULL DEFAULT 2,
    `solving_time` DATETIME,
    `time_to_solve` DATETIME,

    `rejected_by` INTEGER,
    `resolved_by` INTEGER,

    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ticket_category` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `owner` ENUM('user','merchant','organization'),
    `name` TEXT COLLATE utf8_unicode_ci NOT NULL,
    `parent` INTEGER,
    `time_to_solve` INTEGER,
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `ticket_category` ( `id`, `owner`, `name`, `parent`, `time_to_solve` ) VALUES
    ( 1,  'user', 'Registration', NULL, 3600 ),
        ( 22,  'user', 'General', 1, 3600 ),
        ( 23,  'user', 'OTP Never Arrive', 1, 3600 ),
    ( 2,  'user', 'Login', NULL, 3600 ),
        ( 24,  'user', 'General', 2, 3000 ),
        ( 25,  'user', 'OTP Never Arrive', 2, 4000 ),
    ( 3,  'user', 'Purchase', NULL, 3600 ),
        ( 26,  'user', 'General', 3, 3600 ),
        ( 27,  'user', 'The Credit Never Arrive', 3, 4500 ),
    ( 4,  'user', 'Top Up', NULL, 3600 ),
        ( 28,  'user', 'General', 4, 3600 ),
        ( 29,  'user', 'Already Transfer But Never Success', 4, 4500 ),
    ( 5,  'user', 'Transfer', NULL, 3600 ),
        ( 30,  'user', 'General', 5, 3600 ),
        ( 31,  'user', 'Not Yet Received', 5, 5000 ),
    ( 6,  'user', 'Bonus', NULL, 3600 ),
        ( 32,  'user', 'General', 6, 3600 ),
        ( 33,  'user', 'Never Get On Registration', 6, 4100 ),
    ( 7,  'user', 'nVoucher', NULL, 3600 ),
        ( 34,  'user', 'General', 7, 3600 ),
        ( 35,  'user', 'Unable To Redeem', 7, 4100 ),
    ( 8,  'merchant', 'Registration', NULL, 3600 ),
        ( 36,  'merchant', 'General', 8, 3600 ),
        ( 37,  'merchant', 'Cant Register', 8, 4100 ),
    ( 9,  'merchant', 'Login', NULL, 3600 ),
        ( 38,  'merchant', 'General', 9, 3600 ),
        ( 39,  'merchant', 'Cant Login', 9, 4500 ),
    ( 10, 'merchant', 'Product Listing', NULL, 3600 ),
        ( 40,  'merchant', 'General', 10, 3600 ),
        ( 41,  'merchant', 'The Product Not Shown', 10, 4500 ),
    ( 11, 'merchant', 'Promotion', NULL, 3600 ),
        ( 42,  'merchant', 'General', 11, 3600 ),
        ( 43,  'merchant', 'The Promotion Cant Used', 11, 4500 ),
    ( 12, 'merchant', 'Transfer', NULL, 3600 ),
        ( 44,  'merchant', 'General', 12, 3600 ),
        ( 45,  'merchant', 'Unable To Transfer To Some User', 12, 5000 ),
    ( 13, 'merchant', 'Top Up', NULL, 3600 ),
        ( 46,  'merchant', 'General', 13, 3600 ),
        ( 47,  'merchant', 'Unable To TopUp', 13, 5000 ),
    ( 14, 'merchant', 'Courier', NULL, 3600 ),
        ( 48,  'merchant', 'General', 14, 3600 ),
        ( 49,  'merchant', 'The Courier Never Arrive', 14, 5000 ),
    ( 15, 'organization', 'Registration', NULL, 3600 ),
        ( 50,  'organization', 'General', 15, 3600 ),
        ( 51,  'organization', 'Cant Register', 15, 5000 ),
    ( 16, 'organization', 'Login', NULL, 3600 ),
        ( 51,  'organization', 'General', 16, 3600 ),
        ( 52,  'organization', 'Cant Login', 16, 4000 ),
    ( 17, 'organization', 'Pricing', NULL, 3600 ),
        ( 53,  'organization', 'General', 17, 3600 ),
        ( 54,  'organization', 'Cant Change The Price', 17, 4000 ),
    ( 18, 'organization', 'Member', NULL, 3600 ),
        ( 55,  'organization', 'General', 18, 3600 ),
        ( 56,  'organization', 'Cant Add New Member', 18, 4000 ),
    ( 19, 'organization', 'Transfer', NULL, 3600 ),
        ( 57,  'organization', 'General', 19, 3600 ),
        ( 58,  'organization', 'Cant Do Transfer For Some User', 19, 4000 ),
    ( 20, 'organization', 'Top Up', NULL, 3600 ),
        ( 59,  'organization', 'General', 20, 3600 ),
        ( 60,  'organization', 'Cant Do Topup', 20, 6000 ),
    ( 21, 'organization', 'Purchase', NULL, 3600 ),
        ( 61,  'organization', 'General', 21, 3600 ),
        ( 62,  'organization', 'The Purchase Always Fail', 21, 6000 );

CREATE TABLE IF NOT EXISTS `ticket_comment` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user` INTEGER NOT NULL,
    `ticket` INTEGER NOT NULL,
    `content` TEXT COLLATE utf8_unicode_ci NOT NULL,
    `attachment` VARCHAR(200),
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ticket_guideline` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `category` INTEGER NOT NULL,
    `subject` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
    `content` TEXT COLLATE utf8_unicode_ci NOT NULL,
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ticket_priority` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `label` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `ticket_priority` ( `id`, `label` ) VALUES
    ( 1, 'Low' ),
    ( 2, 'Medium' ),
    ( 3, 'High' ),
    ( 4, 'Super' );

CREATE TABLE IF NOT EXISTS `ticket_scope` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `label` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `ticket_scope` ( `id`, `label` ) VALUES
    ( 1, 'Customer Care' ),
    ( 2, 'Marketing & Sales' ),
    ( 3, 'Bizdev' ),
    ( 4, 'BOD' );

CREATE TABLE IF NOT EXISTS `ticket_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `label` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO `ticket_status` ( `id`, `label` ) VALUES
    ( 1, 'Rejected' ),
    ( 2, 'Active' ),
    ( 3, 'Resolved' );